﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Model.ViewModel
{
    public partial class TemplateTestModeMapViewModel
    {
        public int Id { get; set; }
        //public List<int> ATDPositionIds { get; set; }
        public int? TestModeId { get; set; }
        public List<int> TestModeIdArray { get; set; }
        public string TestModeName { get; set; }
        public int? TemplateId { get; set; }
        public string TemplateName { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
