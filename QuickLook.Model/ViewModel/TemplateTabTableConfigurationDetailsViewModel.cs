﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.ViewModel
{
    public partial class TemplateTabTableConfigurationDetailsViewModel
    {
        public int Id { get; set; }
        public string TemplateTabTableConfigurationName { get; set; }
        public int TemplateTabTableConfigurationId { get; set; }
        public int RowId { get; set; }
        public int RowSpan { get; set; }
        public int ColumnId { get; set; }
        public int ColumnSpan { get; set; }
        public int ColumnWidth { get; set; }
        public int ColumnType { get; set; }
        public string UserDefinedColumn { get; set; }
        public string DbColumn { get; set; }
        public string ValueType { get; set; }
        public string AtdPosition { get; set; }
        public string ConditionalUserDefinedColumn { get; set; }
        public string ResultColumn { get; set; }
        public string ColourCode { get; set; }
        public bool IsConfigHeader { get; set; }
        public bool IsHeader { get; set; }
        public bool? IsDbColumn { get; set; }
        public bool? IsUserDefined { get; set; }
        public bool? IsConditional { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsCalculated { get; set; }
        public int? FormulaOperatorId { get; set; }
        public int? TemplateTabTableConfigurationFormulaId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsParentDisabled { get; set; }
        public int LastUpdatedBy { get; set; }
        public DateTime LastUpdatedOn { get; set; }
    }

    public partial class TemplateTabTableConfigurationDetailsUpsertViewModel
    {
        public int Id { get; set; }
        public int TemplateTabTableConfigurationId { get; set; }
        public string TemplateTabTableConfigurationName { get; set; }
        public int RowId { get; set; }
        public int RowSpan { get; set; }
        public int ColumnId { get; set; }
        public int ColumnSpan { get; set; }
        public int ColumnWidth { get; set; }
        public int ColumnType { get; set; }
        public string UserDefinedColumn { get; set; }
        public string DbColumn { get; set; }
        public string ValueType { get; set; }
        public string AtdPosition { get; set; }
        public string ConditionalUserDefinedColumn { get; set; }
        public string ResultColumn { get; set; }
        public string ColourCode { get; set; }
        public bool IsConfigHeader { get; set; }
        public bool IsHeader { get; set; }
        public bool? IsDbColumn { get; set; }
        public bool? IsUserDefined { get; set; }
        public bool? IsConditional { get; set; }
        public bool IsReadOnly { get; set; }
        public int? FormulaOperatorId { get; set; }
        public int? TemplateTabTableConfigurationFormulaId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsParentDisabled { get; set; }
    }

    public partial class TemplateTabTableConfigurationDetailsRowsViewModel
    {
        public string TemplateTabTableConfigurationName { get; set; }
        public int TemplateTabTableConfigurationId { get; set; }
        public int RowId { get; set; }
        public List<TemplateTabTableConfigurationDetailsColumnsViewModel> Columns { get; set; }
    }

    public partial class TemplateTabTableConfigurationDetailsColumnsViewModel
    {
        public int Id { get; set; }
        public int RowId { get; set; }
        public int ColumnId { get; set; }
        public int RowSpan { get; set; }
        public int ColumnSpan { get; set; }
        public int ColumnWidth { get; set; }
        public int ColumnType { get; set; }
        public string UserDefinedColumn { get; set; }
        public string DbColumn { get; set; }
        public string ValueType { get; set; }
        public string AtdPosition { get; set; }
        public string ConditionalUserDefinedColumn { get; set; }
        public string ResultColumn { get; set; }
        public string ColourCode { get; set; }
        public bool? IsConfigHeader { get; set; }
        public bool? IsHeader { get; set; }
        public bool? IsDbColumn { get; set; }
        public bool? IsUserDefined { get; set; }
        public bool? IsConditional { get; set; }
        public bool? IsReadOnly { get; set; }
        public bool IsDeleted { get; set; }
        public int? FormulaOperatorId { get; set; }
        public int? TemplateTabTableConfigurationFormulaId { get; set; }
    }

    public partial class TemplateTabTableConfigurationDetailsPreviewViewModel
    {
        public int Id { get; set; }
        public int TemplateTabTableConfigurationId { get; set; }
        public string TemplateTabTableConfigurationName { get; set; }
        public int RowId { get; set; }
        public int ColumnId { get; set; }
        public int RowSpan { get; set; }
        public int ColumnSpan { get; set; }
        public int ColumnWidth { get; set; }
        public int ColumnType { get; set; }
        public string DbColumn { get; set; }
        public string ColumnHeader { get; set; }
        public string ColumnData { get; set; }
        public string ColourCode { get; set; }
        public bool? IsHeader { get; set; }
        public bool IsDeleted { get; set; }
        public string ASAMATDValue { get; set; }
    }

    public partial class TemplateTabTableConfigurationDetailsPreviewRowsViewModel
    {
        public int TemplateTabTableConfigurationId { get; set; }
        public string TemplateTabTableConfigurationName { get; set; }
        public int RowId { get; set; }
        public List<TemplateTabTableConfigurationDetailsPreviewColumnsViewModel> Columns { get; set; }
    }

    public partial class TemplateTabTableConfigurationDetailsPreviewColumnsViewModel
    {
        public int Id { get; set; }
        public int RowId { get; set; }
        public int ColumnId { get; set; }
        public int RowSpan { get; set; }
        public int ColumnSpan { get; set; }
        public int ColumnWidth { get; set; }
        public int ColumnType { get; set; }
        public string DbColumn { get; set; }
        public string ColumnHeader { get; set; }
        public string ColumnData { get; set; }
        public string ColourCode { get; set; }
        public bool? IsHeader { get; set; }
        public bool IsDeleted { get; set; }
        public string ASAMATDValue { get; set; }
    }
}
