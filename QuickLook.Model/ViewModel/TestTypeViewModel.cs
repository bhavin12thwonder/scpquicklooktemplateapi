﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Model.ViewModel
{
    public class TestTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
