﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.ViewModel
{
    public partial class TemplateTabConfigurationViewModel
    {
        public int Id { get; set; }
        public int TemplateTabId { get; set; }
        public int RowId { get; set; }
        public int ColumnId { get; set; }
        public int ColumnSpan { get; set; }
        public int DisplayFormatTypeId { get; set; }
        public bool IsDeleted { get; set; }
        public int TemplateTabTableConfigurationId { get; set; }
        public string TemplateTabTableConfigurationName { get; set; }
        public string TemplateTabTableConfigurationLastUpdatedBy { get; set; }
        public DateTime TemplateTabTableConfigurationLastUpdatedOn { get; set; }
        public bool IsTabTableConfigExists { get; set; }
        public int ChartTypeId { get; set; }
        public int TemplateTabChartConfigurationId { get; set; }
        public string TemplateTabChartConfigurationName { get; set; }
        public string TemplateTabChartConfigurationLastUpdatedBy { get; set; }
        public DateTime TemplateTabChartConfigurationLastUpdatedOn { get; set; }
        public bool IsTabChartConfigExists { get; set; }
    }

    public partial class TemplateTabConfigurationTypeCheckViewModel
    {
        public string StatusMessage { get; set; }
        public bool IsConfigExists { get; set; }
        public bool IsSuccess { get; set; }
    }

    public partial class TemplateTabConfigurationUpsertViewModel
    {
        public int Id { get; set; }
        public int TemplateTabId { get; set; }
        public int RowId { get; set; }
        public int ColumnId { get; set; }
        public int ColumnSpan { get; set; }
        public int DisplayFormatTypeId { get; set; }
        public bool IsDeleted { get; set; }
    }

    public partial class TemplateTabConfigurationRowsViewModel
    {
        public int TemplateTabId { get; set; }
        public int RowId { get; set; }
        public List<TemplateTabConfigurationColumnsViewModel> Columns { get; set; }
    }

    public partial class TemplateTabConfigurationColumnsViewModel
    {
        public int Id { get; set; }
        public int RowId { get; set; }
        public int ColumnId { get; set; }
        public int ColumnSpan { get; set; }
        public int DisplayFormatTypeId { get; set; }
        public int TemplateTabTableConfigurationId { get; set; }
        public string TemplateTabTableConfigurationName { get; set; }
        public string TemplateTabTableConfigurationLastUpdatedBy { get; set; }
        public DateTime TemplateTabTableConfigurationLastUpdatedOn { get; set; }
        public bool IsTabTableConfigExists { get; set; }
        public int ChartTypeId { get; set; }
        public int TemplateTabChartConfigurationId { get; set; }
        public string TemplateTabChartConfigurationName { get; set; }
        public string TemplateTabChartConfigurationLastUpdatedBy { get; set; }
        public DateTime TemplateTabChartConfigurationLastUpdatedOn { get; set; }
        public bool IsTabChartConfigExists { get; set; }
    }

    //public partial class TemplateTabConfigurationPreviewViewModel
    //{
    //    public int Id { get; set; }
    //    public int TemplateTabId { get; set; }
    //    public int RowId { get; set; }
    //    public int ColumnId { get; set; }
    //    public int ColumnSpan { get; set; }
    //    public int DisplayFormatTypeId { get; set; }
    //    public int TabTableConfigurationId { get; set; }
    //    public string TabTableConfigurationName { get; set; }
    //    public int TabChartConfigurationId { get; set; }
    //    public string TabChartConfigurationName { get; set; }
    //    public bool IsDeleted { get; set; }
    //}

    //public partial class TemplateTabConfigurationPreviewRowsViewModel
    //{
    //    public int TemplateTabId { get; set; }
    //    public int RowId { get; set; }
    //    public List<TemplateTabConfigurationPreviewColumnsViewModel> Columns { get; set; }
    //}

    //public partial class TemplateTabConfigurationPreviewColumnsViewModel
    //{
    //    public int Id { get; set; }
    //    public int RowId { get; set; }
    //    public int ColumnId { get; set; }
    //    public int ColumnSpan { get; set; }
    //    public int DisplayFormatTypeId { get; set; }
    //    public int TabTableConfigurationId { get; set; }
    //    public string TabTableConfigurationName { get; set; }
    //    public int TabChartConfigurationId { get; set; }
    //    public string TabChartConfigurationName { get; set; }
    //    public List<TemplateTabTableConfigurationDetailsPreviewRowsViewModel> TemplateTabTableConfigurationPreviewViewModel { get; set; }
    //    public TemplateTabChartConfigurationPreviewViewModel TemplateTabChartConfigurationPreviewViewModel { get; set; }
    //    public bool IsDeleted { get; set; }
    //}
}
