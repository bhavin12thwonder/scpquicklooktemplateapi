﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Model.ViewModel
{
    public class ViewData
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
