﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.ViewModel
{
    public partial class ChartChannelViewModel
    {
        public int Id { get; set; }
        public int? TemplateTabChartConfigurationId { get; set; }
        public string Name { get; set; }
        public List<int?> ATDPositionNameIds { get; set; }
        //public List<UniqueIdsViewModel> ATDPositionName { get; set; }
        public string ChartLineColour { get; set; }
        public int? ChartLineStyleId { get; set; }
        public int? ChartLineThicknessId { get; set; }
        public string LegendName { get; set; }
        public int? GoodValue { get; set; }
        public int? AcceptableValue { get; set; }
        public int? MarginalValue { get; set; }
        public int? PoorValue { get; set; }
        public int ATDPosition { get; set; }
        public List<UniqueIdsViewModel> ATDPositionIds { get; set; }
        //public List<int> ATDPositionIds { get; set; }
        public bool? FillArea { get; set; }
        public bool? Polarity { get; set; }
        public bool? IsLegendVisible { get; set; }
        public bool? IsAreaChart { get; set; }
        public bool IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? XAxisChannelId { get; set; }
        public string SiUnit { get; set; }
    }
}
