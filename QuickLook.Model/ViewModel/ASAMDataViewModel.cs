﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Model.ViewModel
{
    public class ChannelDataViewModel
    {
        public string Name { get; set; }
        public double[] Value { get; set; }

        public string Unit { get; set; }
    }

    public class CalculationValueDataViewModel
    {
        public string Name { get; set; }
        public double Value { get; set; }

        public string Unit { get; set; }
    }

    public class ATDRequestModel
    {
        public string CalculationName { get; set; }
        public string ATDPosition { get; set; }
        public int ATDPositionId { get; set; }
    }

}
