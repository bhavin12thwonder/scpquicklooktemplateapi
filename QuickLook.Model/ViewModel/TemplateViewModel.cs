﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Model.ViewModel
{
    public class TemplateViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? TestTypeId { get; set; }
        public string TestType { get; set; }
        public List<int?> TestModeList { get; set; }
        public List<string> TestModeNames { get; set; }
        public List<UniqueIdsViewModel> TestModeIds { get; set; }
        public string TestMode { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }
    }

    public partial class TemplateTabPreviewViewModel
    {
        public int Id { get; set; }
        public int TemplateId { get; set; }
        public string TemplateTabName { get; set; }
        public List<TemplateTabConfigurationPreviewRowsViewModel> TemplateTabConfigurationPreviewModel { get; set; }
    }

    public partial class TemplateTabConfigurationPreviewViewModel
    {
        public int Id { get; set; }
        public int TemplateTabId { get; set; }
        public int RowId { get; set; }
        public int ColumnId { get; set; }
        public int ColumnSpan { get; set; }
        public int DisplayFormatTypeId { get; set; }
        public int TabTableConfigurationId { get; set; }
        public string TabTableConfigurationName { get; set; }
        public int TabChartConfigurationId { get; set; }
        public string TabChartConfigurationName { get; set; }
        public bool IsDeleted { get; set; }
    }

    public partial class TemplateTabConfigurationPreviewRowsViewModel
    {
        public int TemplateTabId { get; set; }
        public int RowId { get; set; }
        public List<TemplateTabConfigurationPreviewColumnsViewModel> Columns { get; set; }
    }

    public partial class TemplateTabConfigurationPreviewColumnsViewModel
    {
        public int Id { get; set; }
        public int RowId { get; set; }
        public int ColumnId { get; set; }
        public int ColumnSpan { get; set; }
        public int DisplayFormatTypeId { get; set; }
        public int TabTableConfigurationId { get; set; }
        public string TabTableConfigurationName { get; set; }
        public int TabChartConfigurationId { get; set; }
        public string TabChartConfigurationName { get; set; }
        public List<TemplateTabTableConfigurationDetailsPreviewRowsViewModel> TemplateTabTableConfigurationPreviewViewModel { get; set; }
        public TemplateTabChartConfigurationPreviewViewModel TemplateTabChartConfigurationPreviewViewModel { get; set; }
        public bool IsDeleted { get; set; }
    }
}
