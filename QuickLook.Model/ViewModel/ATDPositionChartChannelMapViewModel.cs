﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Model.ViewModel
{
    public partial class ATDPositionChartChannelMapViewModel
    {
        public int Id { get; set; }
        //public List<int> ATDPositionIds { get; set; }
        public int? ATDPositionId { get; set; }
        public int? AtdpositionId { get; set; }
        public List<int> AtdPositionIdArray { get; set; } 
        public string ATDPositionValue { get; set; }
        public string ATDPositionName { get; set; }
        public string Name { get; set; }
        public int? ChartChannelId { get; set; }
        public string ChannelName { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
