﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.ViewModel
{
    public partial class ChartAnnotationViewModel
    {
        public int Id { get; set; }
        public int? TemplateTabChartConfigurationId { get; set; }
        public string AnnotationLineStartXCoOrdinate { get; set; }
        public string AnnotationLineEndXCoOrdinate { get; set; }
        public string AnnotationLineStartYCoOrdinate { get; set; }
        public string AnnotationLineEndYCoOrdinate { get; set; }
        public int? AnnotationLineThicknessId { get; set; }
        public string AnnotationLineColour { get; set; }
        public int? AnnotationLineStyleId { get; set; }
        public string AnnotationText { get; set; }
        public string AnnotationTextStartXCoOrdinate { get; set; }
        public string AnnotationTextStartYCoOrdinate { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }

    public partial class ChartAnnotationUpsertViewModel
    {
        public int Id { get; set; }
        public int? TemplateTabChartConfigurationId { get; set; }
        public string AnnotationLineStartXcoOrdinate { get; set; }
        public string AnnotationLineEndXcoOrdinate { get; set; }
        public string AnnotationLineStartYcoOrdinate { get; set; }
        public string AnnotationLineEndYcoOrdinate { get; set; }
        public int? AnnotationLineThicknessId { get; set; }
        public string AnnotationLineColour { get; set; }
        public int? AnnotationLineStyleId { get; set; }
        public string AnnotationText { get; set; }
        public string AnnotationTextStartXcoOrdinate { get; set; }
        public string AnnotationTextStartYcoOrdinate { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
