﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.ViewModel
{

    public partial class TemplateTabChartConfigurationDetailsUpsertViewModel
    {
        public int Id { get; set; }
        public int TemplateTabChartConfigurationId { get; set; }
        public string TemplateTabChartConfigurationName { get; set; }
        public int XAxisChannelId { get; set; }
        public string XAxisUnits { get; set; }
        public double XAxisMinValue { get; set; }
        public double XAxisMaxValue { get; set; }
        public double XAxisInterval { get; set; }
        public string XAxisLabel { get; set; }
        public bool XAxisAutoFill { get; set; }
        public string YAxisUnits { get; set; }
        public double YAxisMinValue { get; set; }
        public double YAxisMaxValue { get; set; }
        public double YAxisInterval { get; set; }
        public string YAxisLabel { get; set; }
        public bool YAxisAutoFill { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsParentDisabled { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public List<ChartAnnotationUpsertViewModel> ChartAnnotationList { get; set; }
    }

    public partial class TemplateTabChartConfigurationDetailsViewModel
    {
        public int Id { get; set; }
        public int TemplateTabChartConfigurationId { get; set; }
        public string TemplateTabChartConfigurationName { get; set; }
        public int XAxisChannelId { get; set; }
        public string XAxisUnits { get; set; }
        public double XAxisMinValue { get; set; }
        public double XAxisMaxValue { get; set; }
        public double XAxisInterval { get; set; }
        public string XAxisLabel { get; set; }
        public bool XAxisAutoFill { get; set; }
        public string YAxisUnits { get; set; }
        public double YAxisMinValue { get; set; }
        public double YAxisMaxValue { get; set; }
        public double YAxisInterval { get; set; }
        public string YAxisLabel { get; set; }
        public bool YAxisAutoFill { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsParentDisabled { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
    }

    public partial class TemplateTabChartConfigurationDetailsPreviewViewModel
    {
        public int Id { get; set; }
        public int ChartTypeId { get; set; }
        public string ChartTypeValue { get; set; }
        public string ChartOrientation { get; set; }
        public int TemplateTabChartConfigurationId { get; set; }
        public string TemplateTabChartConfigurationName { get; set; }
        public int XAxisChannelId { get; set; }
        public string XAxisChannel { get; set; }
        public string XAxisUnits { get; set; }
        public double XAxisMinValue { get; set; }
        public double XAxisMaxValue { get; set; }
        public double XAxisInterval { get; set; }
        public string XAxisLabel { get; set; }
        public string YAxisUnits { get; set; }
        public double YAxisMinValue { get; set; }
        public double YAxisMaxValue { get; set; }
        public double YAxisInterval { get; set; }
        public string YAxisLabel { get; set; }
        public int ChartChannelId { get; set; }
        public string ChartChannelName { get; set; }
        public string ChartLineColour { get; set; }
        public int? ChartLineStyleId { get; set; }
        public string LineStyleName { get; set; }
        public string LineStyleValue { get; set; }
        public int LineThickness { get; set; }
        public int? ChartLineThicknessId { get; set; }
        public string LegendName { get; set; }
        public int? GoodValue { get; set; }
        public int? AcceptableValue { get; set; }
        public int? MarginalValue { get; set; }
        public int? PoorValue { get; set; }
        public int? ATDPosition { get; set; }
        public string ATDPositionValue { get; set; }
        public bool? FillArea { get; set; }
        public bool? Polarity { get; set; }
        public bool? IsLegendVisible { get; set; }
        public int ChartAnnotationId { get; set; }
        public string AnnotationLineStartXCoOrdinate { get; set; }
        public string AnnotationLineEndXCoOrdinate { get; set; }
        public string AnnotationLineStartYCoOrdinate { get; set; }
        public string AnnotationLineEndYCoOrdinate { get; set; }
        public int? AnnotationLineThicknessId { get; set; }
        public string AnnotationLineColour { get; set; }
        public int? AnnotationLineStyleId { get; set; }
        public string AnnotationText { get; set; }
        public string AnnotationTextStartXCoOrdinate { get; set; }
        public string AnnotationTextStartYCoOrdinate { get; set; }
        public bool IsDeleted { get; set; }
    }

    public partial class TemplateTabChartConfigurationIdsPreviewViewModel
    {
        public int Id { get; set; }
        public int TemplateTabChartConfigurationId { get; set; }

    }

    public partial class TemplateTabChartConfigurationPreviewViewModel
    {
        public int Id { get; set; }
        public int ChartTypeId { get; set; }
        public string ChartTypeValue { get; set; }
        public int TemplateTabChartConfigurationId { get; set; }
        public string TemplateTabChartConfigurationName { get; set; }
        public int[] XAxis { get; set; }
        public int[] YAxis { get; set; }
        public int XAxisChannelId { get; set; }
        public string XAxisChannel { get; set; }
        public string XAxisUnits { get; set; }
        public double XAxisMinValue { get; set; }
        public double XAxisMaxValue { get; set; }
        public double XAxisInterval { get; set; }
        public string XAxisLabel { get; set; }
        public string YAxisUnits { get; set; }
        public double YAxisMinValue { get; set; }
        public double YAxisMaxValue { get; set; }
        public double YAxisInterval { get; set; }
        public string YAxisLabel { get; set; }
        public List<TemplateTabChartChannelConfigurationPreviewViewModel> ChartChannelConfiguration { get; set; }
        public List<TemplateTabChartAnnotationConfigurationPreviewViewModel> ChartAnnotationConfiguration { get; set; }

    }
    public partial class TemplateTabChartChannelConfigurationPreviewViewModel
    {
        public int ChartChannelId { get; set; }
        public string ChartChannelName { get; set; }
        public string ChartOrientation { get; set; }
        public string ChartLineColour { get; set; }
        public int? ChartLineStyleId { get; set; }
        public string LineStyleName { get; set; }
        public string LineStyleValue { get; set; }
        public int LineThickness { get; set; }
        public int? ChartLineThicknessId { get; set; }
        public string LegendName { get; set; }
        public int? GoodValue { get; set; }
        public int? AcceptableValue { get; set; }
        public int? MarginalValue { get; set; }
        public int? PoorValue { get; set; }
        public int? ATDPosition { get; set; }
        public string ATDPositionValue { get; set; }
        public bool? FillArea { get; set; }
        public bool? Polarity { get; set; }
        public double ATDPositionAxisValue { get; set; }
        public List<ChartChannelATDPositionPreviewViewModel> ChartChannelATDPositions { get; set; }
        public double BarChartXAxisValue { get; set; }
        public string BarChartYAxisValue { get; set; }
        public double[] ChannelXAxis { get; set; }
        public double[] ChannelYAxis { get; set; }
        public bool? IsLegendVisible { get; set; }
    }

    public partial class TemplateTabChartAnnotationConfigurationPreviewViewModel
    {
        public int ChartAnnotationId { get; set; }
        public string AnnotationLineStartXCoOrdinate { get; set; }
        public string AnnotationLineEndXCoOrdinate { get; set; }
        public string AnnotationLineStartYCoOrdinate { get; set; }
        public string AnnotationLineEndYCoOrdinate { get; set; }
        public int? AnnotationLineThicknessId { get; set; }
        public string AnnotationLineColour { get; set; }
        public int? AnnotationLineStyleId { get; set; }
        public string AnnotationText { get; set; }
        public string AnnotationTextStartXCoOrdinate { get; set; }
        public string AnnotationTextStartYCoOrdinate { get; set; }
    }

    public partial class ChartChannelATDPositionPreviewViewModel
    {
        public int ATDPositionId { get; set; }
        public string ATDPositionValue { get; set; }
        public double ATDPositionAxisValue { get; set; }
    }
}
