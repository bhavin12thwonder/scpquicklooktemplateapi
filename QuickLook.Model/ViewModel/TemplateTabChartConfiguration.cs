﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.ViewModel
{
    public partial class TemplateTabChartConfigurationViewModel
    {
        public int Id { get; set; }
        public int? TemplateTabConfigId { get; set; }
        public int? ChartTypeId { get; set; }
        public char ChartOrientation { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsParentDisabled { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
