﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace QuickLook.Model.Entities
{
    public partial class QuickLookContext : DbContext
    {
        public QuickLookContext()
        {
        }

        public QuickLookContext(DbContextOptions<QuickLookContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Atdposition> Atdposition { get; set; }
        public virtual DbSet<AtdpositionChartChannelMap> AtdpositionChartChannelMap { get; set; }
        public virtual DbSet<ChartAnnotation> ChartAnnotation { get; set; }
        public virtual DbSet<ChartChannel> ChartChannel { get; set; }
        public virtual DbSet<ChartLineStyle> ChartLineStyle { get; set; }
        public virtual DbSet<ChartLineThickness> ChartLineThickness { get; set; }
        public virtual DbSet<ChartType> ChartType { get; set; }
        public virtual DbSet<DisplayFormat> DisplayFormat { get; set; }
        public virtual DbSet<FormulaOperator> FormulaOperator { get; set; }
        public virtual DbSet<Template> Template { get; set; }
        public virtual DbSet<TemplateTabChartConfiguration> TemplateTabChartConfiguration { get; set; }
        public virtual DbSet<TemplateTabChartConfigurationDetails> TemplateTabChartConfigurationDetails { get; set; }
        public virtual DbSet<TemplateTabConfiguration> TemplateTabConfiguration { get; set; }
        public virtual DbSet<TemplateTabs> TemplateTabs { get; set; }
        public virtual DbSet<TemplateTabTableConfiguration> TemplateTabTableConfiguration { get; set; }
        public virtual DbSet<TemplateTabTableConfigurationDetails> TemplateTabTableConfigurationDetails { get; set; }
        public virtual DbSet<TemplateTabTableConfigurationFormula> TemplateTabTableConfigurationFormula { get; set; }
        public virtual DbSet<TestMode> TestMode { get; set; }
        public virtual DbSet<TestType> TestType { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("server=HRARAYL307331P\\MSSQLSERVERCST;database=QuickLook;user id=sa;password=@12thwonder;MultipleActiveResultSets=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Atdposition>(entity =>
            {
                entity.ToTable("ATDPosition");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<AtdpositionChartChannelMap>(entity =>
            {
                entity.ToTable("ATDPositionChartChannelMap");

                entity.Property(e => e.AtdpositionId).HasColumnName("ATDPositionId");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Atdposition)
                    .WithMany(p => p.AtdpositionChartChannelMap)
                    .HasForeignKey(d => d.AtdpositionId)
                    .HasConstraintName("FK_ATDPositionChartChannelMap_ATDPosition");

                entity.HasOne(d => d.ChartChannel)
                    .WithMany(p => p.AtdpositionChartChannelMap)
                    .HasForeignKey(d => d.ChartChannelId)
                    .HasConstraintName("FK_ATDPositionChartChannelMap_ChartChannel");
            });

            modelBuilder.Entity<ChartAnnotation>(entity =>
            {
                entity.Property(e => e.AnnotationLineColour).HasMaxLength(50);

                entity.Property(e => e.AnnotationLineEndXcoOrdinate)
                    .HasColumnName("AnnotationLineEndXCoOrdinate")
                    .HasMaxLength(50);

                entity.Property(e => e.AnnotationLineEndYcoOrdinate)
                    .HasColumnName("AnnotationLineEndYCoOrdinate")
                    .HasMaxLength(50);

                entity.Property(e => e.AnnotationLineStartXcoOrdinate)
                    .HasColumnName("AnnotationLineStartXCoOrdinate")
                    .HasMaxLength(50);

                entity.Property(e => e.AnnotationLineStartYcoOrdinate)
                    .HasColumnName("AnnotationLineStartYCoOrdinate")
                    .HasMaxLength(50);

                entity.Property(e => e.AnnotationText).HasMaxLength(50);

                entity.Property(e => e.AnnotationTextStartXcoOrdinate)
                    .HasColumnName("AnnotationTextStartXCoOrdinate")
                    .HasMaxLength(50);

                entity.Property(e => e.AnnotationTextStartYcoOrdinate)
                    .HasColumnName("AnnotationTextStartYCoOrdinate")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.AnnotationLineStyle)
                    .WithMany(p => p.ChartAnnotation)
                    .HasForeignKey(d => d.AnnotationLineStyleId)
                    .HasConstraintName("FK_ChartAnnotation_ChartLineStyle");

                entity.HasOne(d => d.TemplateTabChartConfiguration)
                    .WithMany(p => p.ChartAnnotation)
                    .HasForeignKey(d => d.TemplateTabChartConfigurationId)
                    .HasConstraintName("FK_ChartAnnotation_TemplateTabChartConfiguration");
            });

            modelBuilder.Entity<ChartChannel>(entity =>
            {
                entity.Property(e => e.Atdposition).HasColumnName("ATDPosition");

                entity.Property(e => e.ChartLineColour).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.FillArea).HasDefaultValueSql("((0))");

                entity.Property(e => e.LegendName).HasMaxLength(50);

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.ChartLineStyle)
                    .WithMany(p => p.ChartChannel)
                    .HasForeignKey(d => d.ChartLineStyleId)
                    .HasConstraintName("FK_ChartChannel_ChartLineStyle");

                entity.HasOne(d => d.TemplateTabChartConfiguration)
                    .WithMany(p => p.ChartChannel)
                    .HasForeignKey(d => d.TemplateTabChartConfigurationId)
                    .HasConstraintName("FK_ChartChannel_TemplateTabChartConfiguration");
            });

            modelBuilder.Entity<ChartLineStyle>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Style).HasMaxLength(50);

                entity.Property(e => e.StyleValue).HasMaxLength(50);
            });

            modelBuilder.Entity<ChartLineThickness>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<ChartType>(entity =>
            {
                entity.Property(e => e.ChartTypeValue).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<DisplayFormat>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);
            });

            modelBuilder.Entity<FormulaOperator>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Operator).HasMaxLength(5);
            });

            modelBuilder.Entity<Template>(entity =>
            {
                entity.HasIndex(e => e.Name)
                    .HasName("UC_Template")
                    .IsUnique();

                entity.Property(e => e.Name).HasMaxLength(500);

                entity.HasOne(d => d.TestMode)
                    .WithMany(p => p.Template)
                    .HasForeignKey(d => d.TestModeId)
                    .HasConstraintName("FK_Template_TestMode");

                entity.HasOne(d => d.TestType)
                    .WithMany(p => p.Template)
                    .HasForeignKey(d => d.TestTypeId)
                    .HasConstraintName("FK_Template_TestType");
            });

            modelBuilder.Entity<TemplateTabChartConfiguration>(entity =>
            {
                entity.HasIndex(e => new { e.Name, e.TemplateTabConfigId })
                    .HasName("UC_TemplateTabChartConfiguration")
                    .IsUnique();

                entity.Property(e => e.ChartOrientation)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsParentDisabled).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.ChartType)
                    .WithMany(p => p.TemplateTabChartConfiguration)
                    .HasForeignKey(d => d.ChartTypeId)
                    .HasConstraintName("FK_TemplateTabChartConfiguration_ChartType1");
            });

            modelBuilder.Entity<TemplateTabChartConfigurationDetails>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsParentDisabled).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.XAxisChannelId).HasColumnName("X-Axis-ChannelId");

                entity.Property(e => e.XAxisInterval).HasColumnName("X-Axis-Interval");

                entity.Property(e => e.XAxisLabel).HasColumnName("X-Axis-Label");

                entity.Property(e => e.XAxisMaxValue).HasColumnName("X-Axis-MaxValue");

                entity.Property(e => e.XAxisMinValue).HasColumnName("X-Axis-MinValue");

                entity.Property(e => e.XAxisUnits)
                    .HasColumnName("X-Axis-Units")
                    .HasMaxLength(50);

                entity.Property(e => e.YAxisInterval).HasColumnName("Y-Axis-Interval");

                entity.Property(e => e.YAxisLabel).HasColumnName("Y-Axis-Label");

                entity.Property(e => e.YAxisMaxValue).HasColumnName("Y-Axis-MaxValue");

                entity.Property(e => e.YAxisMinValue).HasColumnName("Y-Axis-MinValue");

                entity.Property(e => e.YAxisUnits)
                    .HasColumnName("Y-Axis-Units")
                    .HasMaxLength(50);

                entity.HasOne(d => d.TemplateTabChartConfiguration)
                    .WithMany(p => p.TemplateTabChartConfigurationDetails)
                    .HasForeignKey(d => d.TemplateTabChartConfigurationId)
                    .HasConstraintName("FK_TemplateTabChartConfigurationDetails_TemplateTabChartConfiguration");
            });

            modelBuilder.Entity<TemplateTabConfiguration>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsParentDisabled).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.DisplayFormatType)
                    .WithMany(p => p.TemplateTabConfiguration)
                    .HasForeignKey(d => d.DisplayFormatTypeId)
                    .HasConstraintName("FK_TemplateTabConfiguration_DisplayFormat");

                entity.HasOne(d => d.TemplateTab)
                    .WithMany(p => p.TemplateTabConfiguration)
                    .HasForeignKey(d => d.TemplateTabId)
                    .HasConstraintName("FK_TemplateTabConfiguration_TemplateTabs");
            });

            modelBuilder.Entity<TemplateTabs>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsParentDisabled).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Template)
                    .WithMany(p => p.TemplateTabs)
                    .HasForeignKey(d => d.TemplateId)
                    .HasConstraintName("FK_TemplateTabs_Template");
            });

            modelBuilder.Entity<TemplateTabTableConfiguration>(entity =>
            {
                entity.HasIndex(e => new { e.Name, e.TemplateTabConfigId })
                    .HasName("UC_TemplateTabTableConfiguration")
                    .IsUnique();

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.IsParentDisabled).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.TemplateTabConfig)
                    .WithMany(p => p.TemplateTabTableConfiguration)
                    .HasForeignKey(d => d.TemplateTabConfigId)
                    .HasConstraintName("FK_TemplateTabTableConfiguration_TemplateTabConfiguration");
            });

            modelBuilder.Entity<TemplateTabTableConfigurationDetails>(entity =>
            {
                entity.Property(e => e.Atdposition)
                    .HasColumnName("ATDPosition")
                    .HasMaxLength(50);

                entity.Property(e => e.ColourCode).HasMaxLength(20);

                entity.Property(e => e.ConditionalUserDefinedColumn).HasMaxLength(50);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.DbColumn).HasMaxLength(50);

                entity.Property(e => e.IsParentDisabled).HasDefaultValueSql("((0))");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.ResultColumn).HasMaxLength(50);

                entity.Property(e => e.UserDefinedColumn).HasMaxLength(50);

                entity.Property(e => e.ValueType).HasMaxLength(50);

                entity.HasOne(d => d.TemplateTabTableConfigurationFormula)
                    .WithMany(p => p.TemplateTabTableConfigurationDetails)
                    .HasForeignKey(d => d.TemplateTabTableConfigurationFormulaId)
                    .HasConstraintName("FK_TemplateTabTableConfigurationDetails_TemplateTabTableConfigurationFormula");

                entity.HasOne(d => d.TemplateTabTableConfiguration)
                    .WithMany(p => p.TemplateTabTableConfigurationDetails)
                    .HasForeignKey(d => d.TemplateTabTableConfigurationId)
                    .HasConstraintName("FK_TemplateTabTableConfigurationDetails_TemplateTabTableConfiguration");
            });

            modelBuilder.Entity<TemplateTabTableConfigurationFormula>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Operator)
                    .WithMany(p => p.TemplateTabTableConfigurationFormula)
                    .HasForeignKey(d => d.OperatorId)
                    .HasConstraintName("FK_TemplateTabTableConfigurationFormula_FormulaOperator");
            });

            modelBuilder.Entity<TestMode>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.TestType)
                    .WithMany(p => p.TestMode)
                    .HasForeignKey(d => d.TestTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TestMode_TestType");
            });

            modelBuilder.Entity<TestType>(entity =>
            {
                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Department).HasMaxLength(255);

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ManagerSamAccountName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SamAccountName).HasMaxLength(100);
            });
        }
    }
}
