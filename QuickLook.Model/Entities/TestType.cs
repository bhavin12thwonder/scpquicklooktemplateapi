﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TestType
    {
        public TestType()
        {
            Template = new HashSet<Template>();
            TestMode = new HashSet<TestMode>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public ICollection<Template> Template { get; set; }
        public ICollection<TestMode> TestMode { get; set; }
    }
}
