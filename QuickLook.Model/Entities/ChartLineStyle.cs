﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class ChartLineStyle
    {
        public ChartLineStyle()
        {
            ChartAnnotation = new HashSet<ChartAnnotation>();
            ChartChannel = new HashSet<ChartChannel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string StyleValue { get; set; }
        public string Style { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public ICollection<ChartAnnotation> ChartAnnotation { get; set; }
        public ICollection<ChartChannel> ChartChannel { get; set; }
    }
}
