﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TestMode
    {
        public TestMode()
        {
            Template = new HashSet<Template>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int TestTypeId { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public TestType TestType { get; set; }
        public ICollection<Template> Template { get; set; }
    }
}
