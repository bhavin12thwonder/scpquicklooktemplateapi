﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class AtdpositionChartChannelMap
    {
        public int Id { get; set; }
        public int? AtdpositionId { get; set; }
        public int? ChartChannelId { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public Atdposition Atdposition { get; set; }
        public ChartChannel ChartChannel { get; set; }
    }
}
