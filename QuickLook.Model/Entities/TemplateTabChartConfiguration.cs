﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TemplateTabChartConfiguration
    {
        public TemplateTabChartConfiguration()
        {
            ChartAnnotation = new HashSet<ChartAnnotation>();
            ChartChannel = new HashSet<ChartChannel>();
            TemplateTabChartConfigurationDetails = new HashSet<TemplateTabChartConfigurationDetails>();
        }

        public int Id { get; set; }
        public int? TemplateTabConfigId { get; set; }
        public int? ChartTypeId { get; set; }
        public string ChartOrientation { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsParentDisabled { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public ChartType ChartType { get; set; }
        public ICollection<ChartAnnotation> ChartAnnotation { get; set; }
        public ICollection<ChartChannel> ChartChannel { get; set; }
        public ICollection<TemplateTabChartConfigurationDetails> TemplateTabChartConfigurationDetails { get; set; }
    }
}
