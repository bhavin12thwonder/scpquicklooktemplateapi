﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TemplateTabConfiguration
    {
        public TemplateTabConfiguration()
        {
            TemplateTabTableConfiguration = new HashSet<TemplateTabTableConfiguration>();
        }

        public int Id { get; set; }
        public int? TemplateTabId { get; set; }
        public int? RowId { get; set; }
        public int? ColumnId { get; set; }
        public int? ColumnSpan { get; set; }
        public int? DisplayFormatTypeId { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsParentDisabled { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public DisplayFormat DisplayFormatType { get; set; }
        public TemplateTabs TemplateTab { get; set; }
        public ICollection<TemplateTabTableConfiguration> TemplateTabTableConfiguration { get; set; }
    }
}
