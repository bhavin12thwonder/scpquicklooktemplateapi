﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class ChartChannel
    {
        public ChartChannel()
        {
            AtdpositionChartChannelMap = new HashSet<AtdpositionChartChannelMap>();
        }

        public int Id { get; set; }
        public int? TemplateTabChartConfigurationId { get; set; }
        public string Name { get; set; }
        public string ChartLineColour { get; set; }
        public int? ChartLineStyleId { get; set; }
        public int? ChartLineThicknessId { get; set; }
        public string LegendName { get; set; }
        public int? GoodValue { get; set; }
        public int? AcceptableValue { get; set; }
        public int? MarginalValue { get; set; }
        public int? PoorValue { get; set; }
        public int? Atdposition { get; set; }
        public bool? FillArea { get; set; }
        public bool? Polarity { get; set; }
        public bool? IsLegendVisible { get; set; }
        public bool IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public ChartLineStyle ChartLineStyle { get; set; }
        public TemplateTabChartConfiguration TemplateTabChartConfiguration { get; set; }
        public ICollection<AtdpositionChartChannelMap> AtdpositionChartChannelMap { get; set; }
    }
}
