﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TemplateTabChartConfigurationDetails
    {
        public int Id { get; set; }
        public int? TemplateTabChartConfigurationId { get; set; }
        public int? XAxisChannelId { get; set; }
        public string XAxisUnits { get; set; }
        public double? XAxisMinValue { get; set; }
        public double? XAxisMaxValue { get; set; }
        public double? XAxisInterval { get; set; }
        public string XAxisLabel { get; set; }
        public string YAxisUnits { get; set; }
        public double? YAxisMinValue { get; set; }
        public double? YAxisMaxValue { get; set; }
        public double? YAxisInterval { get; set; }
        public string YAxisLabel { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsParentDisabled { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public TemplateTabChartConfiguration TemplateTabChartConfiguration { get; set; }
    }
}
