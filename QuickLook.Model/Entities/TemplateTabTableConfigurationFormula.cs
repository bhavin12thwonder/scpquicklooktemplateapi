﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TemplateTabTableConfigurationFormula
    {
        public TemplateTabTableConfigurationFormula()
        {
            TemplateTabTableConfigurationDetails = new HashSet<TemplateTabTableConfigurationDetails>();
        }

        public int Id { get; set; }
        public int? OperatorId { get; set; }
        public int? RangeFrom { get; set; }
        public int? RangeTo { get; set; }
        public bool? IsRowbased { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public FormulaOperator Operator { get; set; }
        public ICollection<TemplateTabTableConfigurationDetails> TemplateTabTableConfigurationDetails { get; set; }
    }
}
