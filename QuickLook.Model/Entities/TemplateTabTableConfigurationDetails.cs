﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TemplateTabTableConfigurationDetails
    {
        public int Id { get; set; }
        public int? TemplateTabTableConfigurationId { get; set; }
        public int? RowId { get; set; }
        public int? RowSpan { get; set; }
        public int? ColumnId { get; set; }
        public int? ColumnSpan { get; set; }
        public int? ColumnWidth { get; set; }
        public int? ColumnType { get; set; }
        public string UserDefinedColumn { get; set; }
        public string DbColumn { get; set; }
        public string ValueType { get; set; }
        public string Atdposition { get; set; }
        public string ConditionalUserDefinedColumn { get; set; }
        public string ResultColumn { get; set; }
        public string ColourCode { get; set; }
        public bool? IsConfigHeader { get; set; }
        public bool? IsHeader { get; set; }
        public bool? IsDbColumn { get; set; }
        public bool? IsUserDefined { get; set; }
        public bool? IsConditional { get; set; }
        public bool? IsReadOnly { get; set; }
        public bool? IsCalculated { get; set; }
        public int? FormulaOperatorId { get; set; }
        public int? TemplateTabTableConfigurationFormulaId { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsParentDisabled { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public TemplateTabTableConfiguration TemplateTabTableConfiguration { get; set; }
        public TemplateTabTableConfigurationFormula TemplateTabTableConfigurationFormula { get; set; }
    }
}
