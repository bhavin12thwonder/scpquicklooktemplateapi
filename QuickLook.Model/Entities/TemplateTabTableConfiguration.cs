﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TemplateTabTableConfiguration
    {
        public TemplateTabTableConfiguration()
        {
            TemplateTabTableConfigurationDetails = new HashSet<TemplateTabTableConfigurationDetails>();
        }

        public int Id { get; set; }
        public int? TemplateTabConfigId { get; set; }
        public string Name { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsParentDisabled { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public TemplateTabConfiguration TemplateTabConfig { get; set; }
        public ICollection<TemplateTabTableConfigurationDetails> TemplateTabTableConfigurationDetails { get; set; }
    }
}
