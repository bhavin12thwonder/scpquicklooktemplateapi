﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class TemplateTabs
    {
        public TemplateTabs()
        {
            TemplateTabConfiguration = new HashSet<TemplateTabConfiguration>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? TemplateId { get; set; }
        public int? TabIndex { get; set; }
        public int? DisplayOrder { get; set; }
        public int? Rows { get; set; }
        public int? Columns { get; set; }
        public int? RowsLimit { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? IsParentDisabled { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public Template Template { get; set; }
        public ICollection<TemplateTabConfiguration> TemplateTabConfiguration { get; set; }
    }
}
