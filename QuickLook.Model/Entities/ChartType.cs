﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class ChartType
    {
        public ChartType()
        {
            TemplateTabChartConfiguration = new HashSet<TemplateTabChartConfiguration>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ChartTypeValue { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public ICollection<TemplateTabChartConfiguration> TemplateTabChartConfiguration { get; set; }
    }
}
