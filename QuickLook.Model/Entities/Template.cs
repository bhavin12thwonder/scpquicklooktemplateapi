﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class Template
    {
        public Template()
        {
            TemplateTabs = new HashSet<TemplateTabs>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? TestTypeId { get; set; }
        public int? TestModeId { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public int? ModifiedBy { get; set; }

        public TestMode TestMode { get; set; }
        public TestType TestType { get; set; }
        public ICollection<TemplateTabs> TemplateTabs { get; set; }
    }
}
