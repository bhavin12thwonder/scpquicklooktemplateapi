﻿using System;
using System.Collections.Generic;

namespace QuickLook.Model.Entities
{
    public partial class ChartAnnotation
    {
        public int Id { get; set; }
        public int? TemplateTabChartConfigurationId { get; set; }
        public string AnnotationLineStartXcoOrdinate { get; set; }
        public string AnnotationLineEndXcoOrdinate { get; set; }
        public string AnnotationLineStartYcoOrdinate { get; set; }
        public string AnnotationLineEndYcoOrdinate { get; set; }
        public int? AnnotationLineThicknessId { get; set; }
        public string AnnotationLineColour { get; set; }
        public int? AnnotationLineStyleId { get; set; }
        public string AnnotationText { get; set; }
        public string AnnotationTextStartXcoOrdinate { get; set; }
        public string AnnotationTextStartYcoOrdinate { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public ChartLineStyle AnnotationLineStyle { get; set; }
        public TemplateTabChartConfiguration TemplateTabChartConfiguration { get; set; }
    }
}
