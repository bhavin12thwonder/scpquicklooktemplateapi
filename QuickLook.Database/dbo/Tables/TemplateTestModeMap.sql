﻿CREATE TABLE [dbo].[TemplateTestModeMap]
(
	[Id] INT IDENTITY (1, 1) NOT NULL, 
    [TemplateId] INT NULL, 
    [TestModeId] INT NULL, 
    [IsDeleted] BIT NULL, 
    [CreatedBy] INT NULL, 
    [CreatedOn] DATETIME NULL, 
    [ModifiedBy] INT NULL, 
    [ModifiedOn] DATETIME NULL,
    CONSTRAINT [PK_TemplateTestModeMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TemplateTestModeMap_TestMode] FOREIGN KEY ([TestModeId]) REFERENCES [dbo].[TestMode] ([Id]),
    CONSTRAINT [FK_TemplateTestModeMap_Template] FOREIGN KEY ([TemplateId]) REFERENCES [dbo].[Template] ([Id])
)
