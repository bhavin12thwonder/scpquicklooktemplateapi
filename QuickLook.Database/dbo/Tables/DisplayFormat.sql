﻿CREATE TABLE [dbo].[DisplayFormat] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (50) NULL,
    [IsDeleted]  BIT           NULL,
    [CreatedBy]  INT           NULL,
    [CreatedOn]  DATETIME      NULL,
    [ModifiedBy] INT           NULL,
    [ModifiedOn] DATETIME      NULL,
    CONSTRAINT [PK_TableFormat] PRIMARY KEY CLUSTERED ([Id] ASC)
);

