﻿CREATE TABLE [dbo].[ChartLineStyle] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (50) NULL,
    [StyleValue] NVARCHAR (50) NULL,
    [Style]      NVARCHAR (50) NULL,
    [IsDeleted]  BIT           NULL,
    [CreatedBy]  INT           NULL,
    [CreatedOn]  DATETIME      NULL,
    [ModifiedBy] INT           NULL,
    [ModifiedOn] DATETIME      NULL,
    CONSTRAINT [PK_ChartLineStyle] PRIMARY KEY CLUSTERED ([Id] ASC)
);

