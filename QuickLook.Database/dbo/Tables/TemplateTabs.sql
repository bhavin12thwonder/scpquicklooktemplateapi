﻿CREATE TABLE [dbo].[TemplateTabs] (
    [Id]               INT           IDENTITY (1, 1) NOT NULL,
    [Name]             NVARCHAR (50) NULL,
    [TemplateId]       INT           NULL,
    [TabIndex]         INT           NULL,
    [DisplayOrder]     INT           NULL,
    [Rows]             INT           NULL,
    [Columns]          INT           NULL,
    [RowsLimit]        INT           NULL,
    [IsDeleted]        BIT           NULL,
    [IsParentDisabled] BIT           CONSTRAINT [DF_TemplateTabs_IsParentDisabled] DEFAULT ((0)) NULL,
    [CreatedBy]        INT           NULL,
    [CreatedOn]        DATETIME      NULL,
    [ModifiedBy]       INT           NULL,
    [ModifiedOn]       DATETIME      NULL,
    CONSTRAINT [PK_TemplateTabs] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TemplateTabs_Template] FOREIGN KEY ([TemplateId]) REFERENCES [dbo].[Template] ([Id])
);





