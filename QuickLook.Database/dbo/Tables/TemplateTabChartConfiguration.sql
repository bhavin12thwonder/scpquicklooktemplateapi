﻿CREATE TABLE [dbo].[TemplateTabChartConfiguration] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [TemplateTabConfigId] INT           NULL,
    [ChartTypeId]         INT           NULL,
    [ChartOrientation]    CHAR(1)  NULL,
    [Name]                NVARCHAR (50) NULL,
    [IsDeleted]           BIT           NULL,
    [IsParentDisabled]    BIT           CONSTRAINT [DF_TemplateTabChartConfiguration_IsParentDisabled] DEFAULT ((0)) NULL,
    [CreatedBy]           INT           NULL,
    [CreatedOn]           DATETIME      NULL,
    [ModifiedBy]          INT           NULL,
    [ModifiedOn]          DATETIME      NULL,
    CONSTRAINT [PK_TemplateTabChartConfiguration] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TemplateTabChartConfiguration_ChartType1] FOREIGN KEY ([ChartTypeId]) REFERENCES [dbo].[ChartType] ([Id]),
    CONSTRAINT [UC_TemplateTabChartConfiguration] UNIQUE NONCLUSTERED ([Name] ASC, [TemplateTabConfigId] ASC)
);

