﻿CREATE TABLE [dbo].[ChartAnnotation] (
    [Id]                              INT           IDENTITY (1, 1) NOT NULL,
    [TemplateTabChartConfigurationId] INT           NULL,
    [AnnotationLineStartXCoOrdinate]  NVARCHAR (50) NULL,
    [AnnotationLineEndXCoOrdinate]    NVARCHAR (50) NULL,
    [AnnotationLineStartYCoOrdinate]  NVARCHAR (50) NULL,
    [AnnotationLineEndYCoOrdinate]    NVARCHAR (50) NULL,
    [AnnotationLineThicknessId]       INT           NULL,
    [AnnotationLineColour]            NVARCHAR (50) NULL,
    [AnnotationLineStyleId]           INT           NULL,
    [AnnotationText]                  NVARCHAR (50) NULL,
    [AnnotationTextStartXCoOrdinate]  NVARCHAR (50) NULL,
    [AnnotationTextStartYCoOrdinate]  NVARCHAR (50) NULL,
    [IsDeleted]                       BIT           NULL,
    [CreatedBy]                       INT           NULL,
    [CreatedOn]                       DATETIME      NULL,
    [ModifiedBy]                      INT           NULL,
    [ModifiedOn]                      DATETIME      NULL,
    CONSTRAINT [PK_ChartAnnotation] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ChartAnnotation_ChartLineStyle] FOREIGN KEY ([AnnotationLineStyleId]) REFERENCES [dbo].[ChartLineStyle] ([Id]),
    CONSTRAINT [FK_ChartAnnotation_TemplateTabChartConfiguration] FOREIGN KEY ([TemplateTabChartConfigurationId]) REFERENCES [dbo].[TemplateTabChartConfiguration] ([Id])
);



