﻿CREATE TABLE [dbo].[TestMode] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (255) NOT NULL,
    [TestTypeId] INT            NOT NULL,
    [IsDeleted]  BIT            NULL,
    [CreatedBy]  INT            NULL,
    [CreatedOn]  DATETIME       NULL,
    [ModifiedBy] INT            NULL,
    [ModifiedOn] DATETIME       NULL,
    CONSTRAINT [PK_TestMode] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TestMode_TestType] FOREIGN KEY ([TestTypeId]) REFERENCES [dbo].[TestType] ([Id])
);

