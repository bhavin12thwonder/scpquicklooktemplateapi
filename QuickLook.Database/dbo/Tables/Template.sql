﻿CREATE TABLE [dbo].[Template] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (500) NULL,
    [TestTypeId] INT            NULL,
    [TestModeId] INT            NULL,
    [IsDeleted]  BIT            NULL,
    [CreatedOn]  DATETIME2 (7)  NULL,
    [CreatedBy]  INT            NULL,
    [ModifiedOn] DATETIME2 (7)  NULL,
    [ModifiedBy] INT            NULL,
    CONSTRAINT [PK_Template] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Template_TestMode] FOREIGN KEY ([TestModeId]) REFERENCES [dbo].[TestMode] ([Id]),
    CONSTRAINT [FK_Template_TestType] FOREIGN KEY ([TestTypeId]) REFERENCES [dbo].[TestType] ([Id]),
    CONSTRAINT UC_Template UNIQUE ([Name])
);

