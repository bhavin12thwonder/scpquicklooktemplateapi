﻿CREATE TABLE [dbo].[TemplateTabConfiguration] (
    [Id]                  INT       IDENTITY (1, 1) NOT NULL,
    [TemplateTabId]       INT      NULL,
    [RowId]               INT      NULL,
    [ColumnId]            INT      NULL,
    [ColumnSpan]            INT      NULL,
    [DisplayFormatTypeId] INT      NULL,
    [IsDeleted]           BIT      NULL,
    [IsParentDisabled] BIT CONSTRAINT [DF_TemplateTabConfiguration_IsParentDisabled] DEFAULT ((0)) NULL,
    [CreatedBy]           INT      NULL,
    [CreatedOn]           DATETIME NULL,
    [ModifiedBy]          INT      NULL,
    [ModifiedOn]          DATETIME NULL,
    CONSTRAINT [PK_TemplateTabConfiguration] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TemplateTabConfiguration_DisplayFormat] FOREIGN KEY ([DisplayFormatTypeId]) REFERENCES [dbo].[DisplayFormat] ([Id]),
    CONSTRAINT [FK_TemplateTabConfiguration_TemplateTabs] FOREIGN KEY ([TemplateTabId]) REFERENCES [dbo].[TemplateTabs] ([Id])
);



