﻿CREATE TABLE [dbo].[TemplateTabTableConfigurationFormula] (
    [Id]         INT      IDENTITY (1, 1) NOT NULL,
    [OperatorId] INT      NULL,
    [RangeFrom]  INT      NULL,
    [RangeTo]    INT      NULL,
    [IsRowbased] BIT      NULL,
    [IsDeleted]  BIT      NULL,
    [CreatedBy]  INT      NULL,
    [CreatedOn]  DATETIME NULL,
    [ModifiedBy] INT      NULL,
    [ModifiedOn] DATETIME NULL,
    CONSTRAINT [PK_TemplateTabTableConfigurationFormula] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TemplateTabTableConfigurationFormula_FormulaOperator] FOREIGN KEY ([OperatorId]) REFERENCES [dbo].[FormulaOperator] ([Id])
);

