﻿CREATE TABLE [dbo].[ATDPositionChartChannelMap] (
    [Id]             INT      IDENTITY (1, 1) NOT NULL,
    [ATDPositionId]  INT      NULL,
    [ChartChannelId] INT      NULL,
    [IsDeleted]      BIT      NULL,
    [CreatedBy]      INT      NULL,
    [CreatedOn]      DATETIME NULL,
    [ModifiedBy]     INT      NULL,
    [ModifiedOn]     DATETIME NULL,
    CONSTRAINT [PK_ATDPositionChartChannelMap] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ATDPositionChartChannelMap_ATDPosition] FOREIGN KEY ([ATDPositionId]) REFERENCES [dbo].[ATDPosition] ([Id]),
    CONSTRAINT [FK_ATDPositionChartChannelMap_ChartChannel] FOREIGN KEY ([ChartChannelId]) REFERENCES [dbo].[ChartChannel] ([Id])
);

