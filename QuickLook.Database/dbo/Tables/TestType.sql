﻿CREATE TABLE [dbo].[TestType] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (255) NULL,
    [IsDeleted]  BIT            NULL,
    [CreatedBy]  INT            NULL,
    [CreatedOn]  DATETIME       NULL,
    [ModifiedBy] INT            NULL,
    [ModifiedOn] DATETIME       NULL,
    CONSTRAINT [PK_TestType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

