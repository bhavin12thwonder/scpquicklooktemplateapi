﻿CREATE TABLE [dbo].[ChartChannel] (
    [Id]                              INT           IDENTITY (1, 1) NOT NULL,
    [TemplateTabChartConfigurationId] INT           NULL,
    [Name]                            NVARCHAR (50) NULL,
    [ChartLineColour]                 NVARCHAR (50) NULL,
    [ChartLineStyleId]                INT           NULL,
    [ChartLineThicknessId]            INT           NULL,
    [LegendName]                      NVARCHAR (50) NULL,
    [GoodValue]                       INT           NULL,
    [AcceptableValue]                 INT           NULL,
    [MarginalValue]                   INT           NULL,
    [PoorValue]                       INT           NULL,
    [ATDPosition]                     INT            NULL,
    [FillArea]                        BIT           CONSTRAINT [DF_ChartChannel_FillArea] DEFAULT ((0)) NULL,
    [Polarity]                        BIT           NULL,
    [IsLegendVisible]                 BIT           NULL,
	[SiUnit]						  NVARCHAR(50)  NULL,
    [IsDeleted]                       BIT           NOT NULL,
    [CreatedBy]                       INT           NULL,
    [CreatedOn]                       DATETIME      NULL,
    [ModifiedBy]                      INT           NULL,
    [ModifiedOn]                      DATETIME      NULL,
    CONSTRAINT [PK_ChartChannel] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ChartChannel_ChartLineStyle] FOREIGN KEY ([ChartLineStyleId]) REFERENCES [dbo].[ChartLineStyle] ([Id]),
    CONSTRAINT [FK_ChartChannel_TemplateTabChartConfiguration] FOREIGN KEY ([TemplateTabChartConfigurationId]) REFERENCES [dbo].[TemplateTabChartConfiguration] ([Id])
);



