﻿CREATE TABLE [dbo].[ChartType] (
    [Id]         INT           IDENTITY (1, 1) NOT NULL,
    [Name]       NVARCHAR (50) NULL,
    [ChartTypeValue] NVARCHAR(50) NULL, 
    [IsDeleted]  BIT           NULL,
    [CreatedBy]  INT           NULL,
    [CreatedOn]  DATETIME      NULL,
    [ModifiedBy] INT           NULL,
    [ModifiedOn] DATETIME      NULL,
    CONSTRAINT [PK_ChartType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

