﻿CREATE TABLE [dbo].[Users] (
    [Id]                    INT            IDENTITY (1, 1) NOT NULL,
    [SamAccountName]        NVARCHAR (100) NULL,
    [FirstName]             VARCHAR (500)  NULL,
    [LastName]              VARCHAR (500)  NULL,
    [MiddleName]            VARCHAR (100)  NULL,
    [EmailAddress]          VARCHAR (255)  NULL,
    [PhoneNumber]           VARCHAR (50)   NULL,
    [IsDeleted]             BIT            NULL,
    [CreatedDate]           DATETIME       NULL,
    [ModifiedDate]          DATETIME       NULL,
    [ManagerSamAccountName] VARCHAR (20)   NULL,
    [Department]            NVARCHAR (255) NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED ([Id] ASC)
);

