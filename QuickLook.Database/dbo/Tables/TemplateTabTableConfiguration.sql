﻿CREATE TABLE [dbo].[TemplateTabTableConfiguration] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [TemplateTabConfigId] INT           NULL,
    [Name]   NVARCHAR (50) NULL,
    [IsDeleted]           BIT           NULL,
    [IsParentDisabled]    BIT           CONSTRAINT [DF_TemplateTabTableConfiguration_IsParentDisabled] DEFAULT ((0)) NULL,
    [CreatedBy]           INT           NULL,
    [CreatedOn]           DATETIME      NULL,
    [ModifiedBy]          INT           NULL,
    [ModifiedOn]          DATETIME      NULL,
    CONSTRAINT [PK_TemplateTabTableConfiguration] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_TemplateTabTableConfiguration_TemplateTabConfiguration] FOREIGN KEY ([TemplateTabConfigId]) REFERENCES [dbo].[TemplateTabConfiguration] ([Id]),
     CONSTRAINT [UC_TemplateTabTableConfiguration] UNIQUE NONCLUSTERED([Name] ASC,[TemplateTabConfigId] ASC)
);

