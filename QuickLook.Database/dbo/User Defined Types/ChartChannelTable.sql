﻿/*
The database must have a MEMORY_OPTIMIZED_DATA filegroup
before the memory optimized object can be created.
*/

CREATE TYPE [dbo].[ChartChannelTable] AS TABLE
(
	Id INT NOT NULL, 
	[TemplateTabChartConfigurationId] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Colour] [nvarchar](50) NULL,
	[ChartLineStyleId] [int] NULL,
	[ChartLineThicknessId] [int] NULL,
	[LegendText] [nvarchar](50) NULL,
	[GoodValue] [int] NULL,
	[AcceptableValue] [int] NULL,
	[MarginalValue] [int] NULL,
	[PoorValue] [int] NULL,
	[ATDPosition] [int] NULL,
	[FillArea] [bit] NULL,
	[Polarity] [bit] NULL,
	[IsLegendVisible] [bit] NULL,
	[IsDeleted] [bit]  NULL
)