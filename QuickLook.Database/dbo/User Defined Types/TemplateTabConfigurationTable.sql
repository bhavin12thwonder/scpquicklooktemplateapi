﻿CREATE TYPE [dbo].[TemplateTabConfigurationTable] AS TABLE (
    [Id] [int] NULL,
	[TemplateTabId] [int] NULL,
    [RowId]               INT      NULL,
    [ColumnId]            INT      NULL,
    [ColumnSpan]            INT      NULL,
	[DisplayFormatTypeId] [int] NULL,
	[IsDeleted] [bit] NULL);

