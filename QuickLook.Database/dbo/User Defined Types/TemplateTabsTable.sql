﻿CREATE TYPE [dbo].[TemplateTabsTable] AS TABLE (
    [Id] INT  NOT NULL,
    [Name]  NVARCHAR (50) NULL,
    [TemplateId]   INT NULL,
	[TabIndex] [int] NULL,
    [DisplayOrder] INT NULL,
    [Rows]         INT NULL,
    [Columns]      INT NULL,
    [RowsLimit]    INT NULL,
    [IsDeleted]    BIT NULL);

