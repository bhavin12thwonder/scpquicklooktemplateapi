﻿/*
The database must have a MEMORY_OPTIMIZED_DATA filegroup
before the memory optimized object can be created.
*/

CREATE TYPE [dbo].[ChartAnnotationTable] AS TABLE
(
	Id INT NULL, 
	[TemplateTabChartConfigurationId] [int] NULL,
	[AnnotationLineStartXCoOrdinate] [nvarchar](50) NULL,
	[AnnotationLineEndXCoOrdinate] [nvarchar](50) NULL,
	[AnnotationLineStartYCoOrdinate] [nvarchar](50) NULL,
	[AnnotationLineEndYCoOrdinate] [nvarchar](50) NULL,
	[AnnotationLineThicknessId] [int] NULL,
	[AnnotationLineColour] [nvarchar](50) NULL,
	[AnnotationLineStyleId] [int] NULL,
	[AnnotationText] [nvarchar](50) NULL,
	[AnnotationTextStartXCoOrdinate] [nvarchar](50) NULL,
	[AnnotationTextStartYCoOrdinate] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL
);