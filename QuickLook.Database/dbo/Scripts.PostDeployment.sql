﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


print 'ATDPosition.PostDeployment'
:r Data\ATDPosition.PostDeployment.sql

print 'ChartType.PostDeployment'
:r Data\ChartType.PostDeployment.sql

print 'ChartLineStyle.PostDeployment'
:r Data\ChartLineStyle.PostDeployment.sql

print 'ChartLineThickness.PostDeployment'
:r Data\ChartLineThickness.PostDeployment.sql

print 'DisplayFormat.PostDeployment'
:r Data\DisplayFormat.PostDeployment.sql

print 'FormulaOperator.PostDeployment'
:r Data\FormulaOperator.PostDeployment.sql
