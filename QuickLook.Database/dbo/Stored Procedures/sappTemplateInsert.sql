﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateInsert] 
@Name	nvarchar(50)
,@TestTypeId	int
,@TestModeIds dbo.UniqueIdsTable READONLY
,@IsDeleted	bit=0
,@CreatedOn	datetime
,@CreatedBy	int=0

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	DECLARE @TemplateId INT
	BEGIN TRY
	BEGIN TRANSACTION TInsert;
		INSERT INTO Template(
			[Name]
			,TestTypeId
			--,TestModeIds
			,IsDeleted
			,CreatedOn
			,CreatedBy)
		VALUES(
			@Name
			,@TestTypeId
			--,@TestModeId
			,@IsDeleted
			,@CreatedOn
			,@CreatedBy)
			SELECT SCOPE_IDENTITY() AS Id,'Success' As TransactionMessage,1 As IsSuccess;

				SET @TemplateId=SCOPE_IDENTITY();
				IF EXISTS(SELECT 1 FROM @TestModeIds HAVING COUNT(Id)>0)
				BEGIN
					INSERT INTO TemplateTestModeMap(TemplateId,TestModeId,IsDeleted,CreatedBy,CreatedOn)
					SELECT @TemplateId,Id,0,1,GETUTCDATE() FROM @TestModeIds
				END

	COMMIT TRANSACTION TInsert;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TInsert;
			SELECT   
				 0 as Id		
				,ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  

            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
