﻿CREATE PROCEDURE [dbo].[sappTemplateTabConfigurationTypeGetByTemplateTabId]
@Id int,
@TemplateTabConfigurationType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	BEGIN TRY
	
	IF EXISTS(SELECT TOP 1 1 FROM TemplateTabChartConfiguration(NOLOCK) TTCC INNER JOIN TemplateTabConfiguration(NOLOCK) TTC ON TTC.Id=TTCC.TemplateTabConfigId WHERE TTC.DisplayFormatTypeId=@TemplateTabConfigurationType AND TTCC.TemplateTabConfigId=@Id AND TTCC.IsDeleted=0 AND TTCC.IsParentDisabled=0)
		BEGIN		
			SELECT 1 As IsConfigExists,'Chart Configuration' As StatusMessage,1 As IsSuccess;
		END
	ELSE IF EXISTS(SELECT TOP 1 1 FROM TemplateTabTableConfiguration(NOLOCK) TTCC INNER JOIN TemplateTabConfiguration(NOLOCK) TTC ON TTC.Id=TTCC.TemplateTabConfigId WHERE TTC.DisplayFormatTypeId=@TemplateTabConfigurationType AND TTCC.TemplateTabConfigId=@Id AND TTCC.IsDeleted=0 AND TTCC.IsParentDisabled=0)
		BEGIN		
			SELECT 1 As IsConfigExists,'Table Configuration' As StatusMessage,1 As IsSuccess;
		END
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT 0 As IsConfigExists,'Error' As StatusMessage,0 As IsSuccess;

				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END