﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabConfigurationGetByTemplateTabId]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT TTC.Id
			,TTC.TemplateTabId
			,TTC.RowId
			,TTC.ColumnId
			,TTC.ColumnSpan
			,TTC.DisplayFormatTypeId
			,TTC.IsDeleted
			,TemplateTabTableConfigurationId=ISNULL((SELECT TOP 1 Id FROM TemplateTabTableConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),0)
			,TemplateTabTableConfigurationName=ISNULL((SELECT TOP 1 [Name] FROM TemplateTabTableConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),'')
			,IsTabTableConfigExists=ISNULL((SELECT TOP 1 1 FROM TemplateTabTableConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),0)
			,TemplateTabTableConfigurationLastUpdatedBy=ISNULL((
											SELECT TOP 1 ISNULL((USR0.[FirstName]+' '+USR0.LastName),(USR.[FirstName]+' '+USR.LastName))
											FROM TemplateTabTableConfiguration(NOLOCK) TTTC
											LEFT JOIN Users(NOLOCK) USR ON TTTC.CreatedBy=USR.Id
											LEFT JOIN Users(NOLOCK) USR0 ON TTTC.ModifiedBy=USR0.Id
											WHERE TemplateTabConfigId=TTC.Id AND TTC.IsDeleted=0 AND IsParentDisabled=0),'')
			,TemplateTabTableConfigurationLastUpdatedOn=DATEADD(second, DATEDIFF(second, GETUTCDATE(), GETDATE()), (
							SELECT TOP 1 ISNULL(TTTC.ModifiedOn,TTTC.CreatedOn)
							FROM TemplateTabTableConfiguration(NOLOCK) TTTC
							LEFT JOIN Users(NOLOCK) USR ON TTTC.CreatedBy=USR.Id
							LEFT JOIN Users(NOLOCK) USR0 ON TTTC.ModifiedBy=USR0.Id
							WHERE TemplateTabConfigId=TTC.Id AND TTC.IsDeleted=0 AND IsParentDisabled=0))
			,ChartTypeId=ISNULL((SELECT TOP 1 ChartTypeId FROM TemplateTabChartConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),0)
			,TemplateTabChartConfigurationId=ISNULL((SELECT TOP 1 Id FROM TemplateTabChartConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),0)
			,TemplateTabChartConfigurationName=ISNULL((SELECT TOP 1 [Name] FROM TemplateTabChartConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),'')
			,IsTabChartConfigExists=ISNULL((SELECT TOP 1 1 FROM TemplateTabChartConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),0)			
			,TemplateTabChartConfigurationLastUpdatedBy=ISNULL((
						SELECT TOP 1 ISNULL((USR0.[FirstName]+' '+USR0.LastName),(USR.[FirstName]+' '+USR.LastName))
						FROM TemplateTabChartConfiguration(NOLOCK) TTTC
						LEFT JOIN Users(NOLOCK) USR ON TTTC.CreatedBy=USR.Id
						LEFT JOIN Users(NOLOCK) USR0 ON TTTC.ModifiedBy=USR0.Id
						WHERE TTTC.IsDeleted=0 AND TemplateTabConfigId=TTC.Id AND TTC.IsDeleted=0 AND IsParentDisabled=0),'')
			,TemplateTabChartConfigurationLastUpdatedOn=DATEADD(second, DATEDIFF(second, GETUTCDATE(), GETDATE()), (
						SELECT TOP 1 ISNULL(TTTC.ModifiedOn,TTTC.CreatedOn)
						FROM TemplateTabChartConfiguration(NOLOCK) TTTC
						LEFT JOIN Users(NOLOCK) USR ON TTTC.CreatedBy=USR.Id
						LEFT JOIN Users(NOLOCK) USR0 ON TTTC.ModifiedBy=USR0.Id
						WHERE TTTC.IsDeleted=0 AND TemplateTabConfigId=TTC.Id AND TTC.IsDeleted=0 AND IsParentDisabled=0))

			,TTC.CreatedBy
			,TTC.CreatedOn
			,TTC.ModifiedBy
			,TTC.ModifiedOn
		FROM TemplateTabConfiguration TTC
		WHERE TTC.IsDeleted=0 AND TTC.IsParentDisabled=0 AND TTC.TemplateTabId=@Id
END
