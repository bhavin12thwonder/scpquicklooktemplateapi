﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTestModeGetAll] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
			,[Name]
			,TestTypeId
			,IsDeleted
			,CreatedBy
			,CreatedOn
			,ModifiedBy
			,ModifiedOn
		FROM TestMode
		WHERE IsDeleted=0
END
