﻿CREATE PROCEDURE [dbo].[sappChartAnnotationInsert]
@ChartAnnotationTable ChartAnnotationTable Readonly
--,@TemplateTabChartConfigurationId int
,@UserId	int=1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	--BEGIN TRANSACTION TInsert;
		INSERT INTO ChartAnnotation(
			TemplateTabChartConfigurationId
			,AnnotationLineStartXCoOrdinate
			,AnnotationLineEndXCoOrdinate
			,AnnotationLineStartYCoOrdinate
			,AnnotationLineEndYCoOrdinate
			,AnnotationLineThicknessId
			,AnnotationLineColour
			,AnnotationLineStyleId
			,AnnotationText
			,AnnotationTextStartXCoOrdinate
			,AnnotationTextStartYCoOrdinate
			,IsDeleted
			,CreatedBy
			,CreatedOn)
			SELECT
			TemplateTabChartConfigurationId
			,AnnotationLineStartXCoOrdinate
			,AnnotationLineEndXCoOrdinate
			,AnnotationLineStartYCoOrdinate
			,AnnotationLineEndYCoOrdinate
			,AnnotationLineThicknessId
			,AnnotationLineColour
			,AnnotationLineStyleId
			,AnnotationText
			,AnnotationTextStartXCoOrdinate
			,AnnotationTextStartYCoOrdinate
			,0
			,@UserId
			,GETUTCDATE()
			FROM @ChartAnnotationTable
			--WHERE ISNULL(Id,0)=0

			SELECT 0 AS Id,'Success' As TransactionMessage,1 As IsSuccess;
	--COMMIT TRANSACTION TInsert;
END TRY

BEGIN CATCH
	--ROLLBACK TRANSACTION TInsert;
			SELECT   
				 0 as Id		
				,ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess
				,ERROR_NUMBER() AS Number  
				,ERROR_SEVERITY() AS Severity  
				,ERROR_STATE() AS State  
				,ERROR_LINE () AS LineNumber		  
				,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
END CATCH;

END
