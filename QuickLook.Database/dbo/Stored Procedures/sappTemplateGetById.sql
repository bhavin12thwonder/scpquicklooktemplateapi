﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateGetById] 
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT T.Id,
		T.[Name],
			TTMM.TestModeId,
			TTMM.TemplateId,
			TT.[Name] AS TestType,
			TM.TestTypeId 
		FROM TemplateTestModeMap(NOLOCK) TTMM
		INNER JOIN Template T on T.id = TTMM.TemplateId
		INNER JOIN TestType TT on TT.Id = T.TestTypeId
		LEFT JOIN TestMode(NOLOCK) TM ON TTMM.TestModeId=TM.Id
		WHERE TTMM.IsDeleted=0 AND TTMM.TemplateId=@Id
END
