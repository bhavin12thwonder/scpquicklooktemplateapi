﻿CREATE PROCEDURE [dbo].[sappTemplatePreviewById]
@Id int
AS
BEGIN

SET NOCOUNT ON;

		SELECT 
			 TT.Id
			,TT.TemplateId
			,TT.[Name] AS TemplateTabName
		FROM TemplateTabs(NOLOCK) TT
		WHERE TT.IsDeleted=0 AND TT.IsParentDisabled=0 AND TT.TemplateId=@Id

		-- Tab Configuration
		SELECT 
			 TTC.Id
			,TTC.TemplateTabId
			,TTC.RowId
			,TTC.ColumnId
			,TTC.ColumnSpan
			,TTC.DisplayFormatTypeId
			,TabTableConfigurationId=ISNULL((SELECT TOP 1 Id FROM TemplateTabTableConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),0)
			,TabTableConfigurationName=ISNULL((SELECT TOP 1 [Name] FROM TemplateTabTableConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),'')
			,TabChartConfigurationId=ISNULL((SELECT TOP 1 Id FROM TemplateTabChartConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),0)
			,TabChartConfigurationName=ISNULL((SELECT TOP 1 [Name] FROM TemplateTabChartConfiguration(NOLOCK) WHERE TemplateTabConfigId=TTC.Id AND IsDeleted=0 AND IsParentDisabled=0),'')
			,TTC.IsDeleted
		FROM TemplateTabConfiguration(NOLOCK) TTC
		INNER JOIN TemplateTabs(NOLOCK) TT ON TTC.TemplateTabId=TT.Id AND TT.IsDeleted=0 AND TT.IsParentDisabled=0
		WHERE TTC.IsDeleted=0 AND TTC.IsParentDisabled=0 AND TT.TemplateId=@Id
		ORDER BY TTC.TemplateTabId

		-- TabTable Configuration
		SELECT TTCD.Id
			  ,TTCD.TemplateTabTableConfigurationId
			  ,TTTC.[Name] AS TemplateTabTableConfigurationName
			  ,TTCD.RowId
			  ,TTCD.RowSpan
			  ,TTCD.ColumnId
			  ,TTCD.ColumnSpan
			  ,TTCD.ColumnWidth
			  ,TTCD.ColumnType
			  ,TTCD.DbColumn
			  ,CASE WHEN TTCD.IsHeader=1 THEN TTCD.UserDefinedColumn END ColumnHeader
			  ,CASE WHEN TTCD.IsUserDefined=1 THEN TTCD.UserDefinedColumn
			  		WHEN TTCD.IsDbColumn=1 THEN TTCD.ATDPosition+'-'+TTCD.DbColumn--+'-'+TTCD.ValueType
			  		WHEN TTCD.IsConditional=1 THEN TTCD.ConditionalUserDefinedColumn+FO.Operator+TTCD.UserDefinedColumn+'='+ISNULL(TTCD.ResultColumn,TTCD.ConditionalUserDefinedColumn)
			  		ELSE TTCD.UserDefinedColumn END ColumnData
			  ,TTCD.ColourCode
			  ,TTCD.IsHeader
			  ,TTCD.IsDeleted
		FROM TemplateTabTableConfigurationDetails(NOLOCK) TTCD
		INNER JOIN TemplateTabTableConfiguration(NOLOCK) TTTC ON TTCD.TemplateTabTableConfigurationId=TTTC.Id
		INNER JOIN TemplateTabConfiguration(NOLOCK) TTC ON TTC.Id=TTTC.TemplateTabConfigId		
		INNER JOIN TemplateTabs(NOLOCK) TT ON TTC.TemplateTabId=TT.Id AND TT.IsDeleted=0 AND TT.IsParentDisabled=0
		LEFT JOIN FormulaOperator(NOLOCK) FO ON TTCD.FormulaOperatorId=FO.Id
		WHERE TTCD.IsParentDisabled=0 AND TT.TemplateId=@Id AND TTCD.IsConfigHeader=0
		ORDER BY TTCD.TemplateTabTableConfigurationId,TTCD.RowId,TTCD.ColumnId


		--TabChart Configuration
		SELECT	TTCD.Id
				,TTCC.ChartTypeId
				,CT.ChartTypeValue
				,TTCD.TemplateTabChartConfigurationId
				,TTCC.[Name] AS TemplateTabChartConfigurationName
				,TTCD.[X-Axis-ChannelId] AS XAxisChannelId
				,XAC.[Name] AS XAxisChannel
				,TTCD.[X-Axis-Units] AS XAxisUnits
				,TTCD.[X-Axis-MinValue] AS XAxisMinValue
				,TTCD.[X-Axis-MaxValue] AS XAxisMaxValue
				,TTCD.[X-Axis-Interval] AS XAxisInterval
				,TTCD.[X-Axis-Label] AS XAxisLabel
				,TTCD.[Y-Axis-Units] AS YAxisUnits
				,TTCD.[Y-Axis-MinValue] AS YAxisMinValue
				,TTCD.[Y-Axis-MaxValue] AS YAxisMaxValue
				,TTCD.[Y-Axis-Interval] AS YAxisInterval
				,TTCD.[Y-Axis-Label] AS YAxisLabel
				,CC.Id AS ChartChannelId
				,CC.[Name] AS ChartChannelName
				,CC.ChartLineColour
				,CC.ChartLineStyleId
				,CLS.[Name] AS LineStyleName
				,CLS.[StyleValue] AS LineStyleValue
				,CC.ChartLineThicknessId
				,CLT.[Name] AS LineThickness
				,CC.LegendName
				,CC.GoodValue
				,CC.AcceptableValue
				,CC.MarginalValue
				,CC.PoorValue
				,CC.ATDPosition
				,AP.[Name] AS ATDPositionValue
				,CC.FillArea
				,CC.Polarity
				,CC.IsLegendVisible
				,CA.Id AS ChartAnnotationId
				,CA.AnnotationLineStartXCoOrdinate
				,CA.AnnotationLineEndXCoOrdinate
				,CA.AnnotationLineStartYCoOrdinate
				,CA.AnnotationLineEndYCoOrdinate
				,CA.AnnotationLineThicknessId
				,CA.AnnotationLineColour
				,CA.AnnotationLineStyleId
				,CA.AnnotationText
				,CA.AnnotationTextStartXCoOrdinate
				,CA.AnnotationTextStartYCoOrdinate
				,TTCD.IsDeleted
		FROM TemplateTabChartConfigurationDetails(NOLOCK) TTCD
		INNER JOIN TemplateTabChartConfiguration(NOLOCK) TTCC ON TTCD.TemplateTabChartConfigurationId=TTCC.Id
		INNER JOIN TemplateTabConfiguration(NOLOCK) TTC ON TTC.Id=TTCC.TemplateTabConfigId		
		INNER JOIN TemplateTabs(NOLOCK) TT ON TTC.TemplateTabId=TT.Id AND TT.IsDeleted=0 AND TT.IsParentDisabled=0
		INNER JOIN ChartType(NOLOCK) CT ON CT.Id=TTCC.ChartTypeId
		LEFT JOIN ChartChannel(NOLOCK) CC ON TTCC.Id=CC.TemplateTabChartConfigurationId AND CC.IsDeleted=0
		LEFT JOIN ChartChannel(NOLOCK) XAC ON TTCC.Id=XAC.TemplateTabChartConfigurationId AND XAC.IsDeleted=0 AND TTCD.[X-Axis-ChannelId]=XAC.Id
		LEFT JOIN ChartLineStyle(NOLOCK) CLS ON CC.ChartLineStyleId=CLS.Id
		LEFT JOIN ChartLineThickness(NOLOCK) CLT ON CC.ChartLineThicknessId=CLT.Id
		LEFT JOIN ChartAnnotation(NOLOCK) CA ON TTCC.Id=CA.TemplateTabChartConfigurationId AND CA.IsDeleted=0
		LEFT JOIN ATDPosition(NOLOCK) AP ON AP.Id=CC.ATDPosition AND AP.IsDeleted=0
		WHERE TTCD.IsParentDisabled=0 AND TT.TemplateId=@Id
		ORDER BY TTCD.TemplateTabChartConfigurationId
END
