﻿CREATE PROCEDURE [dbo].[sappGetTestModeIds]
	@TemplateId int
AS
	SET NOCOUNT ON;
		SELECT TTMM.Id,
			TTMM.TestModeId,
			TTMM.TemplateId,
			T.[Name] ,
			TM.[Name] as TestModeName
		FROM TemplateTestModeMap (NOLOCK) TTMM
		LEFT JOIN Template(NOLOCK) T ON TTMM.TemplateId=T.Id
		LEFT JOIN TestMode (NOLOCK) TM ON TTMM.TestModeId=TM.Id
		WHERE TTMM.IsDeleted=0 AND TTMM.TemplateId=@TemplateId
