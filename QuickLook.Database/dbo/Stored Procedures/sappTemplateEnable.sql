﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateEnable] 
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TDelete;

	-- UPDATE TEMPLATETAB CONFIGURATION FOR THE TEMPLATE TABS
		UPDATE ttc set IsParentDisabled=0
		FROM Template t
		INNER JOIN TemplateTabs tt ON t.Id=tt.TemplateId
		INNER JOIN TemplateTabConfiguration ttc ON tt.Id=ttc.TemplateTabId
		WHERE tt.TemplateId=@Id

		-- UPDATE TEMPLATETAB FOR THE TEMPLATE 
		UPDATE tt set IsParentDisabled=0
		FROM Template t
		INNER JOIN TemplateTabs tt ON t.Id=tt.TemplateId
		WHERE tt.TemplateId=@Id

		-- UPDATE TEMPLATE 
		UPDATE Template set
				IsDeleted=0
		WHERE id=@id

			SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TDelete;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TDelete;
			SELECT 
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
