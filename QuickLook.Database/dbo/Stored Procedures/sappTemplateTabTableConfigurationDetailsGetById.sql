﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sappTemplateTabTableConfigurationDetailsGetById
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,TemplateTabTableConfigurationId
				,RowId
				,RowSpan
				,ColumnId
				,ColumnSpan
				,ColumnWidth
				,ColumnType
				,UserDefinedColumn
				,DbColumn
				,ValueType
				,ATDPosition
				,ConditionalUserDefinedColumn
				,ResultColumn
				,ColourCode
				,IsConfigHeader
				,IsHeader
				,IsDbColumn
				,IsUserDefined
				,IsConditional
				,IsReadOnly
				,IsCalculated
				,FormulaOperatorId
				,TemplateTabTableConfigurationFormulaId
				,IsDeleted
				,LastUpdatedBy=ISNULL(ModifiedBy,CreatedBy)
				,LastUpdatedOn=ISNULL(ModifiedOn,CreatedOn)
		FROM TemplateTabTableConfigurationDetails
		WHERE IsDeleted=0 AND IsParentDisabled=0 and Id=@Id
END
