﻿CREATE PROCEDURE [dbo].[sappTemplateTabTableConfigurationNameAvailability]
@TabConfigId int,
@Name nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	DECLARE @IsExists NVARCHAR(5);
	IF EXISTS(SELECT 1 FROM TemplateTabTableConfiguration(NOLOCK) WHERE TemplateTabConfigId=@TabConfigId AND IsDeleted=0)
	BEGIN
		IF EXISTS(SELECT 1 FROM TemplateTabTableConfiguration(NOLOCK) WHERE [Name]=@Name AND IsDeleted=0)
		BEGIN
			SET @IsExists='Yes';
		END
		SET @IsExists='Yes';
	END
	ELSE
	BEGIN
		SET @IsExists='No';
	END

	SELECT @IsExists AS IsExists
END