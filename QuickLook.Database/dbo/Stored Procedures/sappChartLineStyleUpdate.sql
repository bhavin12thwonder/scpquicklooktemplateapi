﻿CREATE PROCEDURE [dbo].[sappChartLineStyleUpdate]
@Id int,
@Name	nvarchar(50),
@Style	nvarchar(50),
@StyleValue	nvarchar(50),
@IsDeleted	bit=0,
@UserId	int=1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TUpdate;
		UPDATE ChartLineStyle set
				[Name]=@Name,
				Style=@Style,
				StyleValue=@StyleValue,
				ModifiedBy=@UserId,
				ModifiedOn=GETUTCDATE()
		WHERE id=@id

			SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT 
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END