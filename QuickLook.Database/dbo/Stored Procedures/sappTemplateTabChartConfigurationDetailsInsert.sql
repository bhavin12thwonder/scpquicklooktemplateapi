﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabChartConfigurationDetailsInsert]
@TemplateTabChartConfigurationId int
,@TemplateTabChartConfigurationName nvarchar(50)
,@XAxisChannelId int
,@XAxisUnits nvarchar(50)
,@XAxisMinValue FLOAT
,@XAxisMaxValue FLOAT
,@XAxisInterval FLOAT
,@XAxisLabel nvarchar(50)
,@XAxisAutoFill BIT
,@YAxisUnits nvarchar(50)
,@YAxisMinValue FLOAT
,@YAxisMaxValue FLOAT
,@YAxisInterval FLOAT
,@YAxisLabel nvarchar(50)
,@YAxisAutoFill BIT
,@ChartAnnotationTable ChartAnnotationTable Readonly
--,@ChartChannelTable ChartChannelTable Readonly
,@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TInsert;
		INSERT INTO TemplateTabChartConfigurationDetails(
			TemplateTabChartConfigurationId
			,[X-Axis-ChannelId]
			,[X-Axis-Units]
			,[X-Axis-MinValue]
			,[X-Axis-MaxValue]
			,[X-Axis-Interval]
			,[X-Axis-Label]
			,[X-Axis-AutoFill]
			,[Y-Axis-Units]
			,[Y-Axis-MinValue]
			,[Y-Axis-MaxValue]
			,[Y-Axis-Interval]
			,[Y-Axis-Label]
			,[Y-Axis-AutoFill]
			,IsDeleted
			,CreatedBy
			,CreatedOn)
			VALUES(
			@TemplateTabChartConfigurationId
			,@XAxisChannelId
			,@XAxisUnits
			,@XAxisMinValue
			,@XAxisMaxValue
			,@XAxisInterval
			,@XAxisLabel
			,@XAxisAutoFill
			,@YAxisUnits
			,@YAxisMinValue
			,@YAxisMaxValue
			,@YAxisInterval
			,@YAxisLabel
			,@YAxisAutoFill
			,0
			,@UserId
			,GETUTCDATE());
			
			EXEC sappChartAnnotationInsert @ChartAnnotationTable,@UserId
			--EXEC sappChartChannelInsert @ChartChannelTable,@UserId

			UPDATE TemplateTabChartConfiguration SET Name = @TemplateTabChartConfigurationName WHERE Id=@TemplateTabChartConfigurationId

			SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TInsert;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TInsert;
			SELECT 	
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
GO


