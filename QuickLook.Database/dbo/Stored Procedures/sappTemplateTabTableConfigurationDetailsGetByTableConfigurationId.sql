﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sappTemplateTabTableConfigurationDetailsGetByTableConfigurationId
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT TTCD.Id
				,TTC.[Name] As TemplateTabTableConfigurationName
				,TTCD.TemplateTabTableConfigurationId
				,TTCD.RowId
				,TTCD.RowSpan
				,TTCD.ColumnId
				,TTCD.ColumnSpan
				,TTCD.ColumnWidth
				,TTCD.ColumnType
				,TTCD.UserDefinedColumn
				,TTCD.DbColumn
				,TTCD.ValueType
				,TTCD.ATDPosition
				,TTCD.ConditionalUserDefinedColumn
				,TTCD.ResultColumn
				,TTCD.ColourCode
				,TTCD.IsConfigHeader
				,TTCD.IsHeader
				,TTCD.IsDbColumn
				,TTCD.IsUserDefined
				,TTCD.IsConditional
				,TTCD.IsReadOnly
				,TTCD.IsCalculated
				,TTCD.FormulaOperatorId
				,TTCD.TemplateTabTableConfigurationFormulaId
				,TTCD.IsDeleted				
				,LastUpdatedBy=ISNULL(TTCD.ModifiedBy,TTCD.CreatedBy)
				,LastUpdatedOn=ISNULL(TTCD.ModifiedOn,TTCD.CreatedOn)
		FROM TemplateTabTableConfigurationDetails(NOLOCK) TTCD
		INNER JOIN TemplateTabTableConfiguration(NOLOCK) TTC ON TTCD.TemplateTabTableConfigurationId=TTC.Id
		WHERE TTCD.IsParentDisabled=0 and TTCD.TemplateTabTableConfigurationId=@Id
		ORDER BY TTCD.RowId,TTCD.ColumnId
END
