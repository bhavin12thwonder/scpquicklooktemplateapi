﻿CREATE PROCEDURE [dbo].[sappChartChannelGetByChartConfigId]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT	CC.Id
				,CC.TemplateTabChartConfigurationId
				,CC.[Name]
				,CC.ChartLineColour
				,CC.ChartLineStyleId
				,CC.ChartLineThicknessId
				,CC.LegendName
				,CC.GoodValue
				,CC.AcceptableValue
				,CC.MarginalValue
				,CC.PoorValue				
				,CC.ATDPosition
				,CC.FillArea
				,CC.Polarity
				,CC.IsLegendVisible
				,IsAreaChart=CASE WHEN (SELECT TOP 1 ChartTypeId FROM TemplateTabChartConfiguration(NOLOCK)TTCC WHERE TTCC.Id=CC.TemplateTabChartConfigurationId)=3 THEN 1 ELSE 0 END
				,CC.IsDeleted
				,CC.CreatedBy
				,CC.CreatedOn
				,CC.ModifiedBy
				,CC.ModifiedOn
				,XAxisChannelId = (SELECT ID FROM TemplateTabChartConfigurationDetails WHERE [X-Axis-ChannelId] = CC.Id)
				,CC.SiUnit
		FROM ChartChannel CC
		WHERE CC.IsDeleted=0 AND TemplateTabChartConfigurationId=@Id
		ORDER BY CC.Id DESC

END
