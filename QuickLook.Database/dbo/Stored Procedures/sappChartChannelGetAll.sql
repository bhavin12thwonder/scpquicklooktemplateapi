﻿CREATE PROCEDURE [dbo].[sappChartChannelGetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,TemplateTabChartConfigurationId
				,[Name]
				,ChartLineColour
				,ChartLineStyleId
				,ChartLineThicknessId
				,LegendName
				,GoodValue
				,AcceptableValue
				,MarginalValue
				,PoorValue
				,ATDPosition
				,FillArea
				,Polarity
				,IsLegendVisible
				,SiUnit
				,IsDeleted
				,CreatedBy
				,CreatedOn
				,ModifiedBy
				,ModifiedOn 
		FROM ChartChannel
		WHERE IsDeleted=0
END
