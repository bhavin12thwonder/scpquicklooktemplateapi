﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabChartConfigurationDetailsUpdate]
@Id int,
@TemplateTabChartConfigurationId int
,@TemplateTabChartConfigurationName nvarchar(50)
--,@XAxisChannelId int = null
,@XAxisUnits nvarchar(50)
,@XAxisMinValue FLOAT
,@XAxisMaxValue FLOAT
,@XAxisInterval FLOAT
,@XAxisLabel nvarchar(50)
,@XAxisAutoFill BIT
,@YAxisUnits nvarchar(50)
,@YAxisMinValue FLOAT
,@YAxisMaxValue FLOAT
,@YAxisInterval FLOAT
,@YAxisLabel nvarchar(50)
,@YAxisAutoFill BIT
,@ChartAnnotationTable ChartAnnotationTable Readonly
--,@ChartChannelTable ChartChannelTable Readonly
,@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TUpdate;
		UPDATE TemplateTabChartConfigurationDetails SET
				 TemplateTabChartConfigurationId=@TemplateTabChartConfigurationId
				--,[X-Axis-ChannelId]=@XAxisChannelId
				,[X-Axis-Units]=@XAxisUnits
				,[X-Axis-MinValue]=@XAxisMinValue
				,[X-Axis-MaxValue]=@XAxisMaxValue
				,[X-Axis-Interval]=@XAxisInterval
				,[X-Axis-Label]=@XAxisLabel
				,[X-Axis-AutoFill]=@XAxisAutoFill
				,[Y-Axis-Units]=@YAxisUnits
				,[Y-Axis-MinValue]=@YAxisMinValue
				,[Y-Axis-MaxValue]=@YAxisMaxValue
				,[Y-Axis-Interval]=@YAxisInterval
				,[Y-Axis-Label]=@YAxisLabel
				,[Y-Axis-AutoFill]=@YAxisAutoFill
				,ModifiedBy=@UserId
				,ModifiedOn=GETUTCDATE()
			WHERE Id=@Id

			EXEC sappChartAnnotationInsert @ChartAnnotationTable,@UserId
			--EXEC sappChartChannelUpdate @ChartChannelTable,@UserId

			UPDATE TemplateTabChartConfiguration SET ModifiedBy=@UserId,ModifiedOn=GETUTCDATE(),
			Name = @TemplateTabChartConfigurationName WHERE Id=@TemplateTabChartConfigurationId


			SELECT @Id AS Id,'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT   
				 0 as Id		
				,ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess
				,ERROR_NUMBER() AS Number  
				,ERROR_SEVERITY() AS Severity  
				,ERROR_STATE() AS State  
				,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
GO


