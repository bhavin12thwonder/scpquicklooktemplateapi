﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sappTemplateTabChartConfigurationGetByAll
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,TemplateTabConfigId
				,ChartTypeId
				,[ChartOrientation]=ISNULL (ChartOrientation,'v')
				,[Name]
				,IsDeleted
				,LastUpdatedBy=ISNULL(ModifiedBy,CreatedBy)
				,LastUpdatedOn=ISNULL(ModifiedOn,CreatedOn)
		FROM TemplateTabChartConfiguration
		WHERE IsDeleted=0 AND IsParentDisabled=0
END
