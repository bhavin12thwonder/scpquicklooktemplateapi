﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sappTemplateTabChartConfigurationDetailsGetByChartConfigurationId
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT TTCD.Id
				,TTC.[Name] As TemplateTabChartConfigurationName
				,TTCD.TemplateTabChartConfigurationId
				,TTCD.[X-Axis-ChannelId] AS XAxisChannelId
				,TTCD.[X-Axis-Units] AS XAxisUnits
				,TTCD.[X-Axis-MinValue] AS XAxisMinValue
				,TTCD.[X-Axis-MaxValue] AS XAxisMaxValue
				,TTCD.[X-Axis-Interval] AS XAxisInterval
				,TTCD.[X-Axis-Label] AS XAxisLabel
				,TTCD.[X-Axis-AutoFill] AS XAxisAutoFill
				,TTCD.[Y-Axis-Units] AS YAxisUnits
				,TTCD.[Y-Axis-MinValue] AS YAxisMinValue
				,TTCD.[Y-Axis-MaxValue] AS YAxisMaxValue
				,TTCD.[Y-Axis-Interval] AS YAxisInterval
				,TTCD.[Y-Axis-Label] AS YAxisLabel
				,TTCD.[Y-Axis-AutoFill] AS YAxisAutoFill
				,TTCD.IsDeleted				
				,LastUpdatedBy=ISNULL(TTCD.ModifiedBy,TTCD.CreatedBy)
				,LastUpdatedOn=ISNULL(TTCD.ModifiedOn,TTCD.CreatedOn)
		FROM TemplateTabChartConfigurationDetails(NOLOCK) TTCD
		INNER JOIN TemplateTabChartConfiguration(NOLOCK) TTC ON TTCD.TemplateTabChartConfigurationId=TTC.Id
		WHERE TTCD.IsParentDisabled=0 and TTCD.TemplateTabChartConfigurationId=@Id
END
