﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabsDelete] 
@TemplateId int,
@TabIndex int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TDelete;
	DECLARE @Id INT,@TabIndexIn INT=0;
	SET @Id=(SELECT DISTINCT Id FROM TemplateTabs(NOLOCK) WHERE TemplateId=@TemplateId AND TabIndex=@TabIndex AND IsDeleted=0);

		UPDATE TemplateTabConfiguration set
				IsParentDisabled=1
		WHERE TemplateTabId=@id

		UPDATE TemplateTabs set
				IsDeleted=1
		WHERE id=@id

		;WITH ReOrderTabIndexCTE
		As (
		SELECT Id,[Name],(ROW_NUMBER() OVER(ORDER BY Id ASC)-1) TabIndex from TemplateTabs where TemplateId=@TemplateId AND IsDeleted=0
		)
		
		UPDATE tt set TabIndex=cte.Tabindex
		FROM TemplateTabs tt
		INNER JOIN ReOrderTabIndexCTE cte on tt.id=cte.id
		WHERE tt.TemplateId=@TemplateId AND tt.IsDeleted=0
	
	SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TDelete;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TDelete;
			SELECT 
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
