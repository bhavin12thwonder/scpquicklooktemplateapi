﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabConfigurationGetById]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
			,TemplateTabId
			,RowId
			,ColumnId
			,ColumnSpan
			,DisplayFormatTypeId
			,IsDeleted
			,CreatedBy
			,CreatedOn
			,ModifiedBy
			,ModifiedOn
		FROM TemplateTabConfiguration
		WHERE IsDeleted=0 AND IsParentDisabled=0 AND Id=@Id
END
