﻿CREATE PROCEDURE [dbo].[sappChartLineStyleGetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,[Name]
				,Style
				,StyleValue
				,IsDeleted
				,CreatedBy
				,CreatedOn
				,ModifiedBy
				,ModifiedOn 
		FROM ChartLineStyle
		WHERE IsDeleted=0
END
