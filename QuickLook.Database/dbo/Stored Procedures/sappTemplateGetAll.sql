﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateGetAll] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT T.Id
				,T.[Name]
				,T.TestTypeId
				,TT.[Name] As TestType
				,T.IsDeleted
		FROM Template(NOLOCK) T
		INNER JOIN TestType(NOLOCK) TT ON T.TestTypeId=TT.Id
		INNER JOIN TemplateTestModeMap(NOLOCK)  TTMM ON T.Id=TTMM.TemplateId
		INNER JOIN TestMode(NOLOCK) TM ON TM.Id=TTMM.TestModeId
		 GROUP BY T.Id,T.[Name]
				,T.TestTypeId
				,TT.[Name]
				,T.IsDeleted
END
