﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabTableConfigurationDetailsInsert]
@TemplateTabTableConfigurationDetails TemplateTabTableConfigurationDetailsTable Readonly,
@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TInsert;
		INSERT INTO TemplateTabTableConfigurationDetails(
			TemplateTabTableConfigurationId
			,RowId
			,RowSpan
			,ColumnId
			,ColumnSpan
			,ColumnWidth
			,ColumnType
			,UserDefinedColumn
			,DbColumn
			,ValueType
			,ATDPosition
			,ConditionalUserDefinedColumn
			,ResultColumn
			,ColourCode
			,IsConfigHeader
			,IsHeader
			,IsDbColumn
			,IsUserDefined
			,IsConditional
			,IsReadOnly
			,FormulaOperatorId
			--,TemplateTabTableConfigurationFormulaId
			,IsDeleted
			,CreatedBy
			,CreatedOn)
				
			SELECT 
			TemplateTabTableConfigurationId
			,RowId
			,RowSpan
			,ColumnId
			,ColumnSpan
			,ColumnWidth
			,ColumnType
			,UserDefinedColumn
			,DbColumn
			,ValueType
			,ATDPosition
			,ConditionalUserDefinedColumn
			,ResultColumn
			,ColourCode
			,IsConfigHeader
			,IsHeader			
			,IsDbColumn
			,IsUserDefined
			,IsConditional
			,IsReadOnly
			,FormulaOperatorId
			--,TemplateTabTableConfigurationFormulaId
			,IsDeleted
			,@UserId
			,GETUTCDATE()
			FROM @TemplateTabTableConfigurationDetails

			UPDATE TTTC SET Name = TEMP.TemplateTabTableConfiguratioName
			FROM TemplateTabTableConfiguration(NOLOCK) TTTC
			INNER JOIN @TemplateTabTableConfigurationDetails TEMP ON TTTC.Id=TEMP.TemplateTabTableConfigurationId

			SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TInsert;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TInsert;
			SELECT 	
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
GO


