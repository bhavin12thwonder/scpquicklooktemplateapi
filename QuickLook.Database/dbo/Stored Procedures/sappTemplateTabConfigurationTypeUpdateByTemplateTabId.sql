﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabConfigurationTypeUpdateByTemplateTabId]
@Id int,
@TemplateTabConfigurationType int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	BEGIN TRY
	BEGIN TRANSACTION TUpdate;
	
	IF(@TemplateTabConfigurationType=1)
	BEGIN		
		IF EXISTS(SELECT TOP 1 1 FROM TemplateTabTableConfiguration(NOLOCK) WHERE TemplateTabConfigId=@Id AND IsDeleted=0 AND IsParentDisabled=0)
		BEGIN

			UPDATE TemplateTabTableConfigurationDetails set
					IsParentDisabled=1
			WHERE TemplateTabTableConfigurationId=@id

			UPDATE TemplateTabTableConfiguration set
					IsDeleted=1
			WHERE TemplateTabConfigId=@id

			UPDATE TemplateTabConfiguration set
					DisplayFormatTypeId=2
			WHERE Id=@id
		END
	END
	ELSE 
	IF(@TemplateTabConfigurationType=2)
	BEGIN		
		IF EXISTS(SELECT TOP 1 1 FROM TemplateTabChartConfiguration(NOLOCK) WHERE TemplateTabConfigId=@Id AND IsDeleted=0 AND IsParentDisabled=0)
		BEGIN

			UPDATE TemplateTabChartConfigurationDetails set
					IsParentDisabled=1
			WHERE TemplateTabChartConfigurationId=@id

			UPDATE TemplateTabChartConfiguration set
					IsDeleted=1
			WHERE TemplateTabConfigId=@id

			UPDATE TemplateTabConfiguration set
					DisplayFormatTypeId=1
			WHERE Id=@id

		END
	END

	SELECT @Id As Id,'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT 
				0 As Id,
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
