﻿CREATE PROCEDURE [dbo].[sappATDPositionGetByChartChannelId]
	@ChartChannelId int
AS
	SET NOCOUNT ON;
		SELECT APCM.Id,
			APCM.ATDPositionId,
			APCM.ChartChannelId,
			CC.[Name] AS ChannelName,
			AP.ATDPositionValue,
			AP.[Name] as ATDPositionName
		FROM ATDPositionChartChannelMap(NOLOCK) APCM
		LEFT JOIN ChartChannel(NOLOCK) CC ON APCM.ChartChannelId=CC.Id
		LEFT JOIN ATDPosition(NOLOCK) AP ON APCM.ATDPositionId=AP.Id
		WHERE APCM.IsDeleted=0 AND APCM.ChartChannelId=@ChartChannelId