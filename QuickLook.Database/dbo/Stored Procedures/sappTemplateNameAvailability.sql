﻿CREATE PROCEDURE [dbo].[sappTemplateNameAvailability]
@TemplateId int,
@Name nvarchar(50),
@TestType int,
@TestMode dbo.UniqueIdsTable READONLY
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @IsExists NVARCHAR(5);
	DECLARE @p1 dbo.UniqueIdsTable
	INSERT INTO @p1 select TestModeId from TemplateTestModeMap where TemplateId = @TemplateId

	IF (@TemplateId > 0)
	BEGIN
		IF EXISTS(SELECT 1 FROM Template T 
		LEFT JOIN TemplateTestModeMap TM on TM.TemplateId = T.Id
		WHERE T.[Name]=@Name AND T.TestTypeId=@TestType AND T.IsDeleted=0 AND 	
		TM.TestModeId IN(select Id from @TestMode where Id not in (select Id from @p1)))
	BEGIN
		SET @IsExists='Yes';
	END
	ELSE
	BEGIN
		SET @IsExists='No';
	END
	END
	ELSE
		IF EXISTS(SELECT 1 FROM Template T 
		LEFT JOIN TemplateTestModeMap TM on TM.TemplateId = T.Id
		WHERE T.TestTypeId=@TestType AND T.IsDeleted=0 AND 	
		TM.TestModeId IN(select Id from @TestMode where Id not in (select Id from @p1)))
	BEGIN
		SET @IsExists='Yes';
	END
	ELSE
	BEGIN
		SET @IsExists='No';
	END
	SELECT @IsExists AS IsExists
END