﻿CREATE PROCEDURE [dbo].[sappTemplateTabChartConfigurationNameAvailability]
@TabConfigId int,
@ChartTypeId int,
@Name nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	DECLARE @IsExists NVARCHAR(5);
	IF EXISTS(SELECT 1 FROM TemplateTabChartConfiguration(NOLOCK) WHERE ChartTypeId=@ChartTypeId AND [Name]=@Name AND IsDeleted=0)
		BEGIN
			SET @IsExists='Yes';
		END
	ELSE
	BEGIN
		SET @IsExists='No';
	END

	SELECT @IsExists AS IsExists
END