﻿CREATE PROCEDURE [dbo].[sappChartChannelUpdate]
--@ChartChannelTable ChartChannelTable Readonly
@Id	int,
@TemplateTabChartConfigurationId	int,
@Name	nvarchar(50),
@ChartLineColour	nvarchar(50),
@ChartLineStyleId	int,
@ChartLineThicknessId	int,
@LegendName	nvarchar(50),
@GoodValue int,
@AcceptableValue int,
@MarginalValue int,
@PoorValue int,
@ATDPosition int,
@ATDPositionIds dbo.UniqueIdsTable READONLY,
@FillArea	bit,
@Polarity	bit,
@IsLegendVisible	bit,
@SiUnit nvarchar(50) = null,
@IsDeleted	bit,
@UserId	int=1
--,@TemplateTabChartConfigurationId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;

	BEGIN TRY
	BEGIN TRANSACTION TUpdate;	
		DECLARE @ChartTypeId INT;

		SET @ChartTypeId=(SELECT ChartTypeId FROM TemplateTabChartConfiguration(NOLOCK) WHERE Id=@TemplateTabChartConfigurationId)
		IF(@ChartTypeId=4)
		BEGIN
			SET @ATDPosition=NULL;
		END

		UPDATE ChartChannel set
				TemplateTabChartConfigurationId=@TemplateTabChartConfigurationId
				,[Name]=@Name
				,ChartLineColour=@ChartLineColour
				,ChartLineStyleId=@ChartLineStyleId
				,ChartLineThicknessId=@ChartLineThicknessId
				,LegendName=@LegendName
				,GoodValue=@GoodValue
				,AcceptableValue=@AcceptableValue
				,MarginalValue=@MarginalValue
				,PoorValue=@PoorValue
				,ATDPosition=@ATDPosition
				,FillArea=@FillArea
				,Polarity=@Polarity
				,IsLegendVisible=@IsLegendVisible
				,SiUnit = @SiUnit
				,ModifiedBy=@UserId
				,ModifiedOn=GETUTCDATE()
				WHERE Id=@Id
				--FROM ChartChannel CC
				--INNER JOIN @ChartChannelTable temp ON CC.Id=temp.Id --AND CC.TemplateTabChartConfigurationId=@TemplateTabChartConfigurationId

			IF(@ChartTypeId=4)
			BEGIN
				IF EXISTS(SELECT 1 FROM @ATDPositionIds HAVING COUNT(Id)>0)
				BEGIN
				--TODO Update the no.of records available in table for the ChartChannelId and remove extra records. Instead of delete and insert.
					DELETE FROM ATDPositionChartChannelMap WHERE ChartChannelId=@Id
					INSERT INTO ATDPositionChartChannelMap(ChartChannelId,ATDPositionId,IsDeleted,CreatedBy,CreatedOn)
					SELECT @Id,Id,0,@UserId,GETUTCDATE() FROM @ATDPositionIds
				END
			END

			SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT 
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END