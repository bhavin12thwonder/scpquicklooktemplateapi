﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateUpdate] 
@Id int
,@Name	varchar(50)
,@TestTypeId	int
,@TestModeId	dbo.UniqueIdsTable READONLY
,@ModifiedBy	int=1
,@ModifiedOn	datetime=GetUtcDate
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
BEGIN TRY
	BEGIN TRANSACTION TUpdate;
		UPDATE Template
		SET Template.Name = @Name,Template.TestTypeId=@TestTypeId  
		WHERE Id = @Id;  
		
		DECLARE @TemplateId INT;
		SET @TemplateId=@Id;
		IF EXISTS(SELECT 1 FROM @TestModeId HAVING COUNT(Id)>0)
		BEGIN
			DELETE FROM TemplateTestModeMap WHERE TemplateId=@Id;
			INSERT INTO TemplateTestModeMap(TemplateId,TestModeId,IsDeleted,ModifiedBy,ModifiedOn)
			SELECT @TemplateId,Id,0,1,GETUTCDATE() FROM @TestModeId
		END

		SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT 
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END