﻿CREATE PROCEDURE [dbo].[sappChartTypeGetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,[Name]
				,ChartTypeValue
				,IsDeleted
				,CreatedBy
				,CreatedOn
				,ModifiedBy
				,ModifiedOn 
		FROM ChartType
		WHERE IsDeleted=0
END
