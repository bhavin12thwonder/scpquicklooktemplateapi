﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTestTypeGetById]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
			,[Name]
			,IsDeleted
			,CreatedBy
			,CreatedOn
			,ModifiedBy
			,ModifiedOn
		FROM TestType
		WHERE Id=@Id
END
