﻿CREATE PROCEDURE [dbo].[sappChartAnnotationGetByChartConfigId]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,TemplateTabChartConfigurationId
				,AnnotationLineStartXCoOrdinate
				,AnnotationLineEndXCoOrdinate
				,AnnotationLineStartYCoOrdinate
				,AnnotationLineEndYCoOrdinate
				,AnnotationLineThicknessId
				,AnnotationLineColour
				,AnnotationLineStyleId
				,AnnotationText
				,AnnotationTextStartXCoOrdinate
				,AnnotationTextStartYCoOrdinate
				,IsDeleted
				,CreatedBy
				,CreatedOn
				,ModifiedBy
				,ModifiedOn 
		FROM ChartAnnotation
		WHERE IsDeleted=0 AND TemplateTabChartConfigurationId=@Id
		ORDER BY Id DESC
END
