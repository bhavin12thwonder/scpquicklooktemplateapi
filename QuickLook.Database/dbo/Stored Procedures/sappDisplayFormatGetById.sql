﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sappDisplayFormatGetById
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,[Name]
				,IsDeleted
				,CreatedBy
				,CreatedOn
				,ModifiedBy
				,ModifiedOn 
		FROM DisplayFormat
		WHERE IsDeleted=0 AND Id=@Id 
END
