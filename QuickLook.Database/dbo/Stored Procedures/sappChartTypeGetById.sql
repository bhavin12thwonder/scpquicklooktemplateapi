﻿CREATE PROCEDURE [dbo].[sappChartTypeGetById]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,[Name]
				,ChartTypeValue
				,IsDeleted
				,CreatedBy
				,CreatedOn
				,ModifiedBy
				,ModifiedOn 
		FROM ChartType
		WHERE IsDeleted=0 AND Id=@Id 
END
