﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabChartConfigurationDelete] 
@Id int,
@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TDelete;

		UPDATE TemplateTabChartConfigurationDetails set
				IsParentDisabled=1,ModifiedBy=@UserId,ModifiedOn=GETUTCDATE()
		WHERE TemplateTabChartConfigurationId=@id

		UPDATE TemplateTabChartConfiguration set
				IsDeleted=1,ModifiedBy=@UserId,ModifiedOn=GETUTCDATE()
		WHERE id=@id

		SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TDelete;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TDelete;
			SELECT 
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
