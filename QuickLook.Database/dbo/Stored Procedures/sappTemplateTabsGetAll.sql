﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabsGetAll] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
			,[Name]
			,TemplateId
			,TabIndex
			,DisplayOrder
			,[Rows]
			,[Columns]
			,RowsLimit
			,IsDeleted
			,CreatedBy
			,CreatedOn
			,ModifiedBy
			,ModifiedOn
		FROM TemplateTabs
		WHERE IsDeleted=0 AND IsParentDisabled=0
		ORDER BY Id
END
