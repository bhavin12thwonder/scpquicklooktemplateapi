﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTestModeGetById]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
			,[Name]
			,TestTypeId
			,IsDeleted
			,CreatedBy
			,CreatedOn
			,ModifiedBy
			,ModifiedOn
		FROM TestMode
		WHERE IsDeleted=0 AND Id=@Id
END
