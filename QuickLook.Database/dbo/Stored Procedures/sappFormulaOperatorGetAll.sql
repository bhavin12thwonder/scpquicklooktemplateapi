﻿CREATE PROCEDURE [dbo].[sappFormulaOperatorGetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,[Name]
				,Operator
				,IsDeleted
				,CreatedOn
				,CreatedBy
				,ModifiedOn
				,ModifiedBy
		FROM FormulaOperator
		WHERE IsDeleted=0
END
