﻿CREATE PROCEDURE [dbo].[sappUserGetByUsername]
	@Username nvarchar(50)
AS
	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT [Id]
			  ,[SamAccountName]
			  ,[FirstName]
			  ,[LastName]
			  ,[MiddleName]
			  ,[EmailAddress]
			  ,[PhoneNumber]
			  ,[IsDeleted]
			  ,[CreatedDate]
			  ,[ModifiedDate]
			  ,[ManagerSamAccountName]
			  ,[Department]
		FROM Users
		WHERE IsDeleted=0 AND SamAccountName=@Username 
END
