﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sappTemplateTabChartConfigurationDetailsGetAll
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,TemplateTabChartConfigurationId
				,[X-Axis-ChannelId] AS XAxisChannelId
				,[X-Axis-Units] AS XAxisUnits
				,[X-Axis-MinValue] AS XAxisMinValue
				,[X-Axis-MaxValue] AS XAxisMaxValue
				,[X-Axis-Interval] AS XAxisInterval
				,[X-Axis-Label] AS XAxisLabel
				,[X-Axis-AutoFill] AS XAxisAutoFill
				,[Y-Axis-Units] AS YAxisUnits
				,[Y-Axis-MinValue] AS YAxisMinValue
				,[Y-Axis-MaxValue] AS YAxisMaxValue
				,[Y-Axis-Interval] AS YAxisInterval
				,[Y-Axis-Label] AS YAxisLabel
				,[Y-Axis-AutoFill] AS YAxisAutoFill
				,IsDeleted
				,LastUpdatedBy=ISNULL(ModifiedBy,CreatedBy)
				,LastUpdatedOn=ISNULL(ModifiedOn,CreatedOn)
		FROM TemplateTabChartConfigurationDetails
		WHERE IsDeleted=0 AND IsParentDisabled=0
END
