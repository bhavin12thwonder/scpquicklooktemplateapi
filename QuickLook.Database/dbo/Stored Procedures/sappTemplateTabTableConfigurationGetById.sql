﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE sappTemplateTabTableConfigurationGetById
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,TemplateTabConfigId
				,[Name]
				,IsDeleted
				,LastUpdatedBy=ISNULL(ModifiedBy,CreatedBy)
				,LastUpdatedOn=ISNULL(ModifiedOn,CreatedOn)
		FROM TemplateTabTableConfiguration
		WHERE IsDeleted=0 AND IsParentDisabled=0 AND Id=@Id 
END
