﻿CREATE PROCEDURE [dbo].[sappFormulaOperatorInsert]
@Name	nvarchar(50)
,@Operator	nvarchar(50)
,@IsDeleted	bit=0
,@CreatedBy	int=1

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TInsert;
		INSERT INTO FormulaOperator(
			[Name]
			,Operator
			,IsDeleted
			,CreatedOn
			,CreatedBy)
		VALUES(
			@Name
			,@Operator
			,@IsDeleted
			,GETUTCDATE()
			,@CreatedBy)
		SELECT SCOPE_IDENTITY() AS Id,'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TInsert;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TInsert;
			SELECT   
				 0 as Id		
				,ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  

            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
