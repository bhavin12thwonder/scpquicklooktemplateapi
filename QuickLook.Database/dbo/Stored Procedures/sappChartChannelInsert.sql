﻿CREATE PROCEDURE [dbo].[sappChartChannelInsert]
--@ChartChannelTable ChartChannelTable Readonly
@TemplateTabChartConfigurationId	int,
@Name	nvarchar(50),
@ChartLineColour	nvarchar(50),
@ChartLineStyleId	int,
@ChartLineThicknessId	int,
@LegendName	nvarchar(50),
@GoodValue int,
@AcceptableValue int,
@MarginalValue int,
@PoorValue int,
@ATDPosition int,
@ATDPositionIds dbo.UniqueIdsTable READONLY,
@FillArea	bit,
@Polarity	bit,
@IsLegendVisible	bit,
@IsDeleted	bit,
@SiUnit nvarchar(50) = null,
@UserId	int=1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	DECLARE @ChannelId INT,@ChartTypeId INT;

	BEGIN TRY
	BEGIN TRANSACTION TInsert;

		SET @ChartTypeId=(SELECT ChartTypeId FROM TemplateTabChartConfiguration(NOLOCK) WHERE Id=@TemplateTabChartConfigurationId)
		IF(@ChartTypeId=4)
		BEGIN
			SET @ATDPosition=NULL;
		END
		INSERT INTO ChartChannel(
			 TemplateTabChartConfigurationId
			,[Name]
			,ChartLineColour
			,ChartLineStyleId
			,ChartLineThicknessId
			,LegendName
			,GoodValue
			,AcceptableValue
			,MarginalValue
			,PoorValue
			,ATDPosition
			,FillArea
			,Polarity
			,IsLegendVisible
			,SiUnit
			,IsDeleted
			,CreatedBy
			,CreatedOn)
			VALUES(
			@TemplateTabChartConfigurationId
			,@Name
			,@ChartLineColour
			,@ChartLineStyleId
			,@ChartLineThicknessId
			,@LegendName
			,@GoodValue
			,@AcceptableValue
			,@MarginalValue
			,@PoorValue
			,@ATDPosition
			,@FillArea
			,@Polarity
			,@IsLegendVisible
			,@SiUnit
			,@IsDeleted
			,@UserId
			,GETUTCDATE())

			SELECT SCOPE_IDENTITY() AS Id,'Success' As TransactionMessage,1 As IsSuccess;
			IF(@ChartTypeId=4)
			BEGIN
				SET @ChannelId=SCOPE_IDENTITY();
				IF EXISTS(SELECT 1 FROM @ATDPositionIds HAVING COUNT(Id)>0)
				BEGIN
					INSERT INTO ATDPositionChartChannelMap(ChartChannelId,ATDPositionId,IsDeleted,CreatedBy,CreatedOn)
					SELECT @ChannelId,Id,0,@UserId,GETUTCDATE() FROM @ATDPositionIds
				END
			END

	COMMIT TRANSACTION TInsert;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TInsert;
			SELECT   
				 0 as Id		
				,ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;

END
