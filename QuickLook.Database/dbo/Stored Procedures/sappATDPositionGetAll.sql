﻿CREATE PROCEDURE [dbo].[sappATDPositionGetAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,[Name]
				,IsDeleted
				,CreatedBy
				,CreatedOn
				,ModifiedBy
				,ModifiedOn 
		FROM ATDPosition
		WHERE IsDeleted=0
END
