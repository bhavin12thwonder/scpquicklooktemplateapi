﻿GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabConfigurationUpdate]
@TemplateTabConfigurationTableData TemplateTabConfigurationTable Readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	declare @templateTabId int=(SELECT DISTINCT TemplateTabId FROM @TemplateTabConfigurationTableData);

	BEGIN TRY
	BEGIN TRANSACTION TUpdate;
		UPDATE ttc SET
		TemplateTabId=temp.TemplateTabId,
		RowId=temp.RowId,
		ColumnId=temp.ColumnId,		
		ColumnSpan=temp.ColumnSpan,
		DisplayFormatTypeId=temp.DisplayFormatTypeId,
		IsDeleted=temp.IsDeleted
		FROM TemplateTabConfiguration ttc
		INNER JOIN @TemplateTabConfigurationTableData temp on ttc.Id=temp.Id
		WHERE temp.Id<>0

		INSERT INTO TemplateTabConfiguration(
			TemplateTabId
			,RowId
			,ColumnId
			,ColumnSpan
			,DisplayFormatTypeId
			,IsDeleted
			)
			SELECT 
			TemplateTabId
			,RowId
			,ColumnId
			,ColumnSpan
			,DisplayFormatTypeId
			,IsDeleted
			FROM @TemplateTabConfigurationTableData
			WHERE Id=0

		SELECT @templateTabId As Id,'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT 
				0 As Id,
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
