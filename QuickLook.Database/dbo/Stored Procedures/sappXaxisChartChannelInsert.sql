﻿CREATE PROCEDURE [dbo].[sappXaxisChartChannelInsert]
@TemplateTabChartConfigurationId	int,
@Name	nvarchar(50),
@ChartLineColour	nvarchar(50),
@ChartLineStyleId	int,
@ChartLineThicknessId	int,
@LegendName	nvarchar(50),
@GoodValue int,
@AcceptableValue int,
@MarginalValue int,
@PoorValue int,
@ATDPosition int,
@ATDPositionIds dbo.UniqueIdsTable READONLY,
@FillArea	bit,
@Polarity	bit,
@IsLegendVisible	bit,
@IsDeleted	bit,
@UserId	int=1
AS
	BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	DECLARE @ChannelId INT,@ChartTypeId INT, @XaxisChannelId INT;

	BEGIN TRY
	BEGIN TRANSACTION TInsert;

		SET @ChartTypeId=(SELECT ChartTypeId FROM TemplateTabChartConfiguration(NOLOCK) WHERE Id=@TemplateTabChartConfigurationId)
		IF(@ChartTypeId=4)
		BEGIN
			SET @ATDPosition=NULL;
		END
		IF EXISTS( SELECT 1 FROM TemplateTabChartConfigurationDetails WHERE TemplateTabChartConfigurationId = @TemplateTabChartConfigurationId)
		BEGIN 
			SET @XaxisChannelId = (SELECT TOP 1 [X-Axis-ChannelId] FROM TemplateTabChartConfigurationDetails WHERE Id = @TemplateTabChartConfigurationId)
			UPDATE ChartChannel
			SET Name = @Name, LegendName = @LegendName, IsDeleted = 0, ModifiedBy = @UserId, ModifiedOn = GETUTCDATE()
			WHERE Id = @XaxisChannelId

			SELECT (SELECT TOP 1 [X-Axis-ChannelId] FROM TemplateTabChartConfigurationDetails WHERE TemplateTabChartConfigurationId = @TemplateTabChartConfigurationId) AS Id,
			'Success' As TransactionMessage,1 As IsSuccess;
		END
		ELSE 
		BEGIN
			INSERT INTO ChartChannel(
			 TemplateTabChartConfigurationId
			,[Name]
			,ChartLineColour
			,ChartLineStyleId
			,ChartLineThicknessId
			,LegendName
			,GoodValue
			,AcceptableValue
			,MarginalValue
			,PoorValue
			,ATDPosition
			,FillArea
			,Polarity
			,IsLegendVisible
			,IsDeleted
			,CreatedBy
			,CreatedOn)
			VALUES(
			@TemplateTabChartConfigurationId
			,@Name
			,@ChartLineColour
			,@ChartLineStyleId
			,@ChartLineThicknessId
			,@LegendName
			,@GoodValue
			,@AcceptableValue
			,@MarginalValue
			,@PoorValue
			,@ATDPosition
			,@FillArea
			,@Polarity
			,@IsLegendVisible
			,@IsDeleted
			,@UserId
			,GETUTCDATE())
		
			SELECT SCOPE_IDENTITY() AS Id,'Success' As TransactionMessage,1 As IsSuccess;
			SET @ChannelId=SCOPE_IDENTITY();
			BEGIN
			INSERT INTO TemplateTabChartConfigurationDetails(TemplateTabChartConfigurationId,[X-Axis-ChannelId],IsDeleted,CreatedBy,CreatedOn)
			VALUES(@TemplateTabChartConfigurationId, @ChannelId,0,@UserId,GETUTCDATE())
			END
		END

	COMMIT TRANSACTION TInsert;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TInsert;
			SELECT   
				 0 as Id		
				,ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;

END