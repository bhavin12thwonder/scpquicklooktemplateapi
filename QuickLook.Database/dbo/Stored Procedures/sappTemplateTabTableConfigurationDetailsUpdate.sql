﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabTableConfigurationDetailsUpdate] 
@TemplateTabTableConfigurationDetails TemplateTabTableConfigurationDetailsTable Readonly,
@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TUpdate;
		UPDATE t SET
				 TemplateTabTableConfigurationId=temp.TemplateTabTableConfigurationId
				,RowId=temp.RowId
				,RowSpan=temp.RowSpan
				,ColumnId=temp.ColumnId
				,ColumnSpan=temp.ColumnSpan
				,ColumnWidth=temp.ColumnWidth
				,ColumnType=temp.ColumnType
				,UserDefinedColumn=temp.UserDefinedColumn
				,DbColumn=temp.DbColumn
				,ValueType  =temp.ValueType
				,ATDPosition=temp.ATDPosition
				,ConditionalUserDefinedColumn=temp.ConditionalUserDefinedColumn
				,ResultColumn=temp.ResultColumn
				,ColourCode=temp.ColourCode
				,IsConfigHeader=temp.IsConfigHeader
				,IsHeader=temp.IsHeader
				,IsDbColumn=temp.IsDbColumn
				,IsUserDefined=temp.IsUserDefined
				,IsConditional=temp.IsConditional
				,IsReadOnly=temp.IsReadOnly
				,FormulaOperatorId=temp.FormulaOperatorId
				--,TemplateTabTableConfigurationFormulaId=temp.TemplateTabTableConfigurationFormulaId
				,IsDeleted=temp.IsDeleted
				,ModifiedBy=@UserId
				,ModifiedOn=GETUTCDATE()
			FROM TemplateTabTableConfigurationDetails t
			INNER JOIN @TemplateTabTableConfigurationDetails temp on t.id=temp.id
			WHERE temp.Id<>0

			INSERT INTO TemplateTabTableConfigurationDetails(
			TemplateTabTableConfigurationId
			,RowId
			,RowSpan
			,ColumnId
			,ColumnSpan
			,ColumnWidth
			,ColumnType
			,UserDefinedColumn
			,DbColumn
			,ValueType
			,ATDPosition
			,ConditionalUserDefinedColumn
			,ResultColumn
			,ColourCode
			,IsConfigHeader
			,IsHeader
			,IsDbColumn
			,IsUserDefined
			,IsConditional
			,IsReadOnly
			,FormulaOperatorId
			--,TemplateTabTableConfigurationFormulaId
			,IsDeleted
			,CreatedBy
			,CreatedOn)
				
			SELECT 
			TemplateTabTableConfigurationId
			,RowId
			,RowSpan
			,ColumnId
			,ColumnSpan			
			,ColumnWidth
			,ColumnType
			,UserDefinedColumn
			,DbColumn
			,ValueType
			,ATDPosition
			,ConditionalUserDefinedColumn
			,ResultColumn
			,ColourCode
			,IsConfigHeader
			,IsHeader
			,IsDbColumn
			,IsUserDefined
			,IsConditional
			,IsReadOnly
			,FormulaOperatorId
			--,TemplateTabTableConfigurationFormulaId
			,IsDeleted
			,@UserId
			,GETUTCDATE()
			FROM @TemplateTabTableConfigurationDetails
			WHERE Id=0

			UPDATE TTTC SET ModifiedBy=@UserId,ModifiedOn=GETUTCDATE(), Name = TEMP.TemplateTabTableConfiguratioName
			FROM TemplateTabTableConfiguration(NOLOCK) TTTC
			INNER JOIN @TemplateTabTableConfigurationDetails TEMP ON TTTC.Id=TEMP.TemplateTabTableConfigurationId

			SELECT SCOPE_IDENTITY() AS Id,'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT   
				 0 as Id		
				,ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
GO


