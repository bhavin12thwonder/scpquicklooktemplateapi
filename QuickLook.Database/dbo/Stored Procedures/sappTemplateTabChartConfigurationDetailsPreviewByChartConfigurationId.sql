﻿GO
CREATE PROCEDURE [dbo].[sappTemplateTabChartConfigurationDetailsPreviewByChartConfigurationId]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT	TTCD.Id
				,TTC.ChartTypeId
				,CT.ChartTypeValue
				,TTCD.TemplateTabChartConfigurationId
				,TTC.[Name] AS TemplateTabChartConfigurationName
				,TTCD.[X-Axis-ChannelId] AS XAxisChannelId
				,XAC.[Name] AS XAxisChannel
				,TTCD.[X-Axis-Units] AS XAxisUnits
				,TTCD.[X-Axis-MinValue] AS XAxisMinValue
				,TTCD.[X-Axis-MaxValue] AS XAxisMaxValue
				,TTCD.[X-Axis-Interval] AS XAxisInterval
				,TTCD.[X-Axis-Label] AS XAxisLabel
				,TTCD.[Y-Axis-Units] AS YAxisUnits
				,TTCD.[Y-Axis-MinValue] AS YAxisMinValue
				,TTCD.[Y-Axis-MaxValue] AS YAxisMaxValue
				,TTCD.[Y-Axis-Interval] AS YAxisInterval
				,TTCD.[Y-Axis-Label] AS YAxisLabel
				,CC.Id AS ChartChannelId
				,CC.[Name] AS ChartChannelName
				,CC.ChartLineColour
				,CC.ChartLineStyleId
				,CLS.[Name] AS LineStyleName
				,CLS.[StyleValue] AS LineStyleValue
				,CC.ChartLineThicknessId
				,CLT.[Name] AS LineThickness
				,CC.LegendName
				,CC.GoodValue
				,CC.AcceptableValue
				,CC.MarginalValue
				,CC.PoorValue
				,CC.ATDPosition
				,AP.[Name] AS ATDPositionValue
				,CC.FillArea
				,CC.Polarity
				,CC.IsLegendVisible
				,CA.Id AS ChartAnnotationId
				,CA.AnnotationLineStartXCoOrdinate
				,CA.AnnotationLineEndXCoOrdinate
				,CA.AnnotationLineStartYCoOrdinate
				,CA.AnnotationLineEndYCoOrdinate
				,CA.AnnotationLineThicknessId
				,CA.AnnotationLineColour
				,CA.AnnotationLineStyleId
				,CA.AnnotationText
				,CA.AnnotationTextStartXCoOrdinate
				,CA.AnnotationTextStartYCoOrdinate
				,TTCD.IsDeleted
		FROM TemplateTabChartConfigurationDetails(NOLOCK) TTCD
		INNER JOIN TemplateTabChartConfiguration(NOLOCK) TTC ON TTCD.TemplateTabChartConfigurationId=TTC.Id
		INNER JOIN ChartType(NOLOCK) CT ON CT.Id=TTC.ChartTypeId
		LEFT JOIN ChartChannel(NOLOCK) XAC ON TTC.Id=XAC.TemplateTabChartConfigurationId AND XAC.IsDeleted=0 AND TTCD.[X-Axis-ChannelId]=XAC.Id
		LEFT JOIN ChartChannel(NOLOCK) CC ON TTC.Id=CC.TemplateTabChartConfigurationId AND CC.IsDeleted=0
		LEFT JOIN ChartLineStyle(NOLOCK) CLS ON CC.ChartLineStyleId=CLS.Id
		LEFT JOIN ChartLineThickness(NOLOCK) CLT ON CC.ChartLineThicknessId=CLT.Id
		LEFT JOIN ChartAnnotation(NOLOCK) CA ON TTC.Id=CA.TemplateTabChartConfigurationId AND CA.IsDeleted=0
		LEFT JOIN ATDPosition(NOLOCK) AP ON AP.Id=CC.ATDPosition AND AP.IsDeleted=0
		WHERE TTCD.IsParentDisabled=0 AND TTCD.TemplateTabChartConfigurationId=@Id
		ORDER BY TTCD.TemplateTabChartConfigurationId
END
