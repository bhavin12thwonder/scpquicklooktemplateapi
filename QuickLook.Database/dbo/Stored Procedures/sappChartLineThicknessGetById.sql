﻿CREATE PROCEDURE [dbo].[sappChartLineThicknessGetById]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT Id
				,[Name]
				,IsDeleted
				,CreatedBy
				,CreatedOn
				,ModifiedBy
				,ModifiedOn 
		FROM ChartLineThickness
		WHERE IsDeleted=0 AND Id=@Id 
END
