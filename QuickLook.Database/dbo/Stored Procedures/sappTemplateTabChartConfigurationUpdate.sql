﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabChartConfigurationUpdate] 
@Id int,
@TemplateTabConfigId int,
@ChartTypeId int,
@ChartOrientation char(1)='v',
@Name nvarchar(50),
@UserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TInsert;
		UPDATE TemplateTabChartConfiguration SET
			TemplateTabConfigId=@TemplateTabConfigId
			,ChartTypeId=@ChartTypeId
			,ChartOrientation=@ChartOrientation
			,[Name]=@Name
			,ModifiedBy=@UserId
			,ModifiedOn=GETUTCDATE()
			WHERE Id=@Id

			SELECT SCOPE_IDENTITY() AS Id,'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TInsert;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TInsert;
			SELECT   
				 0 as Id		
				,ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
GO


