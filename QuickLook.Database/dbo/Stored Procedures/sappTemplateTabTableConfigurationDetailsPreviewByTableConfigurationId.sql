﻿GO
CREATE PROCEDURE [dbo].[sappTemplateTabTableConfigurationDetailsPreviewByTableConfigurationId]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
		SELECT TTCD.Id
			  ,TTCD.TemplateTabTableConfigurationId
			  ,TTCD.RowId
			  ,TTCD.RowSpan
			  ,TTCD.ColumnId
			  ,TTCD.ColumnSpan
			  ,TTCD.ColumnWidth
			  ,TTCD.ColumnType
			  ,TTCD.DbColumn
			  ,CASE WHEN TTCD.ATDPosition is not null THEN (SELECT ASAMATDValue From ATDPosition WHERE [Name] =  TTCD.ATDPosition) ELSE Null END ASAMATDValue
			  ,CASE WHEN TTCD.IsHeader=1 THEN TTCD.UserDefinedColumn END ColumnHeader
			  ,CASE WHEN TTCD.IsUserDefined=1 THEN TTCD.UserDefinedColumn
			  		WHEN TTCD.IsDbColumn=1 THEN TTCD.ATDPosition+'-'+TTCD.DbColumn--+'-'+TTCD.ValueType
			  		WHEN TTCD.IsConditional=1 THEN TTCD.ConditionalUserDefinedColumn+FO.Operator+TTCD.UserDefinedColumn+'='+ISNULL(TTCD.ResultColumn,TTCD.ConditionalUserDefinedColumn)
			  		ELSE TTCD.UserDefinedColumn END ColumnData
			  ,TTCD.ColourCode
			  ,TTCD.IsHeader
			  ,TTCD.IsDeleted
		FROM TemplateTabTableConfigurationDetails(NOLOCK) TTCD
		INNER JOIN TemplateTabTableConfiguration(NOLOCK) TTC ON TTCD.TemplateTabTableConfigurationId=TTC.Id
		LEFT JOIN FormulaOperator(NOLOCK) FO ON TTCD.FormulaOperatorId=FO.Id
		WHERE TTCD.IsParentDisabled=0 AND TTCD.TemplateTabTableConfigurationId=@Id AND TTCD.IsConfigHeader=0
		ORDER BY TTCD.RowId,TTCD.ColumnId
END
