﻿CREATE PROCEDURE [dbo].[sappChartAnnotationUpdate]
--@ChartAnnotationTable ChartAnnotationTable Readonly
@Id int
,@TemplateTabChartConfigurationId int
,@AnnotationLineStartXCoOrdinate int
,@AnnotationLineEndXCoOrdinate int
,@AnnotationLineStartYCoOrdinate int
,@AnnotationLineEndYCoOrdinate int
,@AnnotationLineThicknessId int
,@AnnotationLineColour nvarchar(50)
,@AnnotationLineStyleId int
,@AnnotationText nvarchar(50)
,@AnnotationTextStartXCoOrdinate nvarchar(50)
,@AnnotationTextStartYCoOrdinate nvarchar(50)
,@UserId	int=1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TUpdate;
		UPDATE ChartAnnotation set
				TemplateTabChartConfigurationId=@TemplateTabChartConfigurationId
				,AnnotationLineStartXCoOrdinate=@AnnotationLineStartXCoOrdinate
				,AnnotationLineEndXCoOrdinate=@AnnotationLineEndXCoOrdinate
				,AnnotationLineStartYCoOrdinate=@AnnotationLineStartYCoOrdinate
				,AnnotationLineEndYCoOrdinate=@AnnotationLineEndYCoOrdinate
				,AnnotationLineThicknessId=@AnnotationLineThicknessId
				,AnnotationLineColour=@AnnotationLineColour
				,AnnotationLineStyleId=@AnnotationLineStyleId
				,AnnotationText=@AnnotationText
				,AnnotationTextStartXCoOrdinate=@AnnotationTextStartXCoOrdinate
				,AnnotationTextStartYCoOrdinate=@AnnotationTextStartYCoOrdinate
				,ModifiedBy=@UserId
				,ModifiedOn=GETUTCDATE()
				WHERE Id=@Id
				--FROM ChartAnnotation CA
				--INNER JOIN @ChartAnnotationTable temp ON CA.Id=@Id --AND CA.TemplateTabChartConfigurationId=@TemplateTabChartConfigurationId

			SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT 
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END