﻿
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sappTemplateTabUpdate] 
@Id int
,@Name nvarchar(50)
,@TemplateId int
,@TabIndex int
,@DisplayOrder int
,@Rows int
,@Columns int
,@RowsLimit int
,@ModifiedBy	int=1
,@ModifiedOn	datetime=GetUtcDate
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	
	BEGIN TRY
	BEGIN TRANSACTION TUpdate;
		UPDATE TemplateTabs set
		TemplateId=@TemplateId,
		TabIndex=@TabIndex,
		DisplayOrder=@DisplayOrder,
		[Name]=@Name,
		[Rows]=@Rows,
		[Columns]=@Columns,
		RowsLimit=@RowsLimit
		WHERE id=@id

		SELECT 'Success' As TransactionMessage,1 As IsSuccess;
	COMMIT TRANSACTION TUpdate;
END TRY

BEGIN CATCH
	IF (@@TRANCOUNT > 0)
		BEGIN
			ROLLBACK TRANSACTION TUpdate;
			SELECT 
				ERROR_MESSAGE() AS TransactionMessage
				,0 As IsSuccess;
				 --ERROR_NUMBER() AS Number  
				--,ERROR_SEVERITY() AS Severity  
				--,ERROR_STATE() AS State  
				--,ERROR_LINE () AS LineNumber		  
				--,ERROR_PROCEDURE() AS StoredProcedureName  
            PRINT 'Error detected, all changes reversed';
		END;
END CATCH;
END
