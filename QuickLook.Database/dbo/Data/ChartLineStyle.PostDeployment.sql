﻿GO

DECLARE @ChartLineStyle AS TABLE(
	[Id] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[StyleValue] [nvarchar](50) NULL,
	[Style] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL);


INSERT @ChartLineStyle([Id], [Name], [StyleValue], [Style], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'Solid', N'solid', N'────', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartLineStyle([Id], [Name], [StyleValue], [Style], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'Dashed', N'dashdot', N'─・─・', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartLineStyle([Id], [Name], [StyleValue], [Style], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'Dotted', N'dot', N'·············', 0, 1, GETUTCDATE(), NULL, NULL)


SET IDENTITY_INSERT [dbo].[ChartLineStyle] ON 

MERGE [dbo].[ChartLineStyle] AS T
USING @ChartLineStyle AS S
ON (S.Id=T.Id)
WHEN NOT MATCHED THEN
	INSERT ([Id], [Name], [StyleValue], [Style], [IsDeleted], [CreatedBy], [CreatedOn]) VALUES(S.[Id], S.[Name], S.[StyleValue], S.[Style], S.[IsDeleted], S.[CreatedBy], S.[CreatedOn])
WHEN MATCHED THEN
	UPDATE SET 
		[Name]=S.[Name], 
		[StyleValue]=S.[StyleValue], 
		[Style]=S.[Style], 
		[IsDeleted]=S.[IsDeleted];

SET IDENTITY_INSERT [dbo].[ChartLineStyle] OFF