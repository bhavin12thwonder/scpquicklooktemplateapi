﻿GO
DECLARE @ATDPosition AS TABLE(
    [Id] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[ATDPositionValue] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL);

INSERT @ATDPosition([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'Driver',N'Driver', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ATDPosition([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'Passenger',N'Pass', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ATDPosition([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'RRL',N'RRL', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ATDPosition([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, N'RRC',N'RRC', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ATDPosition([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (5, N'RRR',N'RRR', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ATDPosition([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (6, N'3RL',N'3RL', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ATDPosition([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (7, N'3RC',N'3RC', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ATDPosition([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (8, N'3RR',N'3RR', 0, 1, GETUTCDATE(), NULL, NULL)


SET IDENTITY_INSERT [dbo].[ATDPosition] ON 

MERGE [dbo].[ATDPosition] AS T
USING @ATDPosition AS S
ON (S.Id=T.Id)
WHEN NOT MATCHED THEN
	INSERT([Id], [Name],[ATDPositionValue], [IsDeleted], [CreatedBy], [CreatedOn]) VALUES(S.[Id], S.[Name],S.[ATDPositionValue], S.[IsDeleted], S.[CreatedBy], S.[CreatedOn])
WHEN MATCHED THEN
	UPDATE SET 
			[Name]=S.[Name],
			[ATDPositionValue]=S.[ATDPositionValue],
			[IsDeleted]=S.[IsDeleted];
SET IDENTITY_INSERT [dbo].[ATDPosition] OFF