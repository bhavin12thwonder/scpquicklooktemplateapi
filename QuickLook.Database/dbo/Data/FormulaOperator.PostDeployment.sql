﻿GO

DECLARE @FormulaOperator AS TABLE(
	[Id] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Operator] [nchar](5) NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL)

INSERT @FormulaOperator([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'Sum', N'+    ', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @FormulaOperator([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'Difference', N'-    ', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @FormulaOperator([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'Multiply', N'*    ', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @FormulaOperator([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, N'Division', N'/    ', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @FormulaOperator([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (5, N'Greater Than', N'>    ', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @FormulaOperator([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (6, N'Less Than', N'<    ', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @FormulaOperator([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (7, N'Greater Than Or Equal', N'>=   ', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @FormulaOperator([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (8, N'Less Than Or Equal', N'<=   ', 0, 1, GETUTCDATE(), NULL, NULL)


SET IDENTITY_INSERT [dbo].[FormulaOperator] ON 

MERGE [dbo].[FormulaOperator] AS T
USING @FormulaOperator AS S
ON (S.Id=T.Id)
WHEN NOT MATCHED THEN
	INSERT([Id], [Name], [Operator], [IsDeleted], [CreatedBy], [CreatedOn]) VALUES(S.[Id], S.[Name], S.[Operator], S.[IsDeleted], S.[CreatedBy], S.[CreatedOn])
WHEN MATCHED THEN
	UPDATE SET 
			[Name]=S.[Name],
			[Operator]=S.[Operator],
			[IsDeleted]=S.[IsDeleted];
SET IDENTITY_INSERT [dbo].[FormulaOperator] OFF