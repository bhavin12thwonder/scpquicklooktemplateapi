﻿GO
DECLARE @ChartLineThickness AS TABLE(
	[Id] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL)


INSERT @ChartLineThickness([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'1', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartLineThickness([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'2', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartLineThickness([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'3', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartLineThickness([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, N'4', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartLineThickness([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (5, N'5', 0, 1, GETUTCDATE(), NULL, NULL)


SET IDENTITY_INSERT [dbo].[ChartLineThickness] ON 

MERGE [dbo].[ChartLineThickness] AS T
USING @ChartLineThickness AS S
ON (S.Id=T.Id)
WHEN NOT MATCHED THEN
	INSERT([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn]) VALUES(S.[Id], S.[Name], S.[IsDeleted], S.[CreatedBy], S.[CreatedOn])
WHEN MATCHED THEN
	UPDATE SET 
			[Name]=S.[Name],
			[IsDeleted]=S.[IsDeleted];
SET IDENTITY_INSERT [dbo].[ChartLineThickness] OFF