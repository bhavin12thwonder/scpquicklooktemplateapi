﻿GO
DECLARE @ChartType AS TABLE (
	[Id] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[ChartTypeValue] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL);

INSERT @ChartType ([Id], [Name], [ChartTypeValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'Line Chart', N'line', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartType ([Id], [Name], [ChartTypeValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'Bar Chart', N'bar', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartType ([Id], [Name], [ChartTypeValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'Area Chart', N'scatter', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @ChartType ([Id], [Name], [ChartTypeValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, N'Stack Bar Chart', N'bar', 0, 1, GETUTCDATE(), NULL, NULL)

SET IDENTITY_INSERT [dbo].[ChartType] ON
 
MERGE [dbo].[ChartType] AS T
USING @ChartType AS S
ON (T.Id = S.Id)
when Matched then
 UPDATE set 
		[Name]=s.[Name],
		ChartTypeValue=s.ChartTypeValue,
		IsDeleted=s.IsDeleted
WHEN NOT MATCHED
	THEN INSERT([Id], [Name], [ChartTypeValue], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn])
		  VALUES(S.[Id], S.[Name], S.[ChartTypeValue], S.[IsDeleted], S.[CreatedBy], S.[CreatedOn], S.[ModifiedBy], S.[ModifiedOn]);

SET IDENTITY_INSERT [dbo].[ChartType] OFF