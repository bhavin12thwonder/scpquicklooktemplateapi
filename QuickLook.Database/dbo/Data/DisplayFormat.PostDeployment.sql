﻿GO
DECLARE @DisplayFormat AS TABLE(
	[Id] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL)

INSERT @DisplayFormat([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'Table', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @DisplayFormat([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'Chart', 0, 1, GETUTCDATE(), NULL, NULL)
INSERT @DisplayFormat([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'NoData', 0, 1, GETUTCDATE(), NULL, NULL)


SET IDENTITY_INSERT [dbo].[DisplayFormat] ON 

MERGE [dbo].[DisplayFormat] AS T
USING @DisplayFormat AS S
ON (S.Id=T.Id)
WHEN NOT MATCHED THEN
	INSERT([Id], [Name], [IsDeleted], [CreatedBy], [CreatedOn]) VALUES(S.[Id], S.[Name], S.[IsDeleted], S.[CreatedBy], S.[CreatedOn])
WHEN MATCHED THEN
	UPDATE SET 
			[Name]=S.[Name],
			[IsDeleted]=S.[IsDeleted];
SET IDENTITY_INSERT [dbo].[DisplayFormat] OFF