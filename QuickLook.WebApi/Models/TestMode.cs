﻿using System;
using System.Collections.Generic;

namespace QuickLook_API.Models
{
    public partial class TestMode
    {
        public TestMode()
        {
            Template = new HashSet<Template>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int TestTypeId { get; set; }
        public bool? IsDeleted { get; set; }

        public TestType TestType { get; set; }
        public ICollection<Template> Template { get; set; }
    }
}
