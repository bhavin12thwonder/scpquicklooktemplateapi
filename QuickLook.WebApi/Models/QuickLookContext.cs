﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace QuickLook_API.Models
{
    public partial class QuickLookContext : DbContext
    {
        public QuickLookContext()
        {
        }

        public QuickLookContext(DbContextOptions<QuickLookContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Template> Template { get; set; }
        public virtual DbSet<TestMode> TestMode { get; set; }
        public virtual DbSet<TestType> TestType { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("server=AMHRAOSQL16DEV1;database=Quicklook;user id=QuicklookDuser;password=t5eY6LB$!A5O%JZY;MultipleActiveResultSets=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Template>(entity =>
            {
                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.TemplateCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK_Template_Users");

                entity.HasOne(d => d.ModifiedByNavigation)
                    .WithMany(p => p.TemplateModifiedByNavigation)
                    .HasForeignKey(d => d.ModifiedBy)
                    .HasConstraintName("FK_Template_Users1");

                entity.HasOne(d => d.TestMode)
                    .WithMany(p => p.Template)
                    .HasForeignKey(d => d.TestModeId)
                    .HasConstraintName("FK_Template_TestMode");

                entity.HasOne(d => d.TestTyep)
                    .WithMany(p => p.Template)
                    .HasForeignKey(d => d.TestTypeId)
                    .HasConstraintName("FK_Template_TestType");
            });

            modelBuilder.Entity<TestMode>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.TestType)
                    .WithMany(p => p.TestMode)
                    .HasForeignKey(d => d.TestTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TestMode_TestType");
            });

            modelBuilder.Entity<TestType>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(255);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.Department).HasMaxLength(255);

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ManagerSamAccountName)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SamAccountName).HasMaxLength(100);
            });
        }
    }
}
