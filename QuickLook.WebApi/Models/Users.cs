﻿using System;
using System.Collections.Generic;

namespace QuickLook_API.Models
{
    public partial class Users
    {
        public Users()
        {
            TemplateCreatedByNavigation = new HashSet<Template>();
            TemplateModifiedByNavigation = new HashSet<Template>();
        }

        public int Id { get; set; }
        public string SamAccountName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ManagerSamAccountName { get; set; }
        public string Department { get; set; }

        public ICollection<Template> TemplateCreatedByNavigation { get; set; }
        public ICollection<Template> TemplateModifiedByNavigation { get; set; }
    }
}
