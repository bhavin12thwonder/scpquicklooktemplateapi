﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Web.Http.Cors;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TemplateTabTableConfigurationDetailsController : Controller
    {
        private readonly ITemplateTabTableConfigurationDetailsRepository _templateTabTableConfigurationDetails = new TemplateTabTableConfigurationDetailsRepository();
        private readonly ITemplateTabTableConfigurationRepository _templateTabTableConfigurationRepository = new TemplateTabTableConfigurationRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        private int userId = 1;

        //public TemplateTabTableConfigurationDetailsController(ITemplate template)
        //{
        //    _template = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfigurationDetails/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _templateTabTableConfigurationDetails.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabTableConfigurationDetails
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfigurationDetails/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _templateTabTableConfigurationDetails.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabTableConfigurationDetails By TableConfigurationId
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfigurationDetails/GetByTemplateTabTableConfigId/{Id}")]
        [HttpGet]
        public IActionResult GetByTemplateTabId(int Id)
        {
            var result = _templateTabTableConfigurationDetails.GetByTemplateTabTableConfigId(Id);
            return Json(result);
        }

        /// <summary>
		/// Preview TemplateTabTableConfigurationDetails By TableConfigurationId
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfigurationDetails/PreviewByTemplateTabTableConfigId/{Id}/{testId}")]
        [HttpGet]
        public IActionResult PreviewByTemplateTabId(int Id, int testId)
        {
            var result = _templateTabTableConfigurationDetails.PreviewByTemplateTabTableConfigId(Id, testId);
            return Json(result);
        }

        /// <summary>
		/// Insert TemplateTabTableConfigurationDetails
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfigurationDetails")]
        [HttpPost]
        public IActionResult Insert([FromBody]List<TemplateTabTableConfigurationDetailsUpsertViewModel> viewModel)
        {
            // First check if there is update in chart name
            var TemplateTabTableConfiguration = _templateTabTableConfigurationRepository.GetById(viewModel.FirstOrDefault().TemplateTabTableConfigurationId);
            if (TemplateTabTableConfiguration.Name != viewModel.FirstOrDefault().TemplateTabTableConfigurationName)
            {
                var isExists = _templateTabTableConfigurationRepository.CheckTabTableConfigName(viewModel.FirstOrDefault().TemplateTabTableConfigurationId, viewModel.FirstOrDefault().TemplateTabTableConfigurationName);
                if (isExists)
                    return Json(new { IsSuccess = false, Message = "Configuration Name Alsready Exists" });
            }
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            var result = _templateTabTableConfigurationDetails.Insert(viewModel, User.Id);
            return Json(result);
        }

        /// <summary>
		/// Update TemplateTabTableConfigurationDetails
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfigurationDetails")]
        [HttpPut]
        public IActionResult Update([FromBody]List<TemplateTabTableConfigurationDetailsUpsertViewModel> viewModel)
        {
            // First check if there is update in chart name
            var TemplateTabTableConfiguration = _templateTabTableConfigurationRepository.GetById(viewModel.FirstOrDefault().TemplateTabTableConfigurationId);            
            if (TemplateTabTableConfiguration.Name != viewModel.FirstOrDefault().TemplateTabTableConfigurationName)
            {
                var isExists = _templateTabTableConfigurationRepository.CheckTabTableConfigName(viewModel.FirstOrDefault().TemplateTabTableConfigurationId, viewModel.FirstOrDefault().TemplateTabTableConfigurationName);
                if (isExists)
                    return Json(new { IsSuccess = false, Message = "Configuration Name Alsready Exists" });
            }

            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            var result = _templateTabTableConfigurationDetails.Update(viewModel, User.Id);
            return Json(result);
        }

        /// <summary>
		/// Delete TemplateTabTableConfigurationDetails
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfigurationDetails/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            var result = _templateTabTableConfigurationDetails.Delete(Id, User.Id);
            return Json(result);
        }
    }
}