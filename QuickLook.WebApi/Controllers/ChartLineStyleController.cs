﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class ChartLineStyleController : Controller
    {
        private readonly IChartLineStyleRepository _chartLineStyleRepository = new ChartLineStyleRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineStyle/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _chartLineStyleRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get ChartLineStyle
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineStyle/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _chartLineStyleRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert ChartLineStyle
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineStyle")]
        [HttpPost]
        public IActionResult Insert([FromBody]ChartLineStyleViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _chartLineStyleRepository.Insert(model);
            return Json(result);
        }

        /// <summary>
		/// Update ChartLineStyle
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineStyle")]
        [HttpPut]
        public IActionResult Update([FromBody]ChartLineStyleViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _chartLineStyleRepository.Update(model);
            return Json(result);
        }

        /// <summary>
		/// Delete ChartLineStyle
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineStyle/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _chartLineStyleRepository.Delete(Id);
            return Json(result);
        }
    }
}