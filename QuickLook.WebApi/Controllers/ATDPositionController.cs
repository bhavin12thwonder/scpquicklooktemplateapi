﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class ATDPositionController : Controller
    {
        private readonly IATDPositionRepository _atdPositionRepository = new ATDPositionRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/ATDPosition/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _atdPositionRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get ATDPosition
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ATDPosition/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _atdPositionRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert ATDPosition
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ATDPosition")]
        [HttpPost]
        public IActionResult Insert([FromBody]ATDPositionViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _atdPositionRepository.Insert(model);
            return Json(result);
        }

        /// <summary>
		/// Update ATDPosition
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ATDPosition")]
        [HttpPut]
        public IActionResult Update([FromBody]ATDPositionViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _atdPositionRepository.Update(model);
            return Json(result);
        }

        /// <summary>
		/// Delete ATDPosition
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ATDPosition/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _atdPositionRepository.Delete(Id);
            return Json(result);
        }
    }
}