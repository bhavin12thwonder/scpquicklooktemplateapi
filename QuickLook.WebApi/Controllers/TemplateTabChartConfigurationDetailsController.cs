﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Web.Http.Cors;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TemplateTabChartConfigurationDetailsController : Controller
    {
        private readonly ITemplateTabChartConfigurationDetailsRepository _templateTabChartConfigurationDetails = new TemplateTabChartConfigurationDetailsRepository();
        private readonly ITemplateTabChartConfigurationRepository _templateTabChartConfigurationRepository = new TemplateTabChartConfigurationRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        private int userId = 1;

        //public TemplateTabChartConfigurationDetailsController(ITemplate template)
        //{
        //    _template = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfigurationDetails/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _templateTabChartConfigurationDetails.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabChartConfigurationDetails
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfigurationDetails/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _templateTabChartConfigurationDetails.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabChartConfigurationDetails By ChartConfigurationId
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfigurationDetails/GetByTemplateTabChartConfigId/{Id}")]
        [HttpGet]
        public IActionResult GetByTemplateTabId(int Id)
        {
            var result = _templateTabChartConfigurationDetails.GetByTemplateTabChartConfigId(Id);
            return Json(result);
        }

        /// <summary>
		/// Preview TemplateTabChartConfigurationDetails By ChartConfigurationId
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfigurationDetails/PreviewByTemplateTabChartConfigId/{Id}/{testId}/{submatrixName}")]
        [HttpGet]
        public IActionResult PreviewByTemplateTabId(int Id,int testId, string submatrixName)
        {
            var result = _templateTabChartConfigurationDetails.PreviewByTemplateTabChartConfigId(Id, testId, submatrixName);
            return Json(result);
        }

        /// <summary>
		/// Insert TemplateTabChartConfigurationDetails
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfigurationDetails")]
        [HttpPost]
        public IActionResult Insert([FromBody]TemplateTabChartConfigurationDetailsUpsertViewModel viewModel)
        {
            // First check if there is update in chart name
            var TemplateTabChartConfiguration = _templateTabChartConfigurationRepository.GetById(viewModel.TemplateTabChartConfigurationId);
            if (TemplateTabChartConfiguration.Name != viewModel.TemplateTabChartConfigurationName)
            {
                var isExists = _templateTabChartConfigurationRepository.CheckTabChartConfigName(TemplateTabChartConfiguration.TemplateTabConfigId.Value, TemplateTabChartConfiguration.ChartTypeId.Value, TemplateTabChartConfiguration.Name);
                if (isExists)
                    return Json(new { IsSuccess = false, Message = "Configuration Name Alsready Exists" });
            }
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            var result = _templateTabChartConfigurationDetails.Insert(viewModel, User.Id);
            return Json(result);
        }

        /// <summary>
		/// Update TemplateTabChartConfigurationDetails
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfigurationDetails")]
        [HttpPut]
        public IActionResult Update([FromBody]TemplateTabChartConfigurationDetailsUpsertViewModel viewModel)
        {
            // First check if there is update in chart name
            var TemplateTabChartConfiguration = _templateTabChartConfigurationRepository.GetById(viewModel.TemplateTabChartConfigurationId);
            if (TemplateTabChartConfiguration.Name != viewModel.TemplateTabChartConfigurationName) {
                var isExists = _templateTabChartConfigurationRepository.CheckTabChartConfigName(TemplateTabChartConfiguration.TemplateTabConfigId.Value, TemplateTabChartConfiguration.ChartTypeId.Value, TemplateTabChartConfiguration.Name);
                if (isExists)
                    return Json(new{ IsSuccess = false, Message = "Configuration Name Alsready Exists" });
            }
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            var result = _templateTabChartConfigurationDetails.Update(viewModel, User.Id);
            return Json(result);
        }

        /// <summary>
		/// Delete TemplateTabChartConfigurationDetails
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfigurationDetails/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            var result = _templateTabChartConfigurationDetails.Delete(Id, User.Id);
            return Json(result);
        }
    }
}