﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using System.Web.Http;
using QuickLook.Common;

namespace QuickLook.WebApi.Controllers
{
    public class TemplateController : QuickLookBaseController
    {
        private readonly ITemplateRepository _template = new TemplateRepository();

        //public TemplateController(ITemplate template)
        //{
        //    _template = template;
        //}

        public HttpResponseMessage Index()
        {
            IViewModel viewModel = new ViewData()
            {
                IsSuccess = false,
                Message = " Entity not found"
            };

            return OkMessage(viewModel);
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/Template/GetAll")]
        [HttpGet]
        public HttpResponseMessage GetAll()
        {
            IViewModel viewModel = new ViewData()
            {
                IsSuccess = false,
                Message = " Entity not found"
            };
            var result = _template.GetAll();
            if (result == null)
            {
                return OkMessage(viewModel);
            }
            viewModel.Message = "Success";
            viewModel.IsSuccess = true;
            return OkMessage(result, viewModel);
        }

        /// <summary>
		/// Get Template
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template/GetById/{Id}")]
        [HttpGet]
        public HttpResponseMessage GetById(int Id)
        {
            IViewModel viewModel = new ViewData()
            {
                IsSuccess = false,
                Message = " Entity not found"
            };
            var result = _template.GetById(Id);
            if (result == null)
            {
                return OkMessage(viewModel);
            }
            viewModel.Message = "Success";
            viewModel.IsSuccess = true;
            return OkMessage(result, viewModel);
        }

        /// <summary>
		/// Insert Template
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template")]
        [HttpPost]
        public HttpResponseMessage Insert([FromBody]TemplateViewModel model)
        {
            IViewModel viewModel = new ViewData()
            {
                IsSuccess = false,
                Message = " Entity not found"
            };
            model.CreatedBy = 1;
            var result = _template.Insert(model);
            if (result == null)
            {
                return OkMessage(viewModel);
            }
            viewModel.Message = "Success";
            viewModel.IsSuccess = true;
            return OkMessage(result, viewModel);
        }

        /// <summary>
		/// Update Template
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template")]
        [HttpPut]
        public HttpResponseMessage Update([FromBody]TemplateViewModel model)
        {
            IViewModel viewModel = new ViewData()
            {
                IsSuccess = false,
                Message = " Entity not found"
            };
            model.CreatedBy = 1;
            var result = _template.Update(model);
            if (result == null)
            {
                return OkMessage(viewModel);
            }
            viewModel.Message = "Success";
            viewModel.IsSuccess = true;
            return OkMessage(result, viewModel);
        }

        /// <summary>
		/// Delete Template
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template/{Id}")]
        [HttpDelete]
        public HttpResponseMessage Delete(int Id)
        {
            IViewModel viewModel = new ViewData()
            {
                IsSuccess = false,
                Message = " Entity not found"
            };
            var result = _template.Delete(Id);
            if (result == null)
            {
                return OkMessage(viewModel);
            }
            viewModel.Message = "Success";
            viewModel.IsSuccess = true;
            return OkMessage(result, viewModel);
        }

    }
}