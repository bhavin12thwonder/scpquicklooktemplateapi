﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TestTypeController : Controller
    {
        private readonly ITestTypeRepository _testType = new TestTypeRepository();
        private readonly IUserRepository _userRepository = new UserRepository();

        //public TestTypeController(ITestType template)
        //{
        //    _testType = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl TestTypes
		/// </summary>
		/// <returns></returns>
		[Route("api/TestType/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _testType.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get TestType
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestType/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _testType.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert TestType
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestType")]
        [HttpPost]
        public IActionResult Insert([FromBody]TestTypeViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _testType.Insert(model);
            return Json(result);
        }

        /// <summary>
		/// Update TestType
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestType")]
        [HttpPut]
        public IActionResult Update([FromBody]TestTypeViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _testType.Update(model);
            return Json(result);
        }

        /// <summary>
		/// Delete TestType
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestType/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _testType.Delete(Id);
            return Json(result);
        }

    }
}