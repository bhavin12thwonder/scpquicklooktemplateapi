﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;

namespace QuickLook.WebApi.Controllers
{
    public class ChartAnnotationController : Controller
    {
        private readonly IChartAnnotationRepository _chartAnnotationRepository = new ChartAnnotationRepository();

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartAnnotation/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _chartAnnotationRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get ChartAnnotation
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartAnnotation/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _chartAnnotationRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Get ChartAnnotation
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartAnnotation/GetByChartConfigId/{Id}")]
        [HttpGet]
        public IActionResult GetByChartConfigId(int Id)
        {
            var result = _chartAnnotationRepository.GetByChartConfigId(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert ChartAnnotation
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartAnnotation")]
        [HttpPost]
        public IActionResult Insert([FromBody]List<ChartAnnotationViewModel> model)
        {
            var result = _chartAnnotationRepository.Insert(model, userId: 1);
            return Json(result);
        }

        /// <summary>
		/// Update ChartAnnotation
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartAnnotation")]
        [HttpPut]
        public IActionResult Update([FromBody]ChartAnnotationViewModel model)
        {
            var result = _chartAnnotationRepository.Update(model, userId: 1);
            return Json(result);
        }

        /// <summary>
		/// Delete ChartAnnotation
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartAnnotation/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _chartAnnotationRepository.Delete(Id);
            return Json(result);
        }
    }
}