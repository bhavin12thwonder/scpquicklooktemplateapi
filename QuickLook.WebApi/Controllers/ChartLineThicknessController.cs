﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class ChartLineThicknessController : Controller
    {
        private readonly IChartLineThicknessRepository _chartLineThicknessRepository = new ChartLineThicknessRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineThickness/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _chartLineThicknessRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get ChartLineThickness
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineThickness/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _chartLineThicknessRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert ChartLineThickness
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineThickness")]
        [HttpPost]
        public IActionResult Insert([FromBody]ChartLineThicknessViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _chartLineThicknessRepository.Insert(model);
            return Json(result);
        }

        /// <summary>
		/// Update ChartLineThickness
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineThickness")]
        [HttpPut]
        public IActionResult Update([FromBody]ChartLineThicknessViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _chartLineThicknessRepository.Update(model);
            return Json(result);
        }

        /// <summary>
		/// Delete ChartLineThickness
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartLineThickness/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _chartLineThicknessRepository.Delete(Id);
            return Json(result);
        }
    }
}