﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using Microsoft.AspNetCore.Authorization;
using System.Web.Http.Cors;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TemplateTabConfigurationController : Controller
    {
        private readonly ITemplateTabConfigurationRepository _templateTabConfiguration = new TemplateTabConfigurationRepository();

        //public TemplateController(ITemplate template)
        //{
        //    _template = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _templateTabConfiguration.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabConfiguration
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _templateTabConfiguration.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabConfiguration By TemplateId
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration/GetByTemplateTabId/{Id}")]
        [HttpGet]
        public IActionResult GetTemplateTabConfiguration(int Id)
        {
            var result = _templateTabConfiguration.GetByTemplateTabId(Id);
            return Json(result);
        }

  //      /// <summary>
		///// Get TemplateTabConfiguration By TemplateId
  //      /// /// <param name="Id"></param>
		///// </summary>
		///// <returns></returns>
		//[Route("api/TemplateTabConfiguration/PreviewByTemplateTabId/{Id}")]
  //      [HttpGet]
  //      public IActionResult PreviewTemplateTabConfiguration(int Id)
  //      {
  //          var result = _templateTabConfiguration.PreviewByTemplateTabId(Id);
  //          return Json(result);
  //      }

        /// <summary>
		/// Get TemplateTabConfiguration By TemplateId
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration/GetTemplateTabConfigurationTypeByTemplateTabId/{configTypeId}/{Id}")]
        [HttpGet]
        public IActionResult GetTabConfigTypeByTabId(int configTypeId, int Id)
        {
            var result = _templateTabConfiguration.GetTemplateTabConfigTypeByTabId(Id, configTypeId);
            return Json(result);
        }

        /// <summary>
		/// Insert TemplateTabConfiguration
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration")]
        [HttpPost]
        public IActionResult Insert([FromBody]List<TemplateTabConfigurationUpsertViewModel> viewModel)
        {
            var result = _templateTabConfiguration.Insert(viewModel);
            return Json(result);
        }

        /// <summary>
		/// Update TemplateTabConfiguration
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration")]
        [HttpPut]
        public IActionResult Update([FromBody]List<TemplateTabConfigurationUpsertViewModel> viewModel)
        {
            var result = _templateTabConfiguration.Update(viewModel);
            return Json(result);
        }

        /// <summary>
		/// Update TemplateTabConfiguration
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration/UpdateById")]
        [HttpPut]
        public IActionResult UpdateById([FromBody]TemplateTabConfigurationViewModel viewModel)
        {
            var result = _templateTabConfiguration.UpdateById(viewModel);
            return Json(result);
        }

        /// <summary>
		/// Update TemplateTabConfiguration By ConfigType
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration/UpdateTemplateTabConfigurationTypeByTemplateTabId/{configTypeId}/{Id}")]
        [HttpPut]
        public IActionResult UpdateTabConfigTypeByTabId(int configTypeId, int Id)
        {
            var result = _templateTabConfiguration.UpdateTemplateTabConfigTypeByTabId(Id, configTypeId);
            return Json(result);

        }
        /// <summary>
		/// Delete TemplateTabConfiguration
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabConfiguration/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _templateTabConfiguration.Delete(Id);
            return Json(result);
        }

    }
}