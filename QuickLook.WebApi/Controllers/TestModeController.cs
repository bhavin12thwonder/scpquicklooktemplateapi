﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TestModeController : Controller
    {
        private readonly ITestModeRepository _testMode = new TestModeRepository();
        private readonly IUserRepository _userRepository = new UserRepository();

        //public TestModeController(ITestMode template)
        //{
        //    _testMode = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl TestModes
		/// </summary>
		/// <returns></returns>
		[Route("api/TestMode/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _testMode.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get TestMode
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestMode/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _testMode.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Get TestMode
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestMode/GetByTestType/{Id}")]
        [HttpGet]
        public IActionResult GetByTestTypeI(int Id)
        {
            var result = _testMode.GetByTestType(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert TestMode
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestMode")]
        [HttpPost]
        public IActionResult Insert([FromBody]TestModeViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _testMode.Insert(model);
            return Json(result);
        }

        /// <summary>
		/// Update TestMode
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestMode")]
        [HttpPut]
        public IActionResult Update([FromBody]TestModeViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _testMode.Update(model);
            return Json(result);
        }

        /// <summary>
		/// Delete TestMode
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TestMode/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _testMode.Delete(Id);
            return Json(result);
        }

    }
}