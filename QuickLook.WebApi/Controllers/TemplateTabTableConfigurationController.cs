﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using Microsoft.AspNetCore.Authorization;
using System.Web.Http.Cors;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TemplateTabTableConfigurationController : Controller
    {
        private readonly ITemplateTabTableConfigurationRepository _templateTabTableConfigurationRepository = new TemplateTabTableConfigurationRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        private int userId = 1;

        //public TemplateTabTableConfigurationsController(ITemplateTabTableConfigurations template)
        //{
        //    _template = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl TemplateTabTableConfigurationsTabss
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfiguration/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _templateTabTableConfigurationRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabTableConfiguration
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfiguration/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _templateTabTableConfigurationRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Check TemplateTabTableConfigurationName Availability
        /// /// <param name="tabConfigId"></param>
        /// /// /// <param name="Name"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfiguration/CheckTabTableConfigName/{tabConfigId}/{Name}")]
        [HttpGet]
        public IActionResult CheckTabTableConfigNameAvailability(int tabConfigId,string Name)
        {
            var result = _templateTabTableConfigurationRepository.CheckTabTableConfigName(tabConfigId, Name);
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabTableConfiguration
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfiguration/GetByTemplateTabConfigId/{Id}")]
        [HttpGet]
        public IActionResult GetByTemplateTabConfigId(int Id)
        {
            var result = _templateTabTableConfigurationRepository.GetByTemplateTabConfigId(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert TemplateTabTableConfiguration
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfiguration")]
        [HttpPost]
        public IActionResult Insert([FromBody]TemplateTabTableConfigurationViewModel viewModel)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            viewModel.CreatedBy = User.Id;
            var result = _templateTabTableConfigurationRepository.Insert(viewModel, User.Id);
            return Json(result);
        }

        /// <summary>
		/// Update TemplateTabTableConfiguration
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfiguration")]
        [HttpPut]
        public IActionResult Update([FromBody]TemplateTabTableConfigurationViewModel viewModel)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            viewModel.CreatedBy = User.Id;
            var result = _templateTabTableConfigurationRepository.Update(viewModel, User.Id);
            return Json(result);
        }

        /// <summary>
		/// Delete TemplateTabTableConfiguration
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabTableConfiguration/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            var result = _templateTabTableConfigurationRepository.Delete(Id, User.Id);
            return Json(result);
        }
    }
}