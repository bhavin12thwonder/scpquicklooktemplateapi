﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TemplateController : Controller
    {
        private readonly ITemplateRepository _templateRepository = new TemplateRepository();
        private readonly IUserRepository _userRepository = new UserRepository();

        //public TemplateController(ITemplate template)
        //{
        //    _templateRepository = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/Template/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var UserName = HttpContext.User.Identity.Name;
            var result = _templateRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get Template
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _templateRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Get Template TabConfiguration By TemplateId
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template/PreviewByTemplateId/{Id}/{testId}/{submatrixName}")]
        [HttpGet]
        public IActionResult PreviewTemplateTabConfiguration(int Id,int testId, string submatrixName)
        {
            var result = _templateRepository.PreviewByTemplateId(Id, testId, submatrixName);
            return Json(result);
        }

        [Route("api/Template/CheckTemplateName")]
        [HttpPost]
        public IActionResult CheckTabTableConfigNameAvailability([FromBody] TemplateViewModel model)
        {
            try
            {
                var UserName = HttpContext.User.Identity.Name;
                var result = _templateRepository.CheckTemplateName(model.Id, model.Name, model.TestTypeId.Value, model.TestModeIds);
                return Json(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nMessage ---\n{0}", ex.Message);
                return null;
                //throw;
            }
        }

        /// <summary>
		/// Insert Template
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template")]
        [HttpPost]
        public IActionResult Insert([FromBody]TemplateViewModel model)
        {
            try
            {
                var UserName = HttpContext.User.Identity.Name;
                var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
                model.CreatedBy = User.Id;
                var result = _templateRepository.Insert(model);
                return Json(result);
            }
            catch(Exception ex)
            {
                Console.WriteLine("\nMessage ---\n{0}", ex.Message);
                return null;
            }
        }

        /// <summary>
		/// Update Template
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template")]
        [HttpPut]
        public IActionResult Update([FromBody]TemplateViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _templateRepository.Update(model);
            return Json(result);
        }

        /// <summary>
		/// Delete Template
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _templateRepository.Delete(Id);
            return Json(result);
        }
        
        /// <summary>
		/// Enable Template
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/Template/Enable/{Id}")]
        [HttpPut]
        public IActionResult Enable(int Id)
        {
            var result = _templateRepository.Enable(Id);
            return Json(result);
        }

    }
}