﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class ChartChannelController : Controller
    {
        private readonly IChartChannelRepository _chartChannelRepository = new ChartChannelRepository();

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartChannel/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _chartChannelRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get ChartChannel
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartChannel/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _chartChannelRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Get ChartChannel
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartChannel/GetByChartConfigId/{Id}")]
        [HttpGet]
        public IActionResult GetByChartConfigId(int Id)
        {
            var result = _chartChannelRepository.GetByChartConfigId(Id);
            result.Remove(result.FirstOrDefault(x => x.XAxisChannelId != null));
            return Json(result);
        }

        /// <summary>
		/// Insert ChartChannel
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartChannel")]
        [HttpPost]
        public IActionResult Insert([FromBody]ChartChannelViewModel model)
        {
            var result = _chartChannelRepository.Insert(model, userId: 1);
            return Json(result);
        }

        /// <summary>
		/// Update ChartChannel
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartChannel")]
        [HttpPut]
        public IActionResult Update([FromBody]ChartChannelViewModel model)
        {
            var result = _chartChannelRepository.Update(model,userId:1);
            return Json(result);
        }

        /// <summary>
		/// Delete ChartChannel
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartChannel/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _chartChannelRepository.Delete(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert X-axis ChartChannel
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartChannel/UpdateXAxisChannel")]
        [HttpPost]
        public IActionResult UpdateXAxisChannel([FromBody]ChartChannelViewModel model)
        {
            var result = _chartChannelRepository.UpdateXAxisChannel(model, userId: 1);
            return Json(result);
        }

        [Route("api/ChartChannel/GetXaxisChannel/{Id}")]
        [HttpGet]
        public IActionResult GetXaxisChannel(int Id)
        {
            var result = _chartChannelRepository.GetByChartConfigId(Id);
            result.Remove(result.FirstOrDefault(x => x.XAxisChannelId == null));
            return Json(result);
        }

    }
}