﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class FormulaOperatorController : Controller
    {
        private readonly IFormulaOperatorRepository _formulaOperatorRepository= new FormulaOperatorRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        //public TemplateController(ITemplate template)
        //{
        //    _formulaOperatorRepository = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/FormulaOperator/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _formulaOperatorRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get FormulaOperator
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/FormulaOperator/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _formulaOperatorRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert FormulaOperator
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/FormulaOperator")]
        [HttpPost]
        public IActionResult Insert([FromBody]FormulaOperatorViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _formulaOperatorRepository.Insert(model);
            return Json(result);
        }

        /// <summary>
		/// Update FormulaOperator
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/FormulaOperator")]
        [HttpPut]
        public IActionResult Update([FromBody]FormulaOperatorViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _formulaOperatorRepository.Update(model);
            return Json(result);
        }

        /// <summary>
		/// Delete FormulaOperator
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/FormulaOperator/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _formulaOperatorRepository.Delete(Id);
            return Json(result);
        }

    }
}