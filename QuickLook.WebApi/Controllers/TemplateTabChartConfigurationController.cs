﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TemplateTabChartConfigurationController : Controller
    {
        private readonly ITemplateTabChartConfigurationRepository _templateTabChartConfigurationRepository = new TemplateTabChartConfigurationRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        private int userId = 1;

        //public TemplateTabChartConfigurationsController(ITemplateTabChartConfigurations template)
        //{
        //    _template = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl TemplateTabChartConfigurationsTabss
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfiguration/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _templateTabChartConfigurationRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabChartConfiguration
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfiguration/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _templateTabChartConfigurationRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Check TemplateTabChartConfigurationName Availability
        /// /// <param name="tabConfigId"></param>
        /// /// /// <param name="Name"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfiguration/CheckTabChartConfigName/{tabConfigId}/{chartTypeId}/{Name}")]
        [HttpGet]
        public IActionResult CheckTabChartConfigNameAvailability(int tabConfigId,int chartTypeId, string Name)
        {
            var result = _templateTabChartConfigurationRepository.CheckTabChartConfigName(tabConfigId, chartTypeId, Name);
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabChartConfiguration
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfiguration/GetByTemplateTabConfigId/{Id}")]
        [HttpGet]
        public IActionResult GetByTemplateTabConfigId(int Id)
        {
            var result = _templateTabChartConfigurationRepository.GetByTemplateTabConfigId(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert TemplateTabChartConfiguration
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfiguration")]
        [HttpPost]
        public IActionResult Insert([FromBody]TemplateTabChartConfigurationViewModel viewModel)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            viewModel.CreatedBy = User.Id;
            var result = _templateTabChartConfigurationRepository.Insert(viewModel, User.Id);
            return Json(result);
        }

        /// <summary>
		/// Update TemplateTabChartConfiguration
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfiguration")]
        [HttpPut]
        public IActionResult Update([FromBody]TemplateTabChartConfigurationViewModel viewModel)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            viewModel.CreatedBy = User.Id;
            var result = _templateTabChartConfigurationRepository.Update(viewModel, User.Id);
            return Json(result);
        }

        /// <summary>
		/// Delete TemplateTabChartConfiguration
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTabChartConfiguration/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            var result = _templateTabChartConfigurationRepository.Delete(Id, User.Id);
            return Json(result);
        }
    }
}