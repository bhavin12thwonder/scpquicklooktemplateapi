﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.AspNetCore.Mvc;
using QuickLook.Common;

namespace QuickLook.WebApi.Controllers
{
    public class QuickLookBaseController : ApiController
    {
        protected new HttpResponseMessage OkMessage<T>(T result, IViewModel viewModel)
        {

            return Request.CreateResponse(HttpStatusCode.OK, Envelope.Ok(result, viewModel.Message, viewModel.IsSuccess));
        }

        protected HttpResponseMessage OkMessage(IViewModel viewModel)
        {
            return Request.CreateResponse(HttpStatusCode.OK, Envelope.Ok(viewModel, viewModel.Message, viewModel.IsSuccess));
        }
    }
}