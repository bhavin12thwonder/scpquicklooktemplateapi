﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using QuickLook.Model;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;
using QuickLook.Model.ViewModel;
using Microsoft.AspNetCore.Authorization;
using System.Web.Http.Cors;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class TemplateTabsController : Controller
    {
        private readonly TemplateTabsRepository _templateTabs = new TemplateTabsRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        //public TemplateTabsController(ITemplateTabs template)
        //{
        //    _template = template;
        //}

        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl TemplateTabsTabss
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTab/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _templateTabs.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabs
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTab/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _templateTabs.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Get TemplateTabs
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTab/GetByTemplateId/{Id}")]
        [HttpGet]
        public IActionResult GetByTemplateId(int Id)
        {
            var result = _templateTabs.GetByTemplateId(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert TemplateTabs
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTab")]
        [HttpPost]
        public IActionResult Insert([FromBody]TemplateTabsCreateViewModel viewModel)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            viewModel.CreatedBy = User.Id;
            var result = _templateTabs.Insert(viewModel);
            return Json(result);
        }

        /// <summary>
		/// Update TemplateTabs
        /// /// <param name="viewModel"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTab")]
        [HttpPut]
        public IActionResult Update([FromBody]TemplateTabsViewModel viewModel)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            viewModel.CreatedBy = User.Id;
            var result = _templateTabs.Update(viewModel);
            return Json(result);
        }

        /// <summary>
		/// Delete TemplateTabs
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/TemplateTab/{templateId}/{tabIndex}")]
        [HttpDelete]
        public IActionResult Delete(int templateId, int tabIndex)
        {
            var result = _templateTabs.Delete(templateId, tabIndex);
            return Json(result);
        }

    }
}