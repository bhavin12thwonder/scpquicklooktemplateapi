﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.RepositoryImpl;

namespace QuickLook.WebApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
    [Authorize]
    public class ChartTypeController : Controller
    {
        private readonly IChartTypeRepository _chartTypeRepository = new ChartTypeRepository();
        private readonly IUserRepository _userRepository = new UserRepository();
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
		/// Get ALl Templates
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartType/GetAll")]
        [HttpGet]
        public IActionResult GetAll()
        {
            var result = _chartTypeRepository.GetAll();
            return Json(result);
        }

        /// <summary>
		/// Get ChartType
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartType/GetById/{Id}")]
        [HttpGet]
        public IActionResult GetById(int Id)
        {
            var result = _chartTypeRepository.GetById(Id);
            return Json(result);
        }

        /// <summary>
		/// Insert ChartType
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartType")]
        [HttpPost]
        public IActionResult Insert([FromBody]ChartTypeViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _chartTypeRepository.Insert(model);
            return Json(result);
        }

        /// <summary>
		/// Update ChartType
        /// /// <param name="model"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartType")]
        [HttpPut]
        public IActionResult Update([FromBody]ChartTypeViewModel model)
        {
            var UserName = HttpContext.User.Identity.Name;
            var User = _userRepository.GetByUserName(UserName.Substring(UserName.IndexOf(@"\") + 1));
            model.CreatedBy = User.Id;
            var result = _chartTypeRepository.Update(model);
            return Json(result);
        }

        /// <summary>
		/// Delete ChartType
        /// /// <param name="Id"></param>
		/// </summary>
		/// <returns></returns>
		[Route("api/ChartType/{Id}")]
        [HttpDelete]
        public IActionResult Delete(int Id)
        {
            var result = _chartTypeRepository.Delete(Id);
            return Json(result);
        }
    }
}