﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuickLook.ASAMAPI.Model
{
    public class ReturnObject
    {
    }

    public class CalculationValueObject
    {
        public string Name { get; set; }
        public double Value { get; set; }

        public string Unit { get; set; }
    }
    public class CalculationArrayObject
    {
        public string Name { get; set; }
        public double[] Value { get; set; }

        public string Unit { get; set; }
    }

    public class CalcATDReqObject
    {
        public string CalcName { get; set; }
        public string ATD { get; set; }
    }

    public class AllTestObject
    {
        public string TestName { get; set; }
        public string  SubMatrix { get; set; }

        public int TestId { get; set; }
    }

    public class ChannelConvertData
    {
        public string ChannelName { get; set; }
        public string Unit { get; set; }
        public int TestId { get; set; }
        public string SubMatrix { get; set; }
    }
}