﻿using org.asam.ods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Runtime.Remoting.Channels;
using omg.org.CosNaming;
using Ch.Elca.Iiop;
using Ch.Elca.Iiop.Services;
using System.Configuration;
using System.Web.Http.Cors;
using QuickLook.ASAMAPI.Model;
using System.Web;

namespace QuickLook.ASAMAPI.Controller
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ASAMController : ApiController
    {
        public static AoSession aoSession = null;

        [HttpPost]
        public IEnumerable<CalculationArrayObject> ConvertData_Unit(ChannelConvertData channel)
        {
            List<CalculationArrayObject> calculationsName = new List<CalculationArrayObject>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_channel = asObj.getElementByName("Channel");
                ApplicationElement ae_localColumn = asObj.getElementByName("Local_Column");
                ApplicationElement ae_submatrix = asObj.getElementByName("Submatrix");
                ApplicationElement ae_Unit = asObj.getElementByName("Physical_Unit");

                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidChannel = ae_channel.getId();
                T_LONGLONG aidLocalColumn = ae_localColumn.getId();
                T_LONGLONG aidSubmatrix = ae_submatrix.getId();
                T_LONGLONG aidUnit = ae_Unit.getId();

                QueryStructureExt aoqVer = new QueryStructureExt();
                aoqVer.anuSeq = new SelAIDNameUnitId[2];
                aoqVer.anuSeq[0] = createAnu(aidMeasurement, "TestRequestId");
                aoqVer.anuSeq[1] = createAnu(aidMeasurement, "Version");

                aoqVer.condSeq = new SelItem[1];
                aoqVer.condSeq[0] = new SelItem();
                SelValueExt selValueVer = new SelValueExt();
                selValueVer.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
                selValueVer.oper = SelOpcode.EQ;
                selValueVer.value = new TS_Value(new TS_Union(), (short)15);
                selValueVer.value.u.SetlongVal(channel.TestId);
                aoqVer.condSeq[0].Setvalue(selValueVer);
                aoqVer.orderBy = new SelOrder[0];
                aoqVer.groupBy = new AIDName[0];

                aoqVer.joinSeq = new JoinDef[0];
                ResultSetExt[] resVer = aea.getInstancesExt(aoqVer, 100);

                int[] testRequestId = resVer[0].firstElems[0].values[0].value.u.GetlongVal();
                string[] Version = resVer[0].firstElems[0].values[1].value.u.GetstringVal();
                ///////////////////////////////////////////////
                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[4];
                aoq.anuSeq[0] = createAnu(aidSubmatrix, "Id");
                aoq.anuSeq[1] = createAnu(aidChannel, "Description");
                aoq.anuSeq[2] = createAnu(aidChannel, "Name_Of_The_Channel");
                aoq.anuSeq[3] = createAnu(aidUnit, "Name");


                aoq.condSeq = new SelItem[5];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "TestRequestId"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetlongVal(testRequestId[0]);
                aoq.condSeq[0].Setvalue(selValue);


                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                SelValueExt selValue1 = new SelValueExt();
                selValue1.attr = new AIDNameUnitId(new AIDName(aidChannel, "Description"), new T_LONGLONG(0, 0));
                selValue1.oper = SelOpcode.CI_EQ;
                selValue1.value = new TS_Value(new TS_Union(), (short)15);
                selValue1.value.u.SetstringVal(channel.ChannelName);
                aoq.condSeq[2].Setvalue(selValue1);

                aoq.condSeq[3] = new SelItem();
                aoq.condSeq[3].Setoperator(SelOperator.AND);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Version"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.EQ;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal(Version[0]);
                aoq.condSeq[4].Setvalue(selValue2);


                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];

                aoq.joinSeq = new JoinDef[4];


                aoq.joinSeq[0].fromAID = aidMeasurement;
                aoq.joinSeq[0].toAID = aidSubmatrix;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "Submatrices";

                aoq.joinSeq[1].fromAID = aidSubmatrix;
                aoq.joinSeq[1].toAID = aidLocalColumn;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "Local_Columns";

                aoq.joinSeq[2].fromAID = aidLocalColumn;
                aoq.joinSeq[2].toAID = aidChannel;
                aoq.joinSeq[2].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[2].refName = "Channel";

                aoq.joinSeq[3].fromAID = aidChannel;
                aoq.joinSeq[3].toAID = aidUnit;
                aoq.joinSeq[3].joiningType = JoinType.JTOUTER;
                aoq.joinSeq[3].refName = "unit";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                T_LONGLONG[] ieIds = res[0].firstElems[0].values[0].value.u.GetlonglongVal();
                string[] ChannelDescription = res[0].firstElems[1].values[0].value.u.GetstringVal();
                string[] ChannelName = res[0].firstElems[1].values[1].value.u.GetstringVal();
                string[] ChannelUnit = res[0].firstElems[2].values[0].value.u.GetstringVal();
                for (int i = 0; i < ChannelDescription.Length; i++)
                {
                    ValueMatrix vm = aea.getValueMatrix(new ElemId(aidSubmatrix, ieIds[i]));
                    Column[] cols = vm.getColumns(ChannelName[i]);
                    if (cols.Length > 0)
                    {
                        var calObj = new CalculationArrayObject
                        {
                            Name = ChannelDescription[i],
                            Value = getDataUnit(vm, ChannelName[i], channel.Unit),
                            Unit = ChannelUnit[i]
                        };
                        calculationsName.Add(calObj);
                    }

                }


            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }
            //try
            //{
            //    //if (aoSession == null)
            //    //    aoSession = Con_NameService();
            //    //ApplicationStructure asObj = aoSession.getApplicationStructure();
            //    //ApplElemAccess aea = aoSession.getApplElemAccess();
            //    //ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
            //    //ApplicationElement ae_channel = asObj.getElementByName("Channel");
            //    //ApplicationElement ae_localColumn = asObj.getElementByName("Local_Column");
            //    //ApplicationElement ae_submatrix = asObj.getElementByName("Submatrix");
            //    //ApplicationElement ae_Unit = asObj.getElementByName("Physical_Unit");

            //    //T_LONGLONG aidMeasurement = ae_Measurement.getId();
            //    //T_LONGLONG aidChannel = ae_channel.getId();
            //    //T_LONGLONG aidLocalColumn = ae_localColumn.getId();
            //    //T_LONGLONG aidSubmatrix = ae_submatrix.getId();
            //    //T_LONGLONG aidUnit = ae_Unit.getId();

            //    //QueryStructureExt aoq = new QueryStructureExt();
            //    //aoq.anuSeq = new SelAIDNameUnitId[3];
            //    //aoq.anuSeq[0] = createAnu(aidSubmatrix, "Id");
            //    //aoq.anuSeq[1] = createAnu(aidChannel, "Description");
            //    //aoq.anuSeq[2] = createAnu(aidChannel, "Name_Of_The_Channel");



            //    //aoq.condSeq = new SelItem[5];
            //    //aoq.condSeq[0] = new SelItem();
            //    //SelValueExt selValue = new SelValueExt();
            //    //selValue.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
            //    //selValue.oper = SelOpcode.EQ;
            //    //selValue.value = new TS_Value(new TS_Union(), (short)15);
            //    //selValue.value.u.SetlongVal(channel.TestId);
            //    //aoq.condSeq[0].Setvalue(selValue);


            //    //aoq.condSeq[1] = new SelItem();
            //    //aoq.condSeq[1].Setoperator(SelOperator.AND);

            //    //SelValueExt selValue1 = new SelValueExt();
            //    //selValue1.attr = new AIDNameUnitId(new AIDName(aidChannel, "Description"), new T_LONGLONG(0, 0));
            //    //selValue1.oper = SelOpcode.EQ;
            //    //selValue1.value = new TS_Value(new TS_Union(), (short)15);
            //    //selValue1.value.u.SetstringVal(channel.ChannelName);
            //    //aoq.condSeq[2].Setvalue(selValue1);

            //    //aoq.condSeq[3] = new SelItem();
            //    //aoq.condSeq[3].Setoperator(SelOperator.AND);

            //    //SelValueExt selValue2 = new SelValueExt();
            //    //selValue2.attr = new AIDNameUnitId(new AIDName(aidSubmatrix, "Name"), new T_LONGLONG(0, 0));
            //    //selValue2.oper = SelOpcode.EQ;
            //    //selValue2.value = new TS_Value(new TS_Union(), (short)15);
            //    //selValue2.value.u.SetstringVal(channel.SubMatrix);
            //    //aoq.condSeq[4].Setvalue(selValue2);


            //    //aoq.orderBy = new SelOrder[0];
            //    //aoq.groupBy = new AIDName[0];

            //    //aoq.joinSeq = new JoinDef[4];


            //    //aoq.joinSeq[0].fromAID = aidMeasurement;
            //    //aoq.joinSeq[0].toAID = aidSubmatrix;
            //    //aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
            //    //aoq.joinSeq[0].refName = "Submatrices";

            //    //aoq.joinSeq[1].fromAID = aidSubmatrix;
            //    //aoq.joinSeq[1].toAID = aidLocalColumn;
            //    //aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
            //    //aoq.joinSeq[1].refName = "Local_Columns";

            //    //aoq.joinSeq[2].fromAID = aidLocalColumn;
            //    //aoq.joinSeq[2].toAID = aidChannel;
            //    //aoq.joinSeq[2].joiningType = JoinType.JTDEFAULT;
            //    //aoq.joinSeq[2].refName = "Channel";

            //    //aoq.joinSeq[3].fromAID = aidChannel;
            //    //aoq.joinSeq[3].toAID = aidUnit;
            //    //aoq.joinSeq[3].joiningType = JoinType.JTOUTER;
            //    //aoq.joinSeq[3].refName = "unit";

            //    //ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

            //    //T_LONGLONG[] ieIds = res[0].firstElems[0].values[0].value.u.GetlonglongVal();
            //    //string[] ChannelDescription = res[0].firstElems[1].values[0].value.u.GetstringVal();
            //    //string[] ChannelName = res[0].firstElems[1].values[1].value.u.GetstringVal();

            //    for (int i = 0; i < ChannelDescription.Length; i++)
            //    {
            //        ValueMatrix vm = aea.getValueMatrix(new ElemId(aidSubmatrix, ieIds[i]));
            //        Column[] cols = vm.getColumns(ChannelName[i]);
            //        if (cols.Length > 0)
            //        {
            //            var calObj = new CalculationArrayObject
            //            {
            //                Name = ChannelDescription[i],
            //                Value = getDataUnit(vm, ChannelName[i], channel.Unit),
            //                Unit = channel.Unit
            //            };
            //            calculationsName.Add(calObj);
            //        }

            //    }


            //}
            //catch (AoException ex)
            //{
            //}
            //finally
            //{
            //    if (aoSession != null)
            //    {
            //        aoSession.close();
            //        aoSession = null;
            //    }
            //}

            return calculationsName;
        }

        [HttpPost]
        public IEnumerable<CalculationValueObject> GetCalculationValue_ATD(int testId, [FromBody] CalcATDReqObject[] calcATD)
        {
            List<CalculationValueObject> calculationsName = new List<CalculationValueObject>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();

                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_XCrashCalc = asObj.getElementByName("X-Crash-Calculation");
                ApplicationElement ae_XCrashResult = asObj.getElementByName("X-Crash-Result");
                ApplicationElement ae_Unit = asObj.getElementByName("Physical_Unit");

                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidXCrashCalc = ae_XCrashCalc.getId();
                T_LONGLONG aidXCrashResult = ae_XCrashResult.getId();
                T_LONGLONG aidUnit = ae_Unit.getId();

                QueryStructureExt aoqVer = new QueryStructureExt();
                aoqVer.anuSeq = new SelAIDNameUnitId[1];
                aoqVer.anuSeq[0] = createAnu(aidMeasurement, "Version");

                aoqVer.condSeq = new SelItem[1];
                aoqVer.condSeq[0] = new SelItem();
                SelValueExt selValueVer = new SelValueExt();
                selValueVer.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
                selValueVer.oper = SelOpcode.CI_EQ;
                selValueVer.value = new TS_Value(new TS_Union(), (short)15);
                selValueVer.value.u.SetlongVal(testId);
                aoqVer.condSeq[0].Setvalue(selValueVer);

                aoqVer.orderBy = new SelOrder[0];
                aoqVer.groupBy = new AIDName[0];

                aoqVer.joinSeq = new JoinDef[0];

                ResultSetExt[] resVer = aea.getInstancesExt(aoqVer, 10000000);

                string[] Result_Version = resVer[0].firstElems[0].values[0].value.u.GetstringVal();

                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[5];
                aoq.anuSeq[0] = createAnu(aidXCrashResult, "ResultType");
                aoq.anuSeq[1] = createAnu(aidXCrashResult, "ValueType");
                aoq.anuSeq[2] = createAnu(aidXCrashResult, "Value");
                aoq.anuSeq[3] = createAnu(aidXCrashResult, "ATDPosition");
                aoq.anuSeq[4] = createAnu(aidUnit, "Name");

                aoq.condSeq = new SelItem[3];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.NEQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetlongVal(testId);
                aoq.condSeq[0].Setvalue(selValue);

                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                aoq.condSeq[2] = new SelItem();
                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Version"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.EQ;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal(Result_Version[0]);
                aoq.condSeq[2].Setvalue(selValue2);


                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];

                aoq.joinSeq = new JoinDef[3];


                aoq.joinSeq[0].fromAID = aidMeasurement;
                aoq.joinSeq[0].toAID = aidXCrashCalc;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "X-Crash-Calculations";

                aoq.joinSeq[1].fromAID = aidXCrashCalc;
                aoq.joinSeq[1].toAID = aidXCrashResult;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "X-Crash-Results";

                aoq.joinSeq[2].fromAID = aidXCrashResult;
                aoq.joinSeq[2].toAID = aidUnit;
                aoq.joinSeq[2].joiningType = JoinType.JTOUTER;
                aoq.joinSeq[2].refName = "Unit";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                string[] Result_ResultType = res[0].firstElems[0].values[0].value.u.GetstringVal();
                string[] Result_ValueType = res[0].firstElems[0].values[1].value.u.GetstringVal();
                double[] Result_Value = res[0].firstElems[0].values[2].value.u.GetdoubleVal();
                string[] Result_ATD = res[0].firstElems[0].values[3].value.u.GetstringVal();
                string[] Result_Unit = res[0].firstElems[1].values[0].value.u.GetstringVal();

                for (int i = 0; i < calcATD.Length; i++)
                {
                    int k = calcATD[i].CalcName.LastIndexOf('_');
                    string CalcName = calcATD[i].CalcName.Substring(0, k);
                    string ValueType = calcATD[i].CalcName.Substring(k + 1, calcATD[i].CalcName.Length - k - 1);

                    for (int j = 0; j < Result_ResultType.Length; j++)
                    {
                        if (CalcName.ToUpper() == Result_ResultType[j].ToUpper() && ValueType.ToUpper() == Result_ValueType[j].ToUpper() && calcATD[i].ATD.ToUpper() == Result_ATD[j].ToUpper())
                        {
                            var classObj = new CalculationValueObject
                            {
                                Name = calcATD[i].CalcName,
                                Value = Result_Value[j],
                                Unit = Result_Unit[j],
                            };

                            calculationsName.Add(classObj);
                            break;
                        }
                    }
                }

            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }


            return calculationsName;

        }

        [HttpPost]
        public IEnumerable<CalculationValueObject> GetCalculationValue_ATD_Old(int testRequestId, [FromBody] CalcATDReqObject[] calcATD)
        {
            List<CalculationValueObject> calculationsName = new List<CalculationValueObject>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();

                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_XCrashCalc = asObj.getElementByName("X-Crash-Calculation");
                ApplicationElement ae_XCrashResult = asObj.getElementByName("X-Crash-Result");
                ApplicationElement ae_Unit = asObj.getElementByName("Physical_Unit");

                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidXCrashCalc = ae_XCrashCalc.getId();
                T_LONGLONG aidXCrashResult = ae_XCrashResult.getId();
                T_LONGLONG aidUnit = ae_Unit.getId();

                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[5];
                aoq.anuSeq[0] = createAnu(aidXCrashResult, "ResultType");
                aoq.anuSeq[1] = createAnu(aidXCrashResult, "ValueType");
                aoq.anuSeq[2] = createAnu(aidXCrashResult, "Value"); 
                aoq.anuSeq[3] = createAnu(aidXCrashResult, "ATDPosition");
                aoq.anuSeq[4] = createAnu(aidUnit, "Name");

                aoq.condSeq = new SelItem[1];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.CI_EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetlongVal(testRequestId);
                aoq.condSeq[0].Setvalue(selValue);



                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];

                aoq.joinSeq = new JoinDef[3];


                aoq.joinSeq[0].fromAID = aidMeasurement;
                aoq.joinSeq[0].toAID = aidXCrashCalc;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "X-Crash-Calculations";

                aoq.joinSeq[1].fromAID = aidXCrashCalc;
                aoq.joinSeq[1].toAID = aidXCrashResult;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "X-Crash-Results";

                aoq.joinSeq[2].fromAID = aidXCrashResult;
                aoq.joinSeq[2].toAID = aidUnit;
                aoq.joinSeq[2].joiningType = JoinType.JTOUTER;
                aoq.joinSeq[2].refName = "Unit";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                string[] Result_ResultType = res[0].firstElems[0].values[0].value.u.GetstringVal();
                string[] Result_ValueType = res[0].firstElems[0].values[1].value.u.GetstringVal();
                double[] Result_Value = res[0].firstElems[0].values[2].value.u.GetdoubleVal();
                string[] Result_ATD = res[0].firstElems[0].values[3].value.u.GetstringVal();
                string[] Result_Unit = res[0].firstElems[1].values[0].value.u.GetstringVal();

                for (int i = 0; i < calcATD.Length; i++)
                {
                    int k = calcATD[i].CalcName.LastIndexOf('_');
                    string CalcName = calcATD[i].CalcName.Substring(0, k);
                    string ValueType = calcATD[i].CalcName.Substring(k + 1, calcATD[i].CalcName.Length - k - 1);

                    for (int j = 0; j < Result_ResultType.Length; j++)
                    {
                        if (CalcName.ToUpper() == Result_ResultType[j].ToUpper() && ValueType.ToUpper() == Result_ValueType[j].ToUpper() && calcATD[i].ATD.ToUpper() == Result_ATD[j].ToUpper())
                        {
                            var classObj = new CalculationValueObject
                            {
                                Name = calcATD[i].CalcName,
                                Value = Result_Value[j],
                                Unit = Result_Unit[j],
                            };

                            calculationsName.Add(classObj);
                            break;
                        }
                    }
                }

            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }
            return calculationsName;
        }

        #region output
        // Sample output
        //“mass”
        #endregion

        // GET api/<controller>
        [Route("api/ASAM/GetPhysicalUnit/{channelName}")]
        [HttpGet]
        public string GetPhysicalUnit(string channelName)
        {
            string unitName = "";
            
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_possibleChannel = asObj.getElementByName("Possible_Channel");
                ApplicationElement ae_physicalUnit = asObj.getElementByName("Physical_Unit");
                ApplicationElement ae_physicalDimension = asObj.getElementByName("PhysicalDimension");
                
                T_LONGLONG aidpossibleChannel = ae_possibleChannel.getId();
                T_LONGLONG aidphysicalUnit = ae_physicalUnit.getId();
                T_LONGLONG aidphysicalDimension = ae_physicalDimension.getId();
               
                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[1];
                aoq.anuSeq[0] = createAnuDistinct(aidphysicalDimension, "Name");

                aoq.condSeq = new SelItem[3];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidpossibleChannel, "name"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.CI_EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetstringVal(channelName);
                aoq.condSeq[0].Setvalue(selValue);

                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.OR);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidpossibleChannel, "Description"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.CI_EQ;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal(channelName);
                aoq.condSeq[2].Setvalue(selValue2);

                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];


                aoq.joinSeq = new JoinDef[0];
      
                

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000);

                string[] tempPhyUnit = res[0].firstElems[0].values[0].value.u.GetstringVal();
                if (tempPhyUnit.Length > 0)
                    unitName = tempPhyUnit[0];

            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }

            return unitName;
        }

        #region output
        // Sample output
        //[
        //    "kg",
        //    "g",
        //    "grams",
        //    "mg",
        //    "lb",
        //    "G"
        //]
        #endregion
        // GET api/<controller>
        [Route("api/ASAM/GetChannelUnit/{channelName}")]
        [HttpGet]
        public IEnumerable<string> GetChannelUnit(string channelName)
        {
            List<string> unitName = new List<string>();

            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_possibleChannel = asObj.getElementByName("Possible_Channel");
                ApplicationElement ae_physicalUnit = asObj.getElementByName("Physical_Unit");
                ApplicationElement ae_physicalDimension = asObj.getElementByName("PhysicalDimension");

                T_LONGLONG aidpossibleChannel = ae_possibleChannel.getId();
                T_LONGLONG aidphysicalUnit = ae_physicalUnit.getId();
                T_LONGLONG aidphysicalDimension = ae_physicalDimension.getId();

                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[1];
                aoq.anuSeq[0] = createAnuDistinct(aidphysicalDimension, "Name");

                aoq.condSeq = new SelItem[3];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidpossibleChannel, "name"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.CI_EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetstringVal(channelName);
                aoq.condSeq[0].Setvalue(selValue);

                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.OR);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidpossibleChannel, "Description"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.CI_EQ;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal(channelName);
                aoq.condSeq[2].Setvalue(selValue2);



                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];


                aoq.joinSeq = new JoinDef[0];



                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000);

                string[] tempPhyUnit = res[0].firstElems[0].values[0].value.u.GetstringVal();
                if (tempPhyUnit.Length > 0)
                {
                    string physicalUnitName = tempPhyUnit[0];
                    QueryStructureExt aoqUnit = new QueryStructureExt();
                    aoqUnit.anuSeq = new SelAIDNameUnitId[1];
                    aoqUnit.anuSeq[0] = createAnuDistinct(aidphysicalUnit, "Name");
                    aoqUnit.condSeq = new SelItem[1];
                    aoqUnit.condSeq[0] = new SelItem();
                    SelValueExt selValueUnit = new SelValueExt();
                    selValueUnit.attr = new AIDNameUnitId(new AIDName(aidphysicalDimension, "Name"), new T_LONGLONG(0, 0));
                    selValueUnit.oper = SelOpcode.EQ;
                    selValueUnit.value = new TS_Value(new TS_Union(), (short)15);
                    selValueUnit.value.u.SetstringVal(physicalUnitName);
                    aoqUnit.condSeq[0].Setvalue(selValueUnit);
                    aoqUnit.orderBy = new SelOrder[0];
                    aoqUnit.groupBy = new AIDName[0];


                    aoqUnit.joinSeq = new JoinDef[0];



                    ResultSetExt[] resUnit = aea.getInstancesExt(aoqUnit, 100);
                    string[] Unit = resUnit[0].firstElems[0].values[0].value.u.GetstringVal();
                    unitName.AddRange(Unit);

                }

            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }

            return unitName;
        }

        public IEnumerable<string> GetATD(string testType, string testMode)
        {

            var result = GetAllATD(testType, testMode);
            return result;
        }

        private List<string> GetAllATD(string testType, string testMode)
        {
            List<string> atdName = new List<string>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_typeOfTest = asObj.getElementByName("Type_Of_Test");
                ApplicationElement ae_subTypeOfTest = asObj.getElementByName("Subtype_Of_Test");
                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_XCrashCalc = asObj.getElementByName("X-Crash-Calculation");
                ApplicationElement ae_XCrashResult = asObj.getElementByName("X-Crash-Result");
                T_LONGLONG aidtypeOfTest = ae_typeOfTest.getId();
                T_LONGLONG aidsubTypeOfTest = ae_subTypeOfTest.getId();
                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidXCrashCalc = ae_XCrashCalc.getId();
                T_LONGLONG aidXCrashResult = ae_XCrashResult.getId();
                String[] aaNames = new String[] { "Id" };
                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[1];
                aoq.anuSeq[0] = createAnuDistinct(aidXCrashResult, "ATDPosition");
               
                aoq.condSeq = new SelItem[7];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidtypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetstringVal(testType);
                aoq.condSeq[0].Setvalue(selValue);

                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                SelValueExt selValue1 = new SelValueExt();
                selValue1.attr = new AIDNameUnitId(new AIDName(aidsubTypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValue1.oper = SelOpcode.EQ;
                selValue1.value = new TS_Value(new TS_Union(), (short)15);
                selValue1.value.u.SetstringVal(testMode);
                aoq.condSeq[2].Setvalue(selValue1);

                aoq.condSeq[3] = new SelItem();
                aoq.condSeq[3].Setoperator(SelOperator.AND);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Name"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.LIKE;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal("*_filtered");
                aoq.condSeq[4].Setvalue(selValue2);

                aoq.condSeq[5] = new SelItem();
                aoq.condSeq[5].Setoperator(SelOperator.AND);

                SelValueExt selValue3 = new SelValueExt();
                selValue3.attr = new AIDNameUnitId(new AIDName(aidXCrashCalc, "Name"), new T_LONGLONG(0, 0));
                selValue3.oper = SelOpcode.LIKE;
                selValue3.value = new TS_Value(new TS_Union(), (short)15);
                selValue3.value.u.SetstringVal("*");
                aoq.condSeq[6].Setvalue(selValue3);

                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];
               

                aoq.joinSeq = new JoinDef[4];
                aoq.joinSeq[0].fromAID = aidsubTypeOfTest;
                aoq.joinSeq[0].toAID = aidMeasurement;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "Safety_Tests";

                aoq.joinSeq[1].fromAID = aidtypeOfTest;
                aoq.joinSeq[1].toAID = aidsubTypeOfTest;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "Subtype_Of_Tests";

                aoq.joinSeq[2].fromAID = aidMeasurement;
                aoq.joinSeq[2].toAID = aidXCrashCalc;
                aoq.joinSeq[2].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[2].refName = "X-Crash-Calculations";

                aoq.joinSeq[3].fromAID = aidXCrashCalc;
                aoq.joinSeq[3].toAID = aidXCrashResult;
                aoq.joinSeq[3].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[3].refName = "X-Crash-Results";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                string[] ATDType = res[0].firstElems[0].values[0].value.u.GetstringVal();
                
                atdName = ATDType.ToList();
            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }




            return atdName;
        }

        #region output
        // Sample output
        //[
        //{
        //"Name": "DRIV. 1 AB RB YR",
        // "Value": [
        //-0.34749112426035439,
        //-0.077220249835634011, 
        //0.23166074950690357, 
        //0.15444049967126919, 
        //0.3861012491781724, 
        //0.115830374753452, 
        //0.15444049967126919,
        //0.115830374753452,
        //0.15444049967126919,
        //0.0772202498356348,
        //-0.038610124917816804
        //],
        //"Unit": "g"
        //},
        //{
        //"Name": " C-PLR_SHOLD_G_Y1",
        //"Value": [ 0.011975,
        //0.011973,
        //0.011967,
        //0.011957,
        //0.011942,
        //0.011924,
        //0.011902,
        //0.011878,
        //0.011854,
        //0.011832,
        //0.011814
        //],
        //"Unit": "g"
        //}
        //]

        #endregion
        public IEnumerable<CalculationArrayObject> GetChannelData(int testId, [FromUri] string[] channels)
        {
            List<CalculationArrayObject> calculationsName = new List<CalculationArrayObject>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_channel = asObj.getElementByName("Channel");
                ApplicationElement ae_localColumn = asObj.getElementByName("Local_Column");
                ApplicationElement ae_submatrix = asObj.getElementByName("Submatrix");
                ApplicationElement ae_Unit = asObj.getElementByName("Physical_Unit");

                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidChannel = ae_channel.getId();
                T_LONGLONG aidLocalColumn = ae_localColumn.getId();
                T_LONGLONG aidSubmatrix = ae_submatrix.getId();
                T_LONGLONG aidUnit = ae_Unit.getId();

                QueryStructureExt aoqVer = new QueryStructureExt();
                aoqVer.anuSeq = new SelAIDNameUnitId[2];
                aoqVer.anuSeq[0] = createAnu(aidMeasurement, "TestRequestId");
                aoqVer.anuSeq[1] = createAnu(aidMeasurement, "Version");

                aoqVer.condSeq = new SelItem[1];
                aoqVer.condSeq[0] = new SelItem();
                SelValueExt selValueVer = new SelValueExt();
                selValueVer.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
                selValueVer.oper = SelOpcode.EQ;
                selValueVer.value = new TS_Value(new TS_Union(), (short)15);
                selValueVer.value.u.SetlongVal(testId);
                aoqVer.condSeq[0].Setvalue(selValueVer);
                aoqVer.orderBy = new SelOrder[0];
                aoqVer.groupBy = new AIDName[0];

                aoqVer.joinSeq = new JoinDef[0];
                ResultSetExt[] resVer = aea.getInstancesExt(aoqVer, 100);

                int[] testRequestId = resVer[0].firstElems[0].values[0].value.u.GetlongVal();
                string[] Version = resVer[0].firstElems[0].values[1].value.u.GetstringVal();
                ///////////////////////////////////////////////
                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[4];
                aoq.anuSeq[0] = createAnu(aidSubmatrix, "Id");
                aoq.anuSeq[1] = createAnu(aidChannel, "Description");
                aoq.anuSeq[2] = createAnu(aidChannel, "Name_Of_The_Channel");
                aoq.anuSeq[3] = createAnu(aidUnit, "Name");


                aoq.condSeq = new SelItem[5];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "TestRequestId"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetlongVal(testRequestId[0]);
                aoq.condSeq[0].Setvalue(selValue);


                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                SelValueExt selValue1 = new SelValueExt();
                selValue1.attr = new AIDNameUnitId(new AIDName(aidChannel, "Description"), new T_LONGLONG(0, 0));
                selValue1.oper = SelOpcode.CI_INSET;
                selValue1.value = new TS_Value(new TS_Union(), (short)15);
                selValue1.value.u.SetstringSeq(channels);
                aoq.condSeq[2].Setvalue(selValue1);

                aoq.condSeq[3] = new SelItem();
                aoq.condSeq[3].Setoperator(SelOperator.AND);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Version"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.EQ;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal(Version[0]);
                aoq.condSeq[4].Setvalue(selValue2);


                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];

                aoq.joinSeq = new JoinDef[4];


                aoq.joinSeq[0].fromAID = aidMeasurement;
                aoq.joinSeq[0].toAID = aidSubmatrix;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "Submatrices";

                aoq.joinSeq[1].fromAID = aidSubmatrix;
                aoq.joinSeq[1].toAID = aidLocalColumn;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "Local_Columns";

                aoq.joinSeq[2].fromAID = aidLocalColumn;
                aoq.joinSeq[2].toAID = aidChannel;
                aoq.joinSeq[2].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[2].refName = "Channel";

                aoq.joinSeq[3].fromAID = aidChannel;
                aoq.joinSeq[3].toAID = aidUnit;
                aoq.joinSeq[3].joiningType = JoinType.JTOUTER;
                aoq.joinSeq[3].refName = "unit";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                T_LONGLONG[] ieIds = res[0].firstElems[0].values[0].value.u.GetlonglongVal();
                string[] ChannelDescription = res[0].firstElems[1].values[0].value.u.GetstringVal();
                string[] ChannelName = res[0].firstElems[1].values[1].value.u.GetstringVal();
                string[] ChannelUnit = res[0].firstElems[2].values[0].value.u.GetstringVal();
                for (int i = 0; i < ChannelDescription.Length; i++)
                {
                    ValueMatrix vm = aea.getValueMatrix(new ElemId(aidSubmatrix, ieIds[i]));
                    Column[] cols = vm.getColumns(ChannelName[i]);
                    if (cols.Length > 0)
                    {
                        var calObj = new CalculationArrayObject
                        {
                            Name = ChannelDescription[i],
                            Value = getColumnData(vm, cols[0]),
                            Unit = ChannelUnit[i]
                        };
                        calculationsName.Add(calObj);
                    }

                }


            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }


            return calculationsName;
        }


        public IEnumerable<CalculationArrayObject> GetChannelData_Old(int testRequestId, [FromUri] string[] channels)
        {
            List<CalculationArrayObject> calculationsName = new List<CalculationArrayObject>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_channel = asObj.getElementByName("Channel");
                ApplicationElement ae_localColumn = asObj.getElementByName("Local_Column");
                ApplicationElement ae_submatrix = asObj.getElementByName("Submatrix");
                ApplicationElement ae_Unit = asObj.getElementByName("Physical_Unit");

                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidChannel = ae_channel.getId();
                T_LONGLONG aidLocalColumn = ae_localColumn.getId();
                T_LONGLONG aidSubmatrix = ae_submatrix.getId();
                T_LONGLONG aidUnit = ae_Unit.getId();

                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[4];
                aoq.anuSeq[0] = createAnu(aidSubmatrix, "Id");
                aoq.anuSeq[1] = createAnu(aidChannel, "Description");
                aoq.anuSeq[2] = createAnu(aidChannel, "Name_Of_The_Channel");
                aoq.anuSeq[3] = createAnu(aidUnit, "Name");


                aoq.condSeq = new SelItem[5];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "TestRequestId"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetlongVal(testRequestId);
                aoq.condSeq[0].Setvalue(selValue);


                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                SelValueExt selValue1 = new SelValueExt();
                selValue1.attr = new AIDNameUnitId(new AIDName(aidChannel, "Description"), new T_LONGLONG(0, 0));
                selValue1.oper = SelOpcode.CI_INSET;
                selValue1.value = new TS_Value(new TS_Union(), (short)15);
                selValue1.value.u.SetstringSeq(channels);
                aoq.condSeq[2].Setvalue(selValue1);

                aoq.condSeq[3] = new SelItem();
                aoq.condSeq[3].Setoperator(SelOperator.AND);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Name"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.LIKE;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal("*_filtered");
                aoq.condSeq[4].Setvalue(selValue2);


                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];

                aoq.joinSeq = new JoinDef[4];


                aoq.joinSeq[0].fromAID = aidMeasurement;
                aoq.joinSeq[0].toAID = aidSubmatrix;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "Submatrices";

                aoq.joinSeq[1].fromAID = aidSubmatrix;
                aoq.joinSeq[1].toAID = aidLocalColumn;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "Local_Columns";

                aoq.joinSeq[2].fromAID = aidLocalColumn;
                aoq.joinSeq[2].toAID = aidChannel;
                aoq.joinSeq[2].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[2].refName = "Channel";

                aoq.joinSeq[3].fromAID = aidChannel;
                aoq.joinSeq[3].toAID = aidUnit;
                aoq.joinSeq[3].joiningType = JoinType.JTOUTER;
                aoq.joinSeq[3].refName = "unit";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                T_LONGLONG[] ieIds = res[0].firstElems[0].values[0].value.u.GetlonglongVal();
                string[] ChannelDescription = res[0].firstElems[1].values[0].value.u.GetstringVal();
                string[] ChannelName = res[0].firstElems[1].values[1].value.u.GetstringVal();
                string[] ChannelUnit = res[0].firstElems[2].values[0].value.u.GetstringVal();
                for (int i=0;i< ChannelDescription.Length;i++)
                {
                    ValueMatrix vm = aea.getValueMatrix(new ElemId(aidSubmatrix, ieIds[i]));
                    Column[] cols = vm.getColumns(ChannelName[i]);
                    if (cols.Length > 0)
                    {
                        var calObj = new CalculationArrayObject
                        {
                            Name = ChannelDescription[i],
                            Value = getColumnData(vm, cols[0]),
                            Unit = ChannelUnit[i]
                        };
                        calculationsName.Add(calObj);
                    }

                }

                
            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }


            return calculationsName;
        }

        public IEnumerable<CalculationValueObject> GetCalculationValue(int testId, [FromUri] string[] calculations)
        {
            List<CalculationValueObject> calculationsName = new List<CalculationValueObject>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_SourceFor = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_XCrashCalc = asObj.getElementByName("X-Crash-Calculation");
                ApplicationElement ae_XCrashResult = asObj.getElementByName("X-Crash-Result");
                ApplicationElement ae_Unit = asObj.getElementByName("Physical_Unit");

                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidXCrashCalc = ae_XCrashCalc.getId();
                T_LONGLONG aidXCrashResult = ae_XCrashResult.getId();
                T_LONGLONG aidUnit = ae_Unit.getId();

                QueryStructureExt aoqVer = new QueryStructureExt();
                aoqVer.anuSeq = new SelAIDNameUnitId[1];
                aoqVer.anuSeq[0] = createAnu(aidMeasurement, "Version");

                aoqVer.condSeq = new SelItem[1];
                aoqVer.condSeq[0] = new SelItem();
                SelValueExt selValueVer = new SelValueExt();
                selValueVer.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
                selValueVer.oper = SelOpcode.CI_EQ;
                selValueVer.value = new TS_Value(new TS_Union(), (short)15);
                selValueVer.value.u.SetlongVal(testId);
                aoqVer.condSeq[0].Setvalue(selValueVer);

                aoqVer.orderBy = new SelOrder[0];
                aoqVer.groupBy = new AIDName[0];

                aoqVer.joinSeq = new JoinDef[0];

                ResultSetExt[] resVer = aea.getInstancesExt(aoqVer, 10000000);

                string[] Result_Version = resVer[0].firstElems[0].values[0].value.u.GetstringVal();

                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[4];
                aoq.anuSeq[0] = createAnu(aidXCrashResult, "ResultType");
                aoq.anuSeq[1] = createAnu(aidXCrashResult, "ValueType");
                aoq.anuSeq[2] = createAnu(aidXCrashResult, "Value");
                aoq.anuSeq[3] = createAnu(aidUnit, "Name");

                aoq.condSeq = new SelItem[3];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.NEQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetlongVal(testId);
                aoq.condSeq[0].Setvalue(selValue);

                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                aoq.condSeq[2] = new SelItem();
                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Version"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.EQ;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal(Result_Version[0]);
                aoq.condSeq[2].Setvalue(selValue2);

                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];

                aoq.joinSeq = new JoinDef[3];

                aoq.joinSeq[0].fromAID = aidMeasurement;
                aoq.joinSeq[0].toAID = aidXCrashCalc;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "X-Crash-Calculations";

                aoq.joinSeq[1].fromAID = aidXCrashCalc;
                aoq.joinSeq[1].toAID = aidXCrashResult;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "X-Crash-Results";

                aoq.joinSeq[2].fromAID = aidXCrashResult;
                aoq.joinSeq[2].toAID = aidUnit;
                aoq.joinSeq[2].joiningType = JoinType.JTOUTER;
                aoq.joinSeq[2].refName = "Unit";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                string[] Result_ResultType = res[0].firstElems[0].values[0].value.u.GetstringVal();
                string[] Result_ValueType = res[0].firstElems[0].values[1].value.u.GetstringVal();
                double[] Result_Value = res[0].firstElems[0].values[2].value.u.GetdoubleVal();
                string[] Result_Unit = res[0].firstElems[1].values[0].value.u.GetstringVal();

                for (int i = 0; i < calculations.Length; i++)
                {
                    int k = calculations[i].LastIndexOf('_');
                    string CalcName = calculations[i].Substring(0, k);
                    string ValueType = calculations[i].Substring(k + 1, calculations[i].Length - k - 1);

                    for (int j = 0; j < Result_ResultType.Length; j++)
                    {
                        if (CalcName.ToUpper() == Result_ResultType[j].ToUpper() && ValueType.ToUpper() == Result_ValueType[j].ToUpper())
                        {
                            var classObj = new CalculationValueObject
                            {
                                Name = calculations[i],
                                Value = Result_Value[j],
                                Unit = Result_Unit[j],
                            };

                            calculationsName.Add(classObj);
                            break;
                        }
                    }
                }

            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }
            return calculationsName;
        }

        public IEnumerable<CalculationValueObject> GetCalculationValue_Old( int testRequestId, [FromUri] string[] calculations)
        {

            

            List<CalculationValueObject> calculationsName = new List<CalculationValueObject>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();

                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_XCrashCalc = asObj.getElementByName("X-Crash-Calculation");
                ApplicationElement ae_XCrashResult = asObj.getElementByName("X-Crash-Result");
                ApplicationElement ae_Unit = asObj.getElementByName("Physical_Unit");

                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidXCrashCalc = ae_XCrashCalc.getId();
                T_LONGLONG aidXCrashResult = ae_XCrashResult.getId();
                T_LONGLONG aidUnit = ae_Unit.getId();

                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[4];
                aoq.anuSeq[0] = createAnu(aidXCrashResult, "ResultType");
                aoq.anuSeq[1] = createAnu(aidXCrashResult, "ValueType");
                aoq.anuSeq[2] = createAnu(aidXCrashResult, "Value");
                aoq.anuSeq[3] = createAnu(aidUnit, "Name");

                aoq.condSeq = new SelItem[1];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Id"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.CI_EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetlongVal(testRequestId);
                aoq.condSeq[0].Setvalue(selValue);

               
                
                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];
                
                aoq.joinSeq = new JoinDef[3];


                aoq.joinSeq[0].fromAID = aidMeasurement;
                aoq.joinSeq[0].toAID = aidXCrashCalc;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "X-Crash-Calculations";

                aoq.joinSeq[1].fromAID = aidXCrashCalc;
                aoq.joinSeq[1].toAID = aidXCrashResult;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "X-Crash-Results";

                aoq.joinSeq[2].fromAID = aidXCrashResult;
                aoq.joinSeq[2].toAID = aidUnit;
                aoq.joinSeq[2].joiningType = JoinType.JTOUTER;
                aoq.joinSeq[2].refName = "Unit";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                string[] Result_ResultType = res[0].firstElems[0].values[0].value.u.GetstringVal();
                string[] Result_ValueType = res[0].firstElems[0].values[1].value.u.GetstringVal();
                double[] Result_Value = res[0].firstElems[0].values[2].value.u.GetdoubleVal();
                string[] Result_Unit = res[0].firstElems[1].values[0].value.u.GetstringVal();

                for (int i = 0; i < calculations.Length; i++)
                {
                    int k = calculations[i].LastIndexOf('_');
                    string CalcName = calculations[i].Substring(0, k);
                    string ValueType = calculations[i].Substring(k + 1, calculations[i].Length - k- 1);

                    for(int j=0;j< Result_ResultType.Length; j++)
                    {
                        if(CalcName.ToUpper()== Result_ResultType[j].ToUpper() && ValueType.ToUpper() == Result_ValueType[j].ToUpper())
                        {
                            var classObj = new CalculationValueObject
                            {
                                Name = calculations[i],
                                Value = Result_Value[j],
                                Unit = Result_Unit[j],
                            };

                            calculationsName.Add(classObj);
                            break;
                        }
                    }
                }

            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }


            return calculationsName;

        }


        // GET api/<controller>
        //[Route("api/ASAM/GetAllChannels/{testType}/{testMode}")]
        [HttpGet]
        public IEnumerable<string> GetAllChannels(string testType, [FromUri] string[] testMode)
        {
            
            List<string> channelName = new List<string>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_typeOfTest = asObj.getElementByName("Type_Of_Test");
                ApplicationElement ae_subTypeOfTest = asObj.getElementByName("Subtype_Of_Test");
                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_channel = asObj.getElementByName("Channel");

                T_LONGLONG aidtypeOfTest = ae_typeOfTest.getId();
                T_LONGLONG aidsubTypeOfTest = ae_subTypeOfTest.getId();
                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidChannel = ae_channel.getId();

                String[] aaNames = new String[] { "Id" };
                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[1];

                aoq.anuSeq[0] = createAnuDistinct(aidChannel, "Description");
                
                aoq.condSeq = new SelItem[3];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidtypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetstringVal(testType);
                aoq.condSeq[0].Setvalue(selValue);

                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                //SelValueExt selValue1 = new SelValueExt();
                //selValue1.attr = new AIDNameUnitId(new AIDName(aidsubTypeOfTest, "Name"), new T_LONGLONG(0, 0));
                //selValue1.oper = SelOpcode.CI_INSET;
                //selValue1.value = new TS_Value(new TS_Union(), (short)15);
                //selValue1.value.u.SetstringSeq(testMode);
                //aoq.condSeq[2].Setvalue(selValue1);

                //aoq.condSeq[3] = new SelItem();
                //aoq.condSeq[3].Setoperator(SelOperator.AND);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Name"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.LIKE;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal("*");
                aoq.condSeq[2].Setvalue(selValue2);


                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];
                //    aoq.joinSeq = new JoinDef[0];

                aoq.joinSeq = new JoinDef[3];
                aoq.joinSeq[0].fromAID = aidsubTypeOfTest;
                aoq.joinSeq[0].toAID = aidMeasurement;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "Safety_Tests";

                aoq.joinSeq[1].fromAID = aidtypeOfTest;
                aoq.joinSeq[1].toAID = aidsubTypeOfTest;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "Subtype_Of_Tests";

                aoq.joinSeq[2].fromAID = aidMeasurement;
                aoq.joinSeq[2].toAID = aidChannel;
                aoq.joinSeq[2].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[2].refName = "Channels";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 100000);

                string[] ChannelName = res[0].firstElems[0].values[0].value.u.GetstringVal();


                channelName = ChannelName.OrderBy(x => x).ToList();
            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }



            return channelName;
        }

        #region output
        // Sample output
        //{
        //"TestName": "T6NA FRONT L ODB 64 KMH 20160126",
        //"SubMatrix": "T6NA FR ODB 64 KMH-06 20160126 OLCB_DIAdemHeader_BINARY.dat",
        //"TestId": 17410
        //},
        //{
        //    "TestName": "TBHA-7012- FRONT L ODB 64 KMH -20151006",
        //    "SubMatrix": "TBHA FR ODB 64 KMH-03 20151006_DIAdemHeader_BINARY.dat",
        //    "TestId": 17323
        //}
        #endregion
        [HttpGet]
        public IEnumerable<AllTestObject> GetAllTest(string testType, [FromUri]string[] testMode)
        {

            List<AllTestObject> testDetails = new List<AllTestObject>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_typeOfTest = asObj.getElementByName("Type_Of_Test");
                ApplicationElement ae_subTypeOfTest = asObj.getElementByName("Subtype_Of_Test");
                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_submatrix = asObj.getElementByName("Submatrix");

                T_LONGLONG aidtypeOfTest = ae_typeOfTest.getId();
                T_LONGLONG aidsubTypeOfTest = ae_subTypeOfTest.getId();
                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidSubmatrix = ae_submatrix.getId();

                QueryStructureExt aoqVer = new QueryStructureExt();
                aoqVer.anuSeq = new SelAIDNameUnitId[1];
                aoqVer.anuSeq[0] = createAnu(aidMeasurement, "Version");

                aoqVer.condSeq = new SelItem[5];
                aoqVer.condSeq[0] = new SelItem();
                SelValueExt selValueVer = new SelValueExt();
                selValueVer.attr = new AIDNameUnitId(new AIDName(aidtypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValueVer.oper = SelOpcode.EQ;
                selValueVer.value = new TS_Value(new TS_Union(), (short)15);
                selValueVer.value.u.SetstringVal(testType);
                aoqVer.condSeq[0].Setvalue(selValueVer);

                aoqVer.condSeq[1] = new SelItem();
                aoqVer.condSeq[1].Setoperator(SelOperator.AND);

                SelValueExt selValue1Ver = new SelValueExt();
                selValue1Ver.attr = new AIDNameUnitId(new AIDName(aidsubTypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValue1Ver.oper = SelOpcode.CI_INSET;
                selValue1Ver.value = new TS_Value(new TS_Union(), (short)15);
                selValue1Ver.value.u.SetstringSeq(testMode);
                aoqVer.condSeq[2].Setvalue(selValue1Ver);

                aoqVer.condSeq[3] = new SelItem();
                aoqVer.condSeq[3].Setoperator(SelOperator.AND);

                SelValueExt selValue2Ver = new SelValueExt();
                selValue2Ver.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Name"), new T_LONGLONG(0, 0));
                selValue2Ver.oper = SelOpcode.LIKE;
                selValue2Ver.value = new TS_Value(new TS_Union(), (short)15);
                selValue2Ver.value.u.SetstringVal("*_filtered");
                aoqVer.condSeq[4].Setvalue(selValue2Ver);


                aoqVer.orderBy = new SelOrder[0];
                aoqVer.groupBy = new AIDName[0];
                // aoqVer.joinSeq = new JoinDef[0];

                aoqVer.joinSeq = new JoinDef[2];
                aoqVer.joinSeq[0].fromAID = aidsubTypeOfTest;
                aoqVer.joinSeq[0].toAID = aidMeasurement;
                aoqVer.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoqVer.joinSeq[0].refName = "Safety_Tests";

                aoqVer.joinSeq[1].fromAID = aidtypeOfTest;
                aoqVer.joinSeq[1].toAID = aidsubTypeOfTest;
                aoqVer.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoqVer.joinSeq[1].refName = "Subtype_Of_Tests";




                ResultSetExt[] resVer = aea.getInstancesExt(aoqVer, 100000);
                string[] TestVersion = resVer[0].firstElems[0].values[0].value.u.GetstringVal();


                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[3];

                aoq.anuSeq[0] = createAnu(aidMeasurement, "Name");
                aoq.anuSeq[1] = createAnu(aidMeasurement, "Id");
                aoq.anuSeq[2] = createAnu(aidSubmatrix, "Name");

                aoq.condSeq = new SelItem[7];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidtypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetstringVal(testType);
                aoq.condSeq[0].Setvalue(selValue);

                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                SelValueExt selValue1 = new SelValueExt();
                selValue1.attr = new AIDNameUnitId(new AIDName(aidsubTypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValue1.oper = SelOpcode.CI_INSET;
                selValue1.value = new TS_Value(new TS_Union(), (short)15);
                selValue1.value.u.SetstringSeq(testMode);
                aoq.condSeq[2].Setvalue(selValue1);

                aoq.condSeq[3] = new SelItem();
                aoq.condSeq[3].Setoperator(SelOperator.AND);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Name"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.CI_NOTLIKE;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal("*_filtered");
                aoq.condSeq[4].Setvalue(selValue2);

                aoq.condSeq[5] = new SelItem();
                aoq.condSeq[5].Setoperator(SelOperator.AND);

                SelValueExt selValue3 = new SelValueExt();
                selValue3.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Version"), new T_LONGLONG(0, 0));
                selValue3.oper = SelOpcode.CI_INSET;
                selValue3.value = new TS_Value(new TS_Union(), (short)15);
                selValue3.value.u.SetstringSeq(TestVersion);
                aoq.condSeq[6].Setvalue(selValue3);



                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[0];
                //    aoq.joinSeq = new JoinDef[0];

                aoq.joinSeq = new JoinDef[3];
                aoq.joinSeq[0].fromAID = aidsubTypeOfTest;
                aoq.joinSeq[0].toAID = aidMeasurement;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "Safety_Tests";

                aoq.joinSeq[1].fromAID = aidtypeOfTest;
                aoq.joinSeq[1].toAID = aidsubTypeOfTest;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "Subtype_Of_Tests";

                aoq.joinSeq[2].fromAID = aidMeasurement;
                aoq.joinSeq[2].toAID = aidSubmatrix;
                aoq.joinSeq[2].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[2].refName = "Submatrices";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 100000);

                string[] TestName = res[0].firstElems[0].values[0].value.u.GetstringVal();
                T_LONGLONG[] TestId = res[0].firstElems[0].values[1].value.u.GetlonglongVal();
                string[] SubMatrixName = res[0].firstElems[1].values[0].value.u.GetstringVal();
                HashSet<string> uniqueTestName = new HashSet<string>();
                for (int i = 0; i < TestName.Length; i++)
                {
                    var tempTestObject = new AllTestObject
                    {
                        TestName = TestName[i],
                        TestId = TestId[i].low,
                        SubMatrix = SubMatrixName[i]
                    };

                    testDetails.Add(tempTestObject);
                }



            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }
            return testDetails;
        }

        public string ReplaceLastOccurrence(string Source, string Find, string Replace)
        {
            int place = Source.LastIndexOf(Find);

            if (place == -1)
                return Source;

            string result = Source.Remove(place, Find.Length).Insert(place, Replace);
            return result;
        }


        [Route("api/ASAM/GetCalculation/{testType}/{testMode}")]
        [HttpGet]
        public IEnumerable<string> GetCalculation(string testType, string testMode)
        {

            var result = GetCalculationChannel(testType, testMode);
            return result;
        }

        private List<string> GetCalculationChannel(string testType, string testMode)
        {
            List<string> calcName = new List<string>();
            try
            {
                if (aoSession == null)
                    aoSession = Con_NameService();
                ApplicationStructure asObj = aoSession.getApplicationStructure();
                ApplElemAccess aea = aoSession.getApplElemAccess();
                ApplicationElement ae_typeOfTest = asObj.getElementByName("Type_Of_Test");
                ApplicationElement ae_subTypeOfTest = asObj.getElementByName("Subtype_Of_Test");
                ApplicationElement ae_Measurement = asObj.getElementsByBaseType("AoMeasurement")[0];
                ApplicationElement ae_XCrashCalc = asObj.getElementByName("X-Crash-Calculation");
                ApplicationElement ae_XCrashResult = asObj.getElementByName("X-Crash-Result");
                T_LONGLONG aidtypeOfTest = ae_typeOfTest.getId();
                T_LONGLONG aidsubTypeOfTest = ae_subTypeOfTest.getId();
                T_LONGLONG aidMeasurement = ae_Measurement.getId();
                T_LONGLONG aidXCrashCalc = ae_XCrashCalc.getId();
                T_LONGLONG aidXCrashResult = ae_XCrashResult.getId();
                String[] aaNames = new String[] { "Id" };
                QueryStructureExt aoq = new QueryStructureExt();
                aoq.anuSeq = new SelAIDNameUnitId[2];
                // for (int i = 0; i < 2; i++)
                // {
                aoq.anuSeq[0] = createAnu(aidXCrashResult, "ResultType");
                aoq.anuSeq[1] = createAnu(aidXCrashResult, "ValueType");
                // }
                aoq.condSeq = new SelItem[7];
                aoq.condSeq[0] = new SelItem();
                SelValueExt selValue = new SelValueExt();
                selValue.attr = new AIDNameUnitId(new AIDName(aidtypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValue.oper = SelOpcode.EQ;
                selValue.value = new TS_Value(new TS_Union(), (short)15);
                selValue.value.u.SetstringVal(testType);
                aoq.condSeq[0].Setvalue(selValue);

                aoq.condSeq[1] = new SelItem();
                aoq.condSeq[1].Setoperator(SelOperator.AND);

                SelValueExt selValue1 = new SelValueExt();
                selValue1.attr = new AIDNameUnitId(new AIDName(aidsubTypeOfTest, "Name"), new T_LONGLONG(0, 0));
                selValue1.oper = SelOpcode.EQ;
                selValue1.value = new TS_Value(new TS_Union(), (short)15);
                selValue1.value.u.SetstringVal(testMode);
                aoq.condSeq[2].Setvalue(selValue1);

                aoq.condSeq[3] = new SelItem();
                aoq.condSeq[3].Setoperator(SelOperator.AND);

                SelValueExt selValue2 = new SelValueExt();
                selValue2.attr = new AIDNameUnitId(new AIDName(aidMeasurement, "Name"), new T_LONGLONG(0, 0));
                selValue2.oper = SelOpcode.LIKE;
                selValue2.value = new TS_Value(new TS_Union(), (short)15);
                selValue2.value.u.SetstringVal("*_filtered");
                aoq.condSeq[4].Setvalue(selValue2);

                aoq.condSeq[5] = new SelItem();
                aoq.condSeq[5].Setoperator(SelOperator.AND);

                SelValueExt selValue3 = new SelValueExt();
                selValue3.attr = new AIDNameUnitId(new AIDName(aidXCrashCalc, "Name"), new T_LONGLONG(0, 0));
                selValue3.oper = SelOpcode.LIKE;
                selValue3.value = new TS_Value(new TS_Union(), (short)15);
                selValue3.value.u.SetstringVal("*");
                aoq.condSeq[6].Setvalue(selValue3);

                aoq.orderBy = new SelOrder[0];
                aoq.groupBy = new AIDName[2];
                aoq.groupBy[0] = new AIDName(aidXCrashResult, "ResultType");
                aoq.groupBy[1] = new AIDName(aidXCrashResult, "ValueType");
                //    aoq.joinSeq = new JoinDef[0];

                aoq.joinSeq = new JoinDef[4];
                aoq.joinSeq[0].fromAID = aidsubTypeOfTest;
                aoq.joinSeq[0].toAID = aidMeasurement;
                aoq.joinSeq[0].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[0].refName = "Safety_Tests";

                aoq.joinSeq[1].fromAID = aidtypeOfTest;
                aoq.joinSeq[1].toAID = aidsubTypeOfTest;
                aoq.joinSeq[1].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[1].refName = "Subtype_Of_Tests";

                aoq.joinSeq[2].fromAID = aidMeasurement;
                aoq.joinSeq[2].toAID = aidXCrashCalc;
                aoq.joinSeq[2].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[2].refName = "X-Crash-Calculations";

                aoq.joinSeq[3].fromAID = aidXCrashCalc;
                aoq.joinSeq[3].toAID = aidXCrashResult;
                aoq.joinSeq[3].joiningType = JoinType.JTDEFAULT;
                aoq.joinSeq[3].refName = "X-Crash-Results";

                ResultSetExt[] res = aea.getInstancesExt(aoq, 10000000);

                string[] ResultType = res[0].firstElems[0].values[0].value.u.GetstringVal();
                string[] ValueType = res[0].firstElems[0].values[1].value.u.GetstringVal();
                HashSet<string> uniqueCalc = new HashSet<string>();
                for (int i = 0; i < ResultType.Length; i++)
                {
                    if (!uniqueCalc.Contains(ResultType[i] + "_" + ValueType[i]))
                        uniqueCalc.Add(ResultType[i] + "_" + ValueType[i]);
                }
                calcName = uniqueCalc.OrderBy(x => x).ToList();
            }
            catch (AoException ex)
            {
            }
            finally
            {
                if (aoSession != null)
                {
                    aoSession.close();
                    aoSession = null;
                }
            }




            return calcName;
        }
        public static SelAIDNameUnitId createAnu(T_LONGLONG aid, String aaName)
        {
            SelAIDNameUnitId anu = new SelAIDNameUnitId();
            anu.aggregate = AggrFunc.NONE;
            anu.attr = new AIDName(aid, aaName);
            anu.unitId = new T_LONGLONG(0, 0);
            return (anu);
        }
        public static SelAIDNameUnitId createAnuDistinct(T_LONGLONG aid, String aaName)
        {
            SelAIDNameUnitId anu = new SelAIDNameUnitId();
            anu.aggregate = AggrFunc.DISTINCT;
            anu.attr = new AIDName(aid, aaName);
            anu.unitId = new T_LONGLONG(0, 0);
            return (anu);
        }

        public double[] getColumnData(ValueMatrix vm, Column colName)
        {
            double[] tempDouble = null;
            try
            {

                TS_ValueSeq vals = vm.getValueVector(colName, 0, 0);
                DataType dt = vals.u.Discriminator;
                switch (dt)
                {

                    case DataType.DT_DOUBLE:
                        {

                            tempDouble = vals.u.GetdoubleVal();
                            break;
                        }
                }
                //   cols[0].destroy();
            }
            catch (Exception ex)
            {

            }
            return tempDouble;

        }
        public AoSession Con_NameService()
        {
            AoSession session = null;
            try
            {

                foreach (IChannel objChannel in ChannelServices.RegisteredChannels)
                    ChannelServices.UnregisterChannel(objChannel);
                IiopClientChannel channel1 = new IiopClientChannel();
                ChannelServices.RegisterChannel(channel1, false);
                CorbaInit init = CorbaInit.GetInit();
                NamingContext nameService = init.GetNameService(ConfigurationManager.AppSettings["ASAMServer"], Convert.ToInt32(ConfigurationManager.AppSettings["ASAMPort"]));
                NameComponent[] name = new NameComponent[] { new NameComponent(ConfigurationManager.AppSettings["ASAMDB"], "ASAM-ODS") };
                Object obj = nameService.resolve(name);
                AoFactory aoFactory = (AoFactory)obj;
                session = aoFactory.newSession(System.Configuration.ConfigurationManager.AppSettings["ASAMconnection"]);

            }
            catch (Exception aoe) //AoException aoe
            {

            }

            return session;

        }

        [Route("api/ASAM/GetCalculationName/{testType}/{testMode}")]
        [HttpGet]
        public IEnumerable<string> GetCalculationName(string testType, string testMode)
        {
            string[] lstCalculationName = { "3ms Cont Res", "3ms Cum Res", "HIC", "HIC15", "HIC36", "Max Res", "Max X", "Max X MO", "Max Y", "max Y MO", "Max Z", "Min Res", "Min X", "Min X MO", "Min Y", "Min Y MO", "Min Z", "Nce", "Ncf", "Nij", "Nte", "Ntf", "USNCAP P Chest", "USNCAP P Femur", "USNCAP P Femur left", "USNCAP P Femur right", "USNCAP P Head", "USNCAP P Neck", "USNCAP P Neck Compression", "USNCAP P Neck Nij", "USNCAP P Neck Tension" };
            return lstCalculationName.ToList();
        }

        [Route("api/ASAM/GetCalculationChannelYAxis/{channel}")]
        [HttpGet]
        public int[] GetCalculationChannelYAxis(string channel)
        {
            string[] lstCalculationName = { "min Z Time", "min Res Value", "max Res Time", "HIC Value", "HIC T2", "3ms Cum Res Value", "Nce Value", "Nce Time", "Nij Value", "USNCAP P Femur left", "max Y Value", "max Z Time", "Ncf Time", "max X Value", "min Res Time", "max Res Value", "max X MO total Time", "min X Time", "min Y Value", "HIC36 T2", "USNCAP P Neck Tension", "USNCAP P Neck Nij", "USNCAP P Chest", "min Y Time", "HIC15 T2", "3ms Cont Res Value", "3ms Cum Res T1", "USNCAP P Femur", "max Y Time", "HIC T1", "3ms Cum Res T2", "USNCAP P Head", "Ncf Value", "Ntf Time", "USNCAP P Femur right", "min X Value", "max Z Value", "HIC36 Value", "USNCAP P Neck Compression", "min X MO total Value", "max X MO total Value", "min Y MO total Value", "min Y MO total Time", "Ntf Value", "Nij Time", "max X Time", "min Z Value", "HIC36 T1", "HIC15 Value", "HIC15 T1", "3ms Cont Res T1", "3ms Cont Res T2", "min X MO total Time", "max Y MO total Value", "max Y MO total Time", "Nte Value", "Nte Time", "USNCAP P Neck" };
            int[] yAxis = new int[10];
            int i = 0;
            int index = Array.IndexOf(lstCalculationName, channel) + 1;
            for (i = 0; i < 10; i++)
            {
                yAxis[i] = index + 1;
            }
            return yAxis;
        }

        [Route("api/ASAM/GetCalculationChannelNameYAxis/{channelName}")]
        [HttpGet]
        public int[] GetCalculationChannelNameYAxis(string channelName)
        {
            string[] lstCalculationName = { "3ms Cont Res", "3ms Cum Res", "HIC", "HIC15", "HIC36", "Max Res", "Max X", "Max X MO", "Max Y", "max Y MO", "Max Z", "Min Res", "Min X", "Min X MO", "Min Y", "Min Y MO", "Min Z", "Nce", "Ncf", "Nij", "Nte", "Ntf", "USNCAP P Chest", "USNCAP P Femur", "USNCAP P Femur left", "USNCAP P Femur right", "USNCAP P Head", "USNCAP P Neck", "USNCAP P Neck Compression", "USNCAP P Neck Nij", "USNCAP P Neck Tension" };
            int[] yAxis = new int[10];
            int i = 0;
            int index = Array.IndexOf(lstCalculationName, channelName) + 1;
            for (i = 0; i < 10; i++)
            {
                yAxis[i] = index + 1;
            }
            return yAxis;
        }

        public double[] getDataUnit(ValueMatrix vm, String colName, string unit = null)
        {
            double[] tempDouble = null;
            Column[] cols = vm.getColumns(colName);
            if (cols.Length > 0)
            {
                if (unit != null)
                {
                    cols[0].setUnit(unit);
                }
                TS_ValueSeq vals = vm.getValueVector(cols[0], 0, 0);
                DataType dt = vals.u.Discriminator;
                switch (dt)
                {

                    case DataType.DT_DOUBLE:
                        {
                            tempDouble = vals.u.GetdoubleVal();
                            var i = 0;
                            foreach (var val in tempDouble)
                            {
                                tempDouble[i] = Math.Round(val, 5);
                                i++;
                            }
                            break;
                        }
                }
                cols[0].destroy();
            }
            return tempDouble;
        }
    }
}
