﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.RepositoryImpl
{
    public class ChartLineStyleRepository : IChartLineStyleRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();

        public List<ChartLineStyleViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartLineStyleViewModel>(ChartLineStyleQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public ChartLineStyleViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartLineStyleViewModel>(ChartLineStyleQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Insert(ChartLineStyleViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartLineStyleQueries.StoreProcInsert,
                     new
                     {
                         Name = viewModel.Name,
                         Style=viewModel.Style,
                         StyleValue = viewModel.StyleValue,
                         UserId = viewModel.CreatedBy
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(ChartLineStyleViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartLineStyleQueries.StoreProcUpdate,
                    new
                    {
                        Id = viewModel.Id,
                        Name = viewModel.Name,
                        Style = viewModel.Style,
                        StyleValue = viewModel.StyleValue,
                        UserId = viewModel.CreatedBy
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartLineStyleQueries.StoreProcDelete,
                    new
                    {
                        Id = Id
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
