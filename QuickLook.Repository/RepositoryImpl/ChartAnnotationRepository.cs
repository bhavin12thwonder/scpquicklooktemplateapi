﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.RepositoryImpl
{
    public class ChartAnnotationRepository : IChartAnnotationRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();

        public List<ChartAnnotationViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartAnnotationViewModel>(ChartAnnotationQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public ChartAnnotationViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartAnnotationViewModel>(ChartAnnotationQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public List<ChartAnnotationViewModel> GetByChartConfigId(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartAnnotationViewModel>(ChartAnnotationQueries.StoreProcGetByChartConfigId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public SpTransactionMessage Insert(List<ChartAnnotationViewModel> viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartAnnotationQueries.StoreProcInsert,
                     new
                     {
                         ChartAnnotationTable = viewModel.ToDataTable<ChartAnnotationViewModel>().AsTableValuedParameter("dbo.ChartAnnotationTable"),
                         UserId = userId
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(ChartAnnotationViewModel viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartAnnotationQueries.StoreProcUpdate,
                    new
                    {
                        //ChartAnnotationTable = viewModel.ToDataTable<ChartAnnotationViewModel>().AsTableValuedParameter("dbo.ChartAnnotationTable"),
                        Id = viewModel.Id,
                        TemplateTabChartConfigurationId = viewModel.TemplateTabChartConfigurationId,
                        AnnotationLineStartXCoOrdinate = viewModel.AnnotationLineStartXCoOrdinate,
                        AnnotationLineEndXCoOrdinate = viewModel.AnnotationLineEndXCoOrdinate,
                        AnnotationLineStartYCoOrdinate = viewModel.AnnotationLineStartYCoOrdinate,
                        AnnotationLineEndYCoOrdinate = viewModel.AnnotationLineEndYCoOrdinate,
                        AnnotationLineThicknessId = viewModel.AnnotationLineThicknessId,
                        AnnotationLineColour = viewModel.AnnotationLineColour,
                        AnnotationLineStyleId = viewModel.AnnotationLineStyleId,
                        AnnotationText = viewModel.AnnotationText,
                        AnnotationTextStartXCoOrdinate = viewModel.AnnotationTextStartXCoOrdinate,
                        AnnotationTextStartYCoOrdinate = viewModel.AnnotationTextStartYCoOrdinate,
                        UserId = userId
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartAnnotationQueries.StoreProcDelete,
                    new
                    {
                        Id = Id
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
