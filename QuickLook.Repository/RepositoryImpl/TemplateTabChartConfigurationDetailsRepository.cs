﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.RepositoryImpl
{
    public class TemplateTabChartConfigurationDetailsRepository : ITemplateTabChartConfigurationDetailsRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();

        public List<TemplateTabChartConfigurationDetailsViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabChartConfigurationDetailsViewModel>(TemplateTabChartConfigurationDetailsQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public TemplateTabChartConfigurationDetailsViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabChartConfigurationDetailsViewModel>(TemplateTabChartConfigurationDetailsQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public TemplateTabChartConfigurationDetailsViewModel GetByTemplateTabChartConfigId(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabChartConfigurationDetailsViewModel>(TemplateTabChartConfigurationDetailsQueries.StoreProcGetByTemplateTabChartConfigId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public TemplateTabChartConfigurationPreviewViewModel PreviewByTemplateTabChartConfigId(int Id, int testId, string submatrixName)
        {
            ChartAxisConfiguration chartAxisConfiguration = new ChartAxisConfiguration();
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var templateTabChartConfigurationDbData = sqlcon.QueryAsync<TemplateTabChartConfigurationDetailsPreviewViewModel>(TemplateTabChartConfigurationDetailsQueries.StoreProcPreviewByTemplateTabChartConfigId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
                TemplateTabChartConfigurationPreviewViewModel templateTabChartConfigurationData = new TemplateTabChartConfigurationPreviewViewModel();

                if (templateTabChartConfigurationDbData.Count > 0)
                {
                    templateTabChartConfigurationData = templateTabChartConfigurationDbData.Select(x => new TemplateTabChartConfigurationPreviewViewModel
                    {
                        ChartTypeId = x.ChartTypeId,
                        ChartTypeValue = x.ChartTypeValue,
                        TemplateTabChartConfigurationId = x.TemplateTabChartConfigurationId,
                        TemplateTabChartConfigurationName = x.TemplateTabChartConfigurationName,
                        XAxis = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
                        YAxis = new int[6] { 10, 20, 30, 40, 50, 60 },
                        XAxisChannel = x.XAxisChannel,
                        XAxisUnits = x.XAxisUnits,
                        XAxisMinValue = x.XAxisMinValue,
                        XAxisMaxValue = x.XAxisMaxValue,
                        XAxisInterval = x.XAxisInterval,
                        XAxisLabel = x.XAxisLabel,
                        YAxisUnits = x.YAxisUnits,
                        YAxisMinValue = x.YAxisMinValue,
                        YAxisMaxValue = x.YAxisMaxValue,
                        YAxisInterval = x.YAxisInterval,
                        YAxisLabel = x.YAxisLabel
                    }).Distinct().FirstOrDefault();

                    var chartChannelConfiguration = templateTabChartConfigurationDbData.Where(b => b.TemplateTabChartConfigurationId == templateTabChartConfigurationData.TemplateTabChartConfigurationId)
                        .GroupBy(b => new
                        {
                            b.ChartChannelId,
                            b.ChartOrientation,
                            b.ChartChannelName,
                            b.ChartLineColour,
                            b.ChartLineStyleId,
                            b.LineStyleName,
                            b.LineStyleValue,
                            b.ChartLineThicknessId,
                            b.LineThickness,
                            b.LegendName,
                            b.GoodValue,
                            b.AcceptableValue,
                            b.MarginalValue,
                            b.PoorValue,
                            b.ATDPosition,
                            b.ATDPositionValue,
                            b.FillArea,
                            b.Polarity,
                            b.IsLegendVisible
                        }).ToList();

                    var chartChannelConfigurationData = chartChannelConfiguration.Select(c => new TemplateTabChartChannelConfigurationPreviewViewModel
                    {
                        ChartChannelId = c.Key.ChartChannelId,
                        ChartChannelName = c.Key.ChartChannelName,
                        ChartOrientation = c.Key.ChartOrientation,
                        ChartLineColour = c.Key.ChartLineColour,
                        ChartLineStyleId = c.Key.ChartLineStyleId,
                        LineStyleName = c.Key.LineStyleName,
                        LineStyleValue = c.Key.LineStyleValue,
                        ChartLineThicknessId = c.Key.ChartLineThicknessId,
                        LineThickness = c.Key.LineThickness,
                        LegendName = c.Key.LegendName,
                        GoodValue = c.Key.GoodValue,
                        AcceptableValue = c.Key.AcceptableValue,
                        MarginalValue = c.Key.MarginalValue,
                        PoorValue = c.Key.PoorValue,
                        ATDPosition = c.Key.ATDPosition,
                        ATDPositionValue = c.Key.ATDPositionValue,
                        FillArea = c.Key.FillArea,
                        Polarity = c.Key.Polarity,
                        IsLegendVisible = c.Key.IsLegendVisible,
                        //ATDPositionAxisValue=5,
                        ATDPositionAxisValue = chartAxisConfiguration.GetATDPositionAxisValue(testId, new ATDRequestModel { CalculationName = c.Key.ChartChannelName, ATDPosition = c.Key.ATDPositionValue }),
                        ChartChannelATDPositions = chartAxisConfiguration.GetATDPositionsAxisValue(testId, GetATDPositionsById(c.Key.ChartChannelId)),
                        BarChartXAxisValue = (templateTabChartConfigurationDbData.Where(b => b.TemplateTabChartConfigurationId == templateTabChartConfigurationData.TemplateTabChartConfigurationId && b.ChartChannelId == c.Key.ChartChannelId).Select(d => d.ChartTypeId).FirstOrDefault() == 2) ? chartAxisConfiguration.GetBarChartXAxisValue(testId, c.Key.ChartChannelName) : 0,
                        ChannelXAxis = chartAxisConfiguration.GetChannelDataXAxis(testId, templateTabChartConfigurationDbData.Select(e => e.XAxisChannel).FirstOrDefault()).Unit ==
                                       templateTabChartConfigurationDbData.Select(e => e.XAxisUnits).FirstOrDefault() ? chartAxisConfiguration.GetChannelXAxis(testId, templateTabChartConfigurationDbData.Select(e => e.XAxisChannel).FirstOrDefault()) :
                                       chartAxisConfiguration.GetChannelDataConverted(testId, submatrixName, templateTabChartConfigurationDbData.Select(e => e.XAxisUnits).FirstOrDefault(), templateTabChartConfigurationDbData.Select(e => e.XAxisChannel).FirstOrDefault()),

                        ChannelYAxis = chartAxisConfiguration.GetChannelDataYAxis(testId, c.Key.ChartChannelName, c.Key.Polarity.HasValue ? c.Key.Polarity.Value : false).Unit ==
                                       templateTabChartConfigurationDbData.Select(e => e.YAxisUnits).FirstOrDefault() ? chartAxisConfiguration.GetChannelYAxis(testId, c.Key.ChartChannelName, c.Key.Polarity.HasValue ? c.Key.Polarity.Value : false) :
                                       chartAxisConfiguration.GetChannelDataConverted(testId, submatrixName, templateTabChartConfigurationDbData.Select(e => e.YAxisUnits).FirstOrDefault(), c.Key.ChartChannelName),
                    }).ToList();

                    var chartAnnotationConfiguration = templateTabChartConfigurationDbData.Where(c => c.TemplateTabChartConfigurationId == templateTabChartConfigurationData.TemplateTabChartConfigurationId)
                        .GroupBy(c => new
                        {
                            c.ChartAnnotationId,
                            c.AnnotationLineStartXCoOrdinate,
                            c.AnnotationLineEndXCoOrdinate,
                            c.AnnotationLineStartYCoOrdinate,
                            c.AnnotationLineEndYCoOrdinate,
                            c.AnnotationLineThicknessId,
                            c.AnnotationLineColour,
                            c.AnnotationLineStyleId,
                            c.AnnotationText,
                            c.AnnotationTextStartXCoOrdinate,
                            c.AnnotationTextStartYCoOrdinate
                        }).ToList();

                    var chartAnnotationConfigurationData = chartAnnotationConfiguration.Select(c => new TemplateTabChartAnnotationConfigurationPreviewViewModel
                    {
                        ChartAnnotationId = c.Key.ChartAnnotationId,
                        AnnotationLineStartXCoOrdinate = c.Key.AnnotationLineStartXCoOrdinate,
                        AnnotationLineEndXCoOrdinate = c.Key.AnnotationLineEndXCoOrdinate,
                        AnnotationLineStartYCoOrdinate = c.Key.AnnotationLineStartYCoOrdinate,
                        AnnotationLineEndYCoOrdinate = c.Key.AnnotationLineEndYCoOrdinate,
                        AnnotationLineThicknessId = c.Key.AnnotationLineThicknessId,
                        AnnotationLineColour = c.Key.AnnotationLineColour,
                        AnnotationLineStyleId = c.Key.AnnotationLineStyleId,
                        AnnotationText = c.Key.AnnotationText,
                        AnnotationTextStartXCoOrdinate = c.Key.AnnotationTextStartXCoOrdinate,
                        AnnotationTextStartYCoOrdinate = c.Key.AnnotationTextStartYCoOrdinate
                    }).Distinct().ToList();

                    templateTabChartConfigurationData.ChartChannelConfiguration = chartChannelConfigurationData;
                    templateTabChartConfigurationData.ChartAnnotationConfiguration = chartAnnotationConfigurationData;
                }

                return templateTabChartConfigurationData;
            }
        }

        public SpTransactionMessage Insert(TemplateTabChartConfigurationDetailsUpsertViewModel viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabChartConfigurationDetailsQueries.StoreProcInsert,
                     new
                     {
                         TemplateTabChartConfigurationId = viewModel.TemplateTabChartConfigurationId,
                         TemplateTabChartConfigurationName = viewModel.TemplateTabChartConfigurationName,
                         //XAxisChannelId = viewModel.XAxisChannelId,
                         XAxisUnits = viewModel.XAxisUnits,
                         XAxisMinValue = viewModel.XAxisMinValue,
                         XAxisMaxValue = viewModel.XAxisMaxValue,
                         XAxisInterval = viewModel.XAxisInterval,
                         XAxisLabel = viewModel.XAxisLabel,
                         XAxisAutoFill = viewModel.XAxisAutoFill,
                         YAxisUnits = viewModel.YAxisUnits,
                         YAxisMinValue = viewModel.YAxisMinValue,
                         YAxisMaxValue = viewModel.YAxisMaxValue,
                         YAxisInterval = viewModel.YAxisInterval,
                         YAxisLabel = viewModel.YAxisLabel,
                         YAxisAutoFill = viewModel.YAxisAutoFill,
                         ChartAnnotationTable = viewModel.ChartAnnotationList.ToDataTable<ChartAnnotationUpsertViewModel>().AsTableValuedParameter("dbo.ChartAnnotationTable"),
                         //ChartChannelTable = viewModel.ChartChannelList.ToDataTable<ChartChannelViewModel>().AsTableValuedParameter("dbo.ChartChannelTable"),
                         UserId = userId
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(TemplateTabChartConfigurationDetailsUpsertViewModel viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabChartConfigurationDetailsQueries.StoreProcUpdate,
                    new
                    {
                        Id = viewModel.Id,
                        TemplateTabChartConfigurationId = viewModel.TemplateTabChartConfigurationId,
                        TemplateTabChartConfigurationName = viewModel.TemplateTabChartConfigurationName,
                        //XAxisChannelId = viewModel.XAxisChannelId,
                        XAxisUnits = viewModel.XAxisUnits,
                        XAxisMinValue = viewModel.XAxisMinValue,
                        XAxisMaxValue = viewModel.XAxisMaxValue,
                        XAxisInterval = viewModel.XAxisInterval,
                        XAxisLabel = viewModel.XAxisLabel,
                        XAxisAutoFill = viewModel.XAxisAutoFill,
                        YAxisUnits = viewModel.YAxisUnits,
                        YAxisMinValue = viewModel.YAxisMinValue,
                        YAxisMaxValue = viewModel.YAxisMaxValue,
                        YAxisInterval = viewModel.YAxisInterval,
                        YAxisLabel = viewModel.YAxisLabel,
                        YAxisAutoFill = viewModel.YAxisAutoFill,
                        ChartAnnotationTable = viewModel.ChartAnnotationList.ToDataTable<ChartAnnotationUpsertViewModel>().AsTableValuedParameter("dbo.ChartAnnotationTable"),
                        //ChartChannelTable = viewModel.ChartChannelList.ToDataTable<ChartChannelViewModel>().AsTableValuedParameter("dbo.ChartChannelTable"),
                        UserId = userId
                    }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabChartConfigurationDetailsQueries.StoreProcDelete,
                    new
                    {
                        Id = Id,
                        UserId = userId
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        private List<ATDRequestModel> GetATDPositionsById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ATDPositionChartChannelMapViewModel>(ChartChannelQueries.StoreProcGetATDPositionById, new { @ChartChannelId = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.Select(d => new ATDRequestModel { ATDPositionId = (d.AtdpositionId.HasValue) ? d.AtdpositionId.Value : 0, ATDPosition = d.ATDPositionValue, CalculationName = d.ChannelName }).ToList();
            }
        }
    }
}
