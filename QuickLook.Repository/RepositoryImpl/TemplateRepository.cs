﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using QuickLook.Model;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using Dapper;
using System.Linq;
using System.Data.SqlClient;
using QuickLook.Common;

namespace QuickLook.Repository.RepositoryImpl
{
    public class TemplateRepository : ITemplateRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();

        public List<TemplateViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var result= sqlcon.QueryAsync<TemplateViewModel>(TemplateQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
                var TemplateViewModelData = result.Select(x => new TemplateViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    TestTypeId = x.TestTypeId,
                    TestType=x.TestType,
                    TestModeNames= GetTestModeNames(x.Id),
                    IsDeleted = x.IsDeleted
                }).ToList();
                return TemplateViewModelData;
            }
        }
        private List<string> GetTestModeNames(int id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var testModeNames = sqlcon.QueryAsync<TemplateTestModeMapViewModel>(TemplateQueries.StoreProcGetTestModeIdsById,
                    new { @TemplateId = id }, null, 0, System.Data.CommandType.StoredProcedure).Result.Select(d => d.TestModeName).ToList();
                return testModeNames;
            }
        }

        public TemplateViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var result = sqlcon.QueryAsync<TemplateViewModel>(TemplateQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
                var TemplateViewModelData = result.Select(x => new TemplateViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    TestTypeId = x.TestTypeId,
                    TestModeList = GetTestModeListById(Id),
                    TestModeIds = GetTestModeIdsById(Id),
                    IsDeleted = x.IsDeleted,
                    TestType = x.TestType,
                    TestModeNames = GetTestModeNames(x.Id)
                }).ToList().FirstOrDefault();
                //TemplateViewModelData.TestMode = GetTestModeNames(Id).FirstOrDefault();
                return TemplateViewModelData;
            }
        }
        private List<UniqueIdsViewModel> GetTestModeIdsById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                 var TestModeIds = sqlcon.QueryAsync<TemplateTestModeMapViewModel>(TemplateQueries.StoreProcGetTestModeIdsById, new { @TemplateId = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.Select(d => new UniqueIdsViewModel { Id = ((d.TestModeId.HasValue) ? d.TestModeId.Value : 0) }).ToList<UniqueIdsViewModel>();
                return TestModeIds;
            }
        }
        private List<int?> GetTestModeListById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var testModeList = sqlcon.QueryAsync<TemplateTestModeMapViewModel>(TemplateQueries.StoreProcGetTestModeIdsById,
                    new { @TemplateId = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.Select(d => d.TestModeId).ToList();
                return testModeList;
            }
        }

        public List<TemplateTabPreviewViewModel> PreviewByTemplateId(int Id, int testId, string submatrixName)
        {
            ChartAxisConfiguration chartAxisConfiguration = new ChartAxisConfiguration();
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var templateData = sqlcon.QueryMultiple(TemplateQueries.StoreProcPreviewByTemplateId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure);
                var tabData = templateData.ReadAsync<TemplateTabPreviewViewModel>().Result;
                var tabConfigurationData = templateData.ReadAsync<TemplateTabConfigurationPreviewViewModel>().Result;
                var tabTableConfigurationData = templateData.ReadAsync<TemplateTabTableConfigurationDetailsPreviewViewModel>().Result;
                var tabChartConfigurationData = templateData.ReadAsync<TemplateTabChartConfigurationDetailsPreviewViewModel>().Result;

                //Load TableConfiguration Data
                #region TableConfiguration

                var tabTableConfigurationPreviewData = tabTableConfigurationData.Select(x => new
                {
                    x.RowId,
                    x.TemplateTabTableConfigurationId,
                    x.TemplateTabTableConfigurationName
                }).Distinct().ToList();
                var tabTableConfigurationPreviewResultData = tabTableConfigurationPreviewData.Select(x => new TemplateTabTableConfigurationDetailsPreviewRowsViewModel
                {
                    RowId = x.RowId,
                    TemplateTabTableConfigurationId = x.TemplateTabTableConfigurationId,
                    TemplateTabTableConfigurationName = x.TemplateTabTableConfigurationName,
                    Columns = tabTableConfigurationData.Where(y => y.RowId == x.RowId && y.TemplateTabTableConfigurationId == x.TemplateTabTableConfigurationId).Select(z => new TemplateTabTableConfigurationDetailsPreviewColumnsViewModel
                    {
                        Id = z.Id,
                        RowId = z.RowId,
                        ColumnId = z.ColumnId,
                        RowSpan = z.RowSpan,
                        ColumnSpan = z.ColumnSpan,
                        ColumnWidth = z.ColumnWidth,
                        ColumnType = z.ColumnType,
                        DbColumn = z.DbColumn,
                        ColumnHeader = z.ColumnHeader,
                        ColumnData = z.ColumnData,
                        ColourCode = z.ColourCode,
                        IsHeader = z.IsHeader,
                        IsDeleted = z.IsDeleted
                    }).ToList()
                }).Distinct().ToList();

                #endregion

                //Load ChartConfiguration
                #region ChartConfiguration

                var templateTabChartConfigurationDataList = tabChartConfigurationData.Select(x => new TemplateTabChartConfigurationIdsPreviewViewModel
                {
                    Id = x.Id,
                    TemplateTabChartConfigurationId = x.TemplateTabChartConfigurationId
                }).GroupBy(x => new { x.Id, x.TemplateTabChartConfigurationId }).ToList();

                var templateTabChartConfigurationPreviewData = templateTabChartConfigurationDataList.Select(y => new TemplateTabChartConfigurationPreviewViewModel
                {
                    ChartTypeId = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.ChartTypeId).FirstOrDefault(),
                    ChartTypeValue = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.ChartTypeValue).FirstOrDefault(),
                    TemplateTabChartConfigurationId = y.Key.TemplateTabChartConfigurationId,
                    TemplateTabChartConfigurationName = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.TemplateTabChartConfigurationName).FirstOrDefault(),
                    XAxis = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
                    YAxis = new int[6] { 10, 20, 30, 40, 50, 60 },
                    XAxisChannel = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisChannel).FirstOrDefault(),
                    XAxisUnits = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisUnits).FirstOrDefault(),
                    XAxisMinValue = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisMinValue).FirstOrDefault(),
                    XAxisMaxValue = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisMaxValue).FirstOrDefault(),
                    XAxisInterval = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisInterval).FirstOrDefault(),
                    XAxisLabel = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisLabel).FirstOrDefault(),
                    YAxisUnits = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisUnits).FirstOrDefault(),
                    YAxisMinValue = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisMinValue).FirstOrDefault(),
                    YAxisMaxValue = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisMaxValue).FirstOrDefault(),
                    YAxisInterval = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisInterval).FirstOrDefault(),
                    YAxisLabel = tabChartConfigurationData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisLabel).FirstOrDefault(),
                    ChartChannelConfiguration = tabChartConfigurationData.Where(b => b.TemplateTabChartConfigurationId == y.Key.TemplateTabChartConfigurationId)
                    .GroupBy(z => new
                    {
                        z.ChartChannelId,
                        z.ChartChannelName,
                        z.ChartLineColour,
                        z.ChartLineStyleId,
                        z.LineStyleName,
                        z.LineStyleValue,
                        z.ChartLineThicknessId,
                        z.LineThickness,
                        z.LegendName,
                        z.GoodValue,
                        z.AcceptableValue,
                        z.MarginalValue,
                        z.PoorValue,
                        z.ATDPosition,
                        z.ATDPositionValue,
                        z.FillArea,
                        z.Polarity,
                        z.IsLegendVisible
                    }).Select(b => new TemplateTabChartChannelConfigurationPreviewViewModel
                    {
                        ChartChannelId = b.Key.ChartChannelId,
                        ChartChannelName = b.Key.ChartChannelName,
                        ChartLineColour = b.Key.ChartLineColour,
                        ChartLineStyleId = b.Key.ChartLineStyleId,
                        LineStyleName = b.Key.LineStyleName,
                        LineStyleValue = b.Key.LineStyleValue,
                        ChartLineThicknessId = b.Key.ChartLineThicknessId,
                        LineThickness = b.Key.LineThickness,
                        LegendName = b.Key.LegendName,
                        GoodValue = b.Key.GoodValue,
                        AcceptableValue = b.Key.AcceptableValue,
                        MarginalValue = b.Key.MarginalValue,
                        PoorValue = b.Key.PoorValue,
                        ATDPosition = b.Key.ATDPosition,
                        ATDPositionValue = b.Key.ATDPositionValue,
                        FillArea = b.Key.FillArea,
                        Polarity = b.Key.Polarity,
                        IsLegendVisible = b.Key.IsLegendVisible,
                        //ATDPositionAxisValue=5,
                        ATDPositionAxisValue = chartAxisConfiguration.GetATDPositionAxisValue(testId, new ATDRequestModel { CalculationName = b.Key.ChartChannelName, ATDPosition = b.Key.ATDPositionValue }),
                        ChartChannelATDPositions = chartAxisConfiguration.GetATDPositionsAxisValue(testId, GetATDPositionsById(b.Key.ChartChannelId)),
                        BarChartXAxisValue = (tabChartConfigurationData.Where(d => d.TemplateTabChartConfigurationId == y.Key.TemplateTabChartConfigurationId && d.ChartChannelId == b.Key.ChartChannelId).Select(e => e.ChartTypeId).FirstOrDefault() == 2) ? chartAxisConfiguration.GetBarChartXAxisValue(testId, b.Key.ChartChannelName) : 0,
                        ChannelXAxis = //chartAxisConfiguration.GetChannelXAxis(testId, tabChartConfigurationData.Select(e => e.XAxisChannel).FirstOrDefault()),
                                       chartAxisConfiguration.GetChannelDataXAxis(testId, tabChartConfigurationData.Select(e => e.XAxisChannel).FirstOrDefault()).Unit ==
                                       tabChartConfigurationData.Select(e => e.XAxisUnits).FirstOrDefault() ? chartAxisConfiguration.GetChannelXAxis(testId, tabChartConfigurationData.Select(e => e.XAxisChannel).FirstOrDefault()) :
                                       chartAxisConfiguration.GetChannelDataConverted(testId, submatrixName, tabChartConfigurationData.Select(e => e.XAxisUnits).FirstOrDefault(), tabChartConfigurationData.Select(e => e.XAxisChannel).FirstOrDefault()),

                        ChannelYAxis = //chartAxisConfiguration.GetChannelYAxis(testId, b.Key.ChartChannelName, b.Key.Polarity.HasValue ? b.Key.Polarity.Value: false)
                                       chartAxisConfiguration.GetChannelDataYAxis(testId, b.Key.ChartChannelName, b.Key.Polarity.HasValue ? b.Key.Polarity.Value : false).Unit ==
                                       tabChartConfigurationData.Select(e => e.YAxisUnits).FirstOrDefault() ? chartAxisConfiguration.GetChannelYAxis(testId, b.Key.ChartChannelName, b.Key.Polarity.HasValue ? b.Key.Polarity.Value : false) :
                                       chartAxisConfiguration.GetChannelDataConverted(testId, submatrixName, tabChartConfigurationData.Select(e => e.YAxisUnits).FirstOrDefault(), b.Key.ChartChannelName)
                    }).ToList(),
                    ChartAnnotationConfiguration = tabChartConfigurationData.Where(c => c.TemplateTabChartConfigurationId == y.Key.TemplateTabChartConfigurationId)
                    .GroupBy(z => new
                    {
                        z.ChartAnnotationId,
                        z.AnnotationLineStartXCoOrdinate,
                        z.AnnotationLineEndXCoOrdinate,
                        z.AnnotationLineStartYCoOrdinate,
                        z.AnnotationLineEndYCoOrdinate,
                        z.AnnotationLineThicknessId,
                        z.AnnotationLineColour,
                        z.AnnotationLineStyleId,
                        z.AnnotationText,
                        z.AnnotationTextStartXCoOrdinate,
                        z.AnnotationTextStartYCoOrdinate
                    })
                    .Select(c => new TemplateTabChartAnnotationConfigurationPreviewViewModel
                    {
                        ChartAnnotationId = c.Key.ChartAnnotationId,
                        AnnotationLineStartXCoOrdinate = c.Key.AnnotationLineStartXCoOrdinate,
                        AnnotationLineEndXCoOrdinate = c.Key.AnnotationLineEndXCoOrdinate,
                        AnnotationLineStartYCoOrdinate = c.Key.AnnotationLineStartYCoOrdinate,
                        AnnotationLineEndYCoOrdinate = c.Key.AnnotationLineEndYCoOrdinate,
                        AnnotationLineThicknessId = c.Key.AnnotationLineThicknessId,
                        AnnotationLineColour = c.Key.AnnotationLineColour,
                        AnnotationLineStyleId = c.Key.AnnotationLineStyleId,
                        AnnotationText = c.Key.AnnotationText,
                        AnnotationTextStartXCoOrdinate = c.Key.AnnotationTextStartXCoOrdinate,
                        AnnotationTextStartYCoOrdinate = c.Key.AnnotationTextStartYCoOrdinate
                    }).ToList()
                }).ToList();

                #endregion

                //Load TabConfiguration Data
                #region TabConfiguration
                var tabConfigurationRowsData = tabConfigurationData.ToList().Select(x => new
                {
                    TemplateTabId = tabConfigurationData.Where(y => y.TemplateTabId == x.TemplateTabId).Select(z => z.TemplateTabId).FirstOrDefault(),
                    RowId = tabConfigurationData.Where(y => y.TemplateTabId == x.TemplateTabId && y.RowId == x.RowId).Select(z => z.RowId).FirstOrDefault()
                }).Distinct().ToList();

                var tabConfigurationFinalData = tabConfigurationRowsData.Select(x => new TemplateTabConfigurationPreviewRowsViewModel
                {
                    TemplateTabId = x.TemplateTabId,
                    RowId = x.RowId,
                    Columns = tabConfigurationData.Where(y => y.RowId == x.RowId && y.TemplateTabId == x.TemplateTabId).Select(z => new TemplateTabConfigurationPreviewColumnsViewModel
                    {
                        Id = z.Id,
                        RowId = z.RowId,
                        ColumnId = z.ColumnId,
                        ColumnSpan = z.ColumnSpan,
                        DisplayFormatTypeId = z.DisplayFormatTypeId,
                        TabTableConfigurationId = z.TabTableConfigurationId,
                        TabTableConfigurationName = z.TabTableConfigurationName,
                        TabChartConfigurationId = z.TabChartConfigurationId,
                        TabChartConfigurationName = z.TabChartConfigurationName,
                        TemplateTabTableConfigurationPreviewViewModel = tabTableConfigurationPreviewResultData.Where(a => a.TemplateTabTableConfigurationId == z.TabTableConfigurationId).ToList(),
                        TemplateTabChartConfigurationPreviewViewModel = templateTabChartConfigurationPreviewData.Where(a => a.TemplateTabChartConfigurationId == z.TabChartConfigurationId).FirstOrDefault()
                    }).ToList()
                }).Distinct().ToList();

                var tabsPreviewData = tabData.Select(x => new TemplateTabPreviewViewModel
                {
                    Id = x.Id,
                    TemplateId = x.TemplateId,
                    TemplateTabName = x.TemplateTabName,
                    TemplateTabConfigurationPreviewModel = tabConfigurationFinalData.Where(a => a.TemplateTabId == x.Id).ToList()
                }).ToList();

                #endregion

                return tabsPreviewData;
            }
        }

        public bool CheckTemplateName(int templateId, string name, int testType, List<UniqueIdsViewModel> testMode)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var TestModeIds = testMode.ToDataTable<UniqueIdsViewModel>().AsTableValuedParameter("dbo.UniqueIdsTable");
                var result = sqlcon.QueryAsync<string>(TemplateQueries.StoreProcCheckTemplateName, new { @TemplateId = templateId, @Name = name, @TestType = testType, @TestMode = TestModeIds }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
                return (result != null && result == "Yes") ? true : false;
            }
        }

        public SpTransactionMessage Insert(TemplateViewModel templateViewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateQueries.StoreProcInsert,
                     new
                     {
                         Name = templateViewModel.Name,
                         TestTypeId = templateViewModel.TestTypeId,
                         TestModeIds = templateViewModel.TestModeIds.ToDataTable<UniqueIdsViewModel>().AsTableValuedParameter("dbo.UniqueIdsTable"),
                         //TestModeId = templateViewModel.TestModeIds,
                         CreatedOn = DateTime.UtcNow,
                         CreatedBy = templateViewModel.CreatedBy
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(TemplateViewModel templateViewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateQueries.StoreProcUpdate,
                    new
                    {
                        Id = templateViewModel.Id,
                        Name = templateViewModel.Name,
                        TestTypeId = templateViewModel.TestTypeId,
                        TestModeId=templateViewModel.TestModeIds.ToDataTable<UniqueIdsViewModel>().AsTableValuedParameter("dbo.UniqueIdsTable"),
                        //TestModeIds = templateViewModel.TestModeIds,
                        ModifiedOn = DateTime.UtcNow,
                        ModifiedBy = templateViewModel.CreatedBy
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateQueries.StoreProcDelete,
                    new
                    {
                        Id = Id
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Enable(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateQueries.StoreProcEnable,
                    new
                    {
                        Id = Id
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        private List<ATDRequestModel> GetATDPositionsById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ATDPositionChartChannelMapViewModel>(ChartChannelQueries.StoreProcGetATDPositionById, new { @ChartChannelId = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.Select(d => new ATDRequestModel { ATDPositionId = (d.AtdpositionId.HasValue) ? d.AtdpositionId.Value : 0, ATDPosition = d.ATDPositionValue, CalculationName = d.ChannelName }).ToList();
            }
        }
    }
}
