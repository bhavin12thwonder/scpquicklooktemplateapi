﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.RepositoryImpl
{
    public class ChartLineThicknessRepository: IChartLineThicknessRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();

        public List<ChartLineThicknessViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartLineThicknessViewModel>(ChartLineThicknessQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public ChartLineThicknessViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartLineThicknessViewModel>(ChartLineThicknessQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Insert(ChartLineThicknessViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartLineThicknessQueries.StoreProcInsert,
                     new
                     {
                         Name = viewModel.Name,
                         UserId = viewModel.CreatedBy
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(ChartLineThicknessViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartLineThicknessQueries.StoreProcUpdate,
                    new
                    {
                        Id = viewModel.Id,
                        Name = viewModel.Name,
                        UserId = viewModel.CreatedBy
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartLineThicknessQueries.StoreProcDelete,
                    new
                    {
                        Id = Id
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
