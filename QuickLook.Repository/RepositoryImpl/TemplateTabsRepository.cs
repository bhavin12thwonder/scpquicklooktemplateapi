﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public class TemplateTabsRepository : ITemplateTabsRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();
        public List<TemplateTabsViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabsViewModel>(TemplateTabsQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public TemplateTabsViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabsViewModel>(TemplateTabsQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public List<TemplateTabsViewModel> GetByTemplateId(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabsViewModel>(TemplateTabsQueries.StoreProcGetByTemplateId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public SpTransactionMessage Insert(TemplateTabsCreateViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabsQueries.StoreProcInsert,
                     new
                     {
                         Name = viewModel.Name,
                         TemplateId = viewModel.TemplateId,
                         TabIndex=viewModel.TabIndex,
                         DisplayOrder = viewModel.DisplayOrder,
                         CreatedOn = DateTime.UtcNow,
                         CreatedBy = viewModel.CreatedBy
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(TemplateTabsViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabsQueries.StoreProcUpdate,
                    new
                    {
                        Id = viewModel.Id,
                        Name = viewModel.Name,
                        TemplateId = viewModel.TemplateId,
                        TabIndex = viewModel.TabIndex,
                        DisplayOrder = viewModel.TabIndex,
                        Rows = viewModel.Rows,
                        Columns = viewModel.Columns,
                        RowsLimit = viewModel.RowsLimit,
                        ModifiedOn = DateTime.UtcNow,
                        ModifiedBy = viewModel.CreatedBy
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id,int tabIndex)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabsQueries.StoreProcDelete,
                    new
                    {
                        @TemplateId = Id,
                        @TabIndex= tabIndex
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
