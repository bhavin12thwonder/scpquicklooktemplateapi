﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.RepositoryImpl
{
    public class TemplateTabTableConfigurationDetailsRepository : ITemplateTabTableConfigurationDetailsRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();
        public List<TemplateTabTableConfigurationDetailsViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabTableConfigurationDetailsViewModel>(TemplateTabTableConfigurationDetailsQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public TemplateTabTableConfigurationDetailsViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabTableConfigurationDetailsViewModel>(TemplateTabTableConfigurationDetailsQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public List<TemplateTabTableConfigurationDetailsRowsViewModel> GetByTemplateTabTableConfigId(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var result = sqlcon.QueryAsync<TemplateTabTableConfigurationDetailsViewModel>(TemplateTabTableConfigurationDetailsQueries.StoreProcGetByTemplateTabTableConfigId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
                var rowsData = result.Select(x => new
                {
                    x.TemplateTabTableConfigurationName,
                    x.TemplateTabTableConfigurationId,
                    x.RowId
                }).Distinct().ToList();
                var resultData = rowsData.Select(x => new TemplateTabTableConfigurationDetailsRowsViewModel
                {
                    TemplateTabTableConfigurationName = x.TemplateTabTableConfigurationName,
                    TemplateTabTableConfigurationId = x.TemplateTabTableConfigurationId,
                    RowId = x.RowId,
                    Columns = result.Where(y => y.RowId == x.RowId).Select(z => new TemplateTabTableConfigurationDetailsColumnsViewModel
                    {
                        Id = z.Id,
                        RowId = z.RowId,
                        ColumnId = z.ColumnId,
                        RowSpan = z.RowSpan,
                        ColumnSpan = z.ColumnSpan,
                        ColumnWidth = z.ColumnWidth,
                        ColumnType = z.ColumnType,
                        UserDefinedColumn = z.UserDefinedColumn,
                        DbColumn = z.DbColumn,
                        ValueType=z.ValueType,
                        AtdPosition=z.AtdPosition,
                        ConditionalUserDefinedColumn=z.ConditionalUserDefinedColumn,
                        ResultColumn = z.ResultColumn,
                        ColourCode=z.ColourCode,
                        IsConfigHeader = z.IsConfigHeader,
                        IsHeader = z.IsHeader,
                        IsConditional = z.IsConditional,
                        IsDbColumn = z.IsDbColumn,
                        IsUserDefined = z.IsUserDefined,
                        IsReadOnly = z.IsReadOnly,
                        IsDeleted = z.IsDeleted,
                        FormulaOperatorId = z.FormulaOperatorId,
                        TemplateTabTableConfigurationFormulaId = z.TemplateTabTableConfigurationFormulaId
                    }).ToList()
                }).Distinct().ToList();
                return resultData;
            }
        }
        
        public List<TemplateTabTableConfigurationDetailsPreviewRowsViewModel> PreviewByTemplateTabTableConfigId(int Id, int testId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                TableCalculationConfiguration tableCalculationConfiguration = new TableCalculationConfiguration();
                var result = sqlcon.QueryAsync<TemplateTabTableConfigurationDetailsPreviewViewModel>(TemplateTabTableConfigurationDetailsQueries.StoreProcPreviewByTemplateTabTableConfigId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
                var rowsData = result.Select(x => new
                {
                    x.RowId
                }).Distinct().ToList();
                var resultData = rowsData.Select(x => new TemplateTabTableConfigurationDetailsPreviewRowsViewModel
                {
                    RowId = x.RowId,
                    Columns = result.Where(y => y.RowId == x.RowId).Select(z => new TemplateTabTableConfigurationDetailsPreviewColumnsViewModel
                    {
                        Id = z.Id,
                        RowId = z.RowId,
                        ColumnId = z.ColumnId,
                        RowSpan = z.RowSpan,
                        ColumnSpan = z.ColumnSpan,
                        ColumnWidth = z.ColumnWidth,
                        ColumnType = z.ColumnType,
                        DbColumn = z.DbColumn,
                        ColumnHeader = z.ColumnHeader,
                        ColumnData = !string.IsNullOrEmpty(z.DbColumn) ? tableCalculationConfiguration.GetCalculationValue(testId,z.DbColumn, z.ASAMATDValue).ToString() : z.ColumnData,
                        ColourCode = z.ColourCode,
                        IsHeader = z.IsHeader,
                        IsDeleted = z.IsDeleted
                    }).ToList()
                }).Distinct().ToList();
                return resultData;
            }
        }

        public SpTransactionMessage Insert(List<TemplateTabTableConfigurationDetailsUpsertViewModel> viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabTableConfigurationDetailsQueries.StoreProcInsert,
                     new
                     {
                         TemplateTabTableConfigurationDetails = viewModel.ToDataTable<TemplateTabTableConfigurationDetailsUpsertViewModel>().AsTableValuedParameter("dbo.TemplateTabTableConfigurationDetailsTable"),
                         UserId = userId
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(List<TemplateTabTableConfigurationDetailsUpsertViewModel> viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabTableConfigurationDetailsQueries.StoreProcUpdate,
                    new
                    {
                        TemplateTabTableConfigurationDetails = viewModel.ToDataTable<TemplateTabTableConfigurationDetailsUpsertViewModel>().AsTableValuedParameter("dbo.TemplateTabTableConfigurationDetailsTable"),
                        UserId = userId
                    }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabTableConfigurationDetailsQueries.StoreProcDelete,
                    new
                    {
                        Id = Id,
                        UserId = userId
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
