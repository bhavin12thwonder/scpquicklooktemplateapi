﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public class TemplateTabConfigurationRepository : ITemplateTabConfigurationRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();
        public List<TemplateTabConfigurationViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabConfigurationViewModel>(TemplateTabConfigurationQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public TemplateTabConfigurationViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabConfigurationViewModel>(TemplateTabConfigurationQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        //public List<TemplateTabConfigurationViewModel> GetByTemplateTabId(int Id)
        //{
        //    using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
        //    {
        //        return sqlcon.QueryAsync<TemplateTabConfigurationViewModel>(TemplateTabConfigurationQueries.StoreProcGetByTemplateTabId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
        //    }
        //}

        public TemplateTabConfigurationTypeCheckViewModel GetTemplateTabConfigTypeByTabId(int Id, int configTypeId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabConfigurationTypeCheckViewModel>(TemplateTabConfigurationQueries.StoreProcTemplateTabConfigurationTypeGetByTemplateTabId, new { @Id = Id, @TemplateTabConfigurationType = configTypeId }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault(); ;
            }
        }

        public List<TemplateTabConfigurationRowsViewModel> GetByTemplateTabId(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var result = sqlcon.QueryAsync<TemplateTabConfigurationViewModel>(TemplateTabConfigurationQueries.StoreProcGetByTemplateTabId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
                var rowsData = result.Select(x => new
                {
                    x.TemplateTabId,
                    x.RowId
                }).Distinct().ToList();
                var resultData = rowsData.Select(x => new TemplateTabConfigurationRowsViewModel
                {
                    TemplateTabId = x.TemplateTabId,
                    RowId = x.RowId,
                    Columns = result.Where(y => y.RowId == x.RowId).Select(z => new TemplateTabConfigurationColumnsViewModel
                    {
                        Id = z.Id,
                        RowId = z.RowId,
                        ColumnId = z.ColumnId,
                        ColumnSpan = z.ColumnSpan,
                        DisplayFormatTypeId = z.DisplayFormatTypeId,
                        TemplateTabTableConfigurationName = z.TemplateTabTableConfigurationName,
                        TemplateTabTableConfigurationId = z.TemplateTabTableConfigurationId,
                        IsTabTableConfigExists = z.IsTabTableConfigExists,
                        TemplateTabTableConfigurationLastUpdatedBy = z.TemplateTabTableConfigurationLastUpdatedBy + " " + z.TemplateTabTableConfigurationLastUpdatedOn.ToString("MM/dd/yyyy hh:mm:ss"),
                        ChartTypeId=z.ChartTypeId,
                        TemplateTabChartConfigurationName = z.TemplateTabChartConfigurationName,
                        TemplateTabChartConfigurationId = z.TemplateTabChartConfigurationId,
                        IsTabChartConfigExists = z.IsTabChartConfigExists,
                        TemplateTabChartConfigurationLastUpdatedBy = z.TemplateTabChartConfigurationLastUpdatedBy + " " + z.TemplateTabChartConfigurationLastUpdatedOn.ToString("MM/dd/yyyy hh:mm:ss")
                    }).ToList()
                }).Distinct().ToList();
                return resultData;
            }
        }

        //public List<TemplateTabConfigurationPreviewRowsViewModel> PreviewByTemplateTabId(int Id)
        //{
        //    using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
        //    {
        //        var tabConfiguration = sqlcon.QueryMultiple(TemplateTabConfigurationQueries.StoreProcPreviewByTemplateTabId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure);
        //        var tabConfigurationPreviewData = tabConfiguration.ReadAsync<TemplateTabConfigurationPreviewViewModel>().Result;
        //        var tabTableConfigurationPreviewData = tabConfiguration.ReadAsync<TemplateTabTableConfigurationDetailsPreviewViewModel>().Result;
        //        var templateTabChartConfigurationDbData = tabConfiguration.ReadAsync<TemplateTabChartConfigurationDetailsPreviewViewModel>().Result;

        //        //Load TableConfiguration Data
        //        #region TableConfiguration

        //        var tabTableConfigurationPreviewRowsData = tabTableConfigurationPreviewData.Select(x => new
        //        {
        //            x.RowId,
        //            x.TemplateTabTableConfigurationId,
        //            x.TemplateTabTableConfigurationName
        //        }).Distinct().ToList();
        //        var tabTableConfigurationPreviewResultData = tabTableConfigurationPreviewRowsData.Select(x => new TemplateTabTableConfigurationDetailsPreviewRowsViewModel
        //        {
        //            RowId = x.RowId,
        //            TemplateTabTableConfigurationId = x.TemplateTabTableConfigurationId,
        //            TemplateTabTableConfigurationName = x.TemplateTabTableConfigurationName,
        //            Columns = tabTableConfigurationPreviewData.Where(y => y.RowId == x.RowId && y.TemplateTabTableConfigurationId == x.TemplateTabTableConfigurationId).Select(z => new TemplateTabTableConfigurationDetailsPreviewColumnsViewModel
        //            {
        //                Id = z.Id,
        //                RowId = z.RowId,
        //                ColumnId = z.ColumnId,
        //                RowSpan = z.RowSpan,
        //                ColumnSpan = z.ColumnSpan,
        //                ColumnWidth = z.ColumnWidth,
        //                ColumnType = z.ColumnType,
        //                DbColumn = z.DbColumn,
        //                ColumnHeader = z.ColumnHeader,
        //                ColumnData = z.ColumnData,
        //                ColourCode = z.ColourCode,
        //                IsHeader = z.IsHeader,
        //                IsDeleted = z.IsDeleted
        //            }).ToList()
        //        }).Distinct().ToList();

        //        #endregion

        //        //Load ChartConfiguration
        //        #region ChartConfiguration

        //        var templateTabChartConfigurationDataList = templateTabChartConfigurationDbData.Select(x => new TemplateTabChartConfigurationIdsPreviewViewModel
        //        {
        //            Id = x.Id,
        //            TemplateTabChartConfigurationId = x.TemplateTabChartConfigurationId
        //        }).GroupBy(x => new { x.Id, x.TemplateTabChartConfigurationId }).ToList();

        //        var templateTabChartConfigurationData = templateTabChartConfigurationDataList.Select(y => new TemplateTabChartConfigurationPreviewViewModel
        //        {
        //            ChartTypeValue = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.ChartTypeValue).FirstOrDefault(),
        //            TemplateTabChartConfigurationId = y.Key.TemplateTabChartConfigurationId,
        //            TemplateTabChartConfigurationName = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.TemplateTabChartConfigurationName).FirstOrDefault(),
        //            XAxis = new int[10] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
        //            YAxis = new int[6] { 10, 20, 30, 40, 50, 60 },
        //            XAxisUnits = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisUnits).FirstOrDefault(),
        //            XAxisMinValue = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisMinValue).FirstOrDefault(),
        //            XAxisMaxValue = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisMaxValue).FirstOrDefault(),
        //            XAxisInterval = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisInterval).FirstOrDefault(),
        //            XAxisLabel = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.XAxisLabel).FirstOrDefault(),
        //            YAxisUnits = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisUnits).FirstOrDefault(),
        //            YAxisMinValue = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisMinValue).FirstOrDefault(),
        //            YAxisMaxValue = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisMaxValue).FirstOrDefault(),
        //            YAxisInterval = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisInterval).FirstOrDefault(),
        //            YAxisLabel = templateTabChartConfigurationDbData.Where(b => b.Id == y.Key.Id).Select(z => z.YAxisLabel).FirstOrDefault(),
        //            ChartChannelConfiguration = templateTabChartConfigurationDbData.Where(b => b.TemplateTabChartConfigurationId == y.Key.TemplateTabChartConfigurationId)
        //            .GroupBy(z => new
        //            {
        //                z.ChartChannelId,
        //                z.ChartChannelName,
        //                z.ChartLineColour,
        //                z.ChartLineStyleId,
        //                z.LineStyleName,
        //                z.LineStyleValue,
        //                z.ChartLineThicknessId,
        //                z.LineThickness,
        //                z.LegendName,
        //                z.FillArea,
        //                z.Polarity,
        //                z.IsLegendVisible
        //            }).Select(b => new TemplateTabChartChannelConfigurationPreviewViewModel
        //            {
        //                ChartChannelId = b.Key.ChartChannelId,
        //                ChartChannelName = b.Key.ChartChannelName,
        //                ChartLineColour = b.Key.ChartLineColour,
        //                ChartLineStyleId = b.Key.ChartLineStyleId,
        //                LineStyleName = b.Key.LineStyleName,
        //                LineStyleValue = b.Key.LineStyleValue,
        //                ChartLineThicknessId = b.Key.ChartLineThicknessId,
        //                LineThickness = b.Key.LineThickness,
        //                LegendName = b.Key.LegendName,
        //                FillArea = b.Key.FillArea,
        //                Polarity = b.Key.Polarity,
        //                IsLegendVisible = b.Key.IsLegendVisible
        //            }).ToList(),
        //            ChartAnnotationConfiguration = templateTabChartConfigurationDbData.Where(c => c.TemplateTabChartConfigurationId == y.Key.TemplateTabChartConfigurationId)
        //            .GroupBy(z => new
        //            {
        //                z.ChartAnnotationId,
        //                z.AnnotationLineStartXCoOrdinate,
        //                z.AnnotationLineEndXCoOrdinate,
        //                z.AnnotationLineStartYCoOrdinate,
        //                z.AnnotationLineEndYCoOrdinate,
        //                z.AnnotationLineThicknessId,
        //                z.AnnotationLineColour,
        //                z.AnnotationLineStyleId,
        //                z.AnnotationText,
        //                z.AnnotationTextStartXCoOrdinate,
        //                z.AnnotationTextStartYCoOrdinate
        //            })
        //            .Select(c => new TemplateTabChartAnnotationConfigurationPreviewViewModel
        //            {
        //                ChartAnnotationId = c.Key.ChartAnnotationId,
        //                AnnotationLineStartXCoOrdinate = c.Key.AnnotationLineStartXCoOrdinate,
        //                AnnotationLineEndXCoOrdinate = c.Key.AnnotationLineEndXCoOrdinate,
        //                AnnotationLineStartYCoOrdinate = c.Key.AnnotationLineStartYCoOrdinate,
        //                AnnotationLineEndYCoOrdinate = c.Key.AnnotationLineEndYCoOrdinate,
        //                AnnotationLineThicknessId = c.Key.AnnotationLineThicknessId,
        //                AnnotationLineColour = c.Key.AnnotationLineColour,
        //                AnnotationLineStyleId = c.Key.AnnotationLineStyleId,
        //                AnnotationText = c.Key.AnnotationText,
        //                AnnotationTextStartXCoOrdinate = c.Key.AnnotationTextStartXCoOrdinate,
        //                AnnotationTextStartYCoOrdinate = c.Key.AnnotationTextStartYCoOrdinate
        //            }).ToList()
        //        }).ToList();

        //        #endregion

        //        //Load TabConfiguration Data
        //        #region TabConfiguration

        //        var tabConfigurationRowsData = tabConfigurationPreviewData.Select(x => new
        //        {
        //            x.TemplateTabId,
        //            x.RowId
        //        }).Distinct().ToList();

        //        var tabConfigurationData = tabConfigurationRowsData.Select(x => new TemplateTabConfigurationPreviewRowsViewModel
        //        {
        //            TemplateTabId = x.TemplateTabId,
        //            RowId = x.RowId,
        //            Columns = tabConfigurationPreviewData.Where(y => y.RowId == x.RowId).Select(z => new TemplateTabConfigurationPreviewColumnsViewModel
        //            {
        //                Id = z.Id,
        //                RowId = z.RowId,
        //                ColumnId = z.ColumnId,
        //                ColumnSpan = z.ColumnSpan,
        //                DisplayFormatTypeId = z.DisplayFormatTypeId,
        //                TabTableConfigurationId = z.TabTableConfigurationId,
        //                TabTableConfigurationName = z.TabTableConfigurationName,
        //                TabChartConfigurationId = z.TabChartConfigurationId,
        //                TabChartConfigurationName = z.TabChartConfigurationName,
        //                TemplateTabTableConfigurationPreviewViewModel = tabTableConfigurationPreviewResultData.Where(a => a.TemplateTabTableConfigurationId == z.TabTableConfigurationId).ToList(),
        //                TemplateTabChartConfigurationPreviewViewModel = templateTabChartConfigurationData.Where(a => a.TemplateTabChartConfigurationId == z.TabChartConfigurationId).FirstOrDefault()
        //            }).ToList()
        //        }).Distinct().ToList();

        //        #endregion

        //        return tabConfigurationData;
        //    }
        //}

        public SpTransactionMessage Insert(List<TemplateTabConfigurationUpsertViewModel> viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabConfigurationQueries.StoreProcInsert,
                     new
                     {
                         TemplateTabConfigurationTableData = viewModel.ToDataTable<TemplateTabConfigurationUpsertViewModel>().AsTableValuedParameter("dbo.TemplateTabConfigurationTable")
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(List<TemplateTabConfigurationUpsertViewModel> viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabConfigurationQueries.StoreProcUpdate,
                    new
                    {
                        TemplateTabConfigurationTableData = viewModel.ToDataTable<TemplateTabConfigurationUpsertViewModel>().AsTableValuedParameter("dbo.TemplateTabConfigurationTable")
                    }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage UpdateById(TemplateTabConfigurationViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabConfigurationQueries.StoreProcUpdateById,
                    new
                    {
                        Id = viewModel.Id,
                        DisplayFormatTypeId = viewModel.DisplayFormatTypeId
                    }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage UpdateTemplateTabConfigTypeByTabId(int Id, int configTypeId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabConfigurationQueries.StoreProcTemplateTabConfigurationTypeUpdateByTemplateTabId, new { @Id = Id, @TemplateTabConfigurationType = configTypeId }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault(); ;
            }
        }

        public SpTransactionMessage Delete(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabConfigurationQueries.StoreProcDelete,
                    new
                    {
                        Id = Id
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
