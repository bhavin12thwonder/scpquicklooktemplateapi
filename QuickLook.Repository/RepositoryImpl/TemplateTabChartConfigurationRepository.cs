﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.RepositoryImpl
{
    public class TemplateTabChartConfigurationRepository: ITemplateTabChartConfigurationRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();
        public List<TemplateTabChartConfigurationViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabChartConfigurationViewModel>(TemplateTabChartConfigurationQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public TemplateTabChartConfigurationViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabChartConfigurationViewModel>(TemplateTabChartConfigurationQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public List<TemplateTabChartConfigurationViewModel> GetByTemplateTabConfigId(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabChartConfigurationViewModel>(TemplateTabChartConfigurationQueries.StoreProcGetByTemplateTabConfigId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }
        public bool CheckTabChartConfigName(int chartConfigId,int chartTypeId, string Name)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var result = sqlcon.QueryAsync<string>(TemplateTabChartConfigurationQueries.StoreProcCheckTemplateTabChartConfigName, new { @TabConfigId = chartConfigId, @ChartTypeId=chartTypeId, @Name = Name }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
                return (result != null && result == "Yes") ? true : false;
            }
        }

        public SpTransactionMessage Insert(TemplateTabChartConfigurationViewModel viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabChartConfigurationQueries.StoreProcInsert,
                     new
                     {
                         TemplateTabConfigId = viewModel.TemplateTabConfigId,
                         Name = viewModel.Name,
                         ChartTypeId=viewModel.ChartTypeId,
                         ChartOrientation=viewModel.ChartOrientation,
                         UserId = userId
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(TemplateTabChartConfigurationViewModel viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabChartConfigurationQueries.StoreProcUpdate,
                    new
                    {
                        Id = viewModel.Id,
                        TemplateTabConfigId = viewModel.TemplateTabConfigId,
                        Name = viewModel.Name,
                        ChartTypeId = viewModel.ChartTypeId,
                        ChartOrientation=viewModel.ChartOrientation,
                        UserId = userId
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabChartConfigurationQueries.StoreProcDelete,
                    new
                    {
                        @Id = Id,
                        UserId = userId
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
