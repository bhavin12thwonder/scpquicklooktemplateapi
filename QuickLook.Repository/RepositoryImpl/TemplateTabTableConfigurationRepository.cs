﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.RepositoryImpl
{
    public class TemplateTabTableConfigurationRepository : ITemplateTabTableConfigurationRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();
        public List<TemplateTabTableConfigurationViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabTableConfigurationViewModel>(TemplateTabTableConfigurationQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public TemplateTabTableConfigurationViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabTableConfigurationViewModel>(TemplateTabTableConfigurationQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public List<TemplateTabTableConfigurationViewModel> GetByTemplateTabConfigId(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<TemplateTabTableConfigurationViewModel>(TemplateTabTableConfigurationQueries.StoreProcGetByTemplateTabConfigId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }
        public bool CheckTabTableConfigName(int tabConfigId, string Name)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var result= sqlcon.QueryAsync<string>(TemplateTabTableConfigurationQueries.StoreProcCheckTemplateTabTableConfigName, new { @TabConfigId = tabConfigId, @Name = Name }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
                return (result != null && result == "Yes") ? true : false;
            }
        }

        public SpTransactionMessage Insert(TemplateTabTableConfigurationViewModel viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabTableConfigurationQueries.StoreProcInsert,
                     new
                     {
                         TemplateTabConfigId = viewModel.TemplateTabConfigId,
                         Name = viewModel.Name,
                         UserId = userId
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(TemplateTabTableConfigurationViewModel viewModel, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabTableConfigurationQueries.StoreProcUpdate,
                    new
                    {
                        Id = viewModel.Id,
                        TemplateTabConfigId = viewModel.TemplateTabConfigId,
                        Name = viewModel.Name,
                        UserId = userId
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id, int userId)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(TemplateTabTableConfigurationQueries.StoreProcDelete,
                    new
                    {
                        @Id = Id,
                        UserId = userId
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
