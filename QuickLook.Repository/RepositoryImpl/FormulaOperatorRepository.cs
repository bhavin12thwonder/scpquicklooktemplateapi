﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLook.Repository.RepositoryImpl
{
    public class FormulaOperatorRepository: IFormulaOperatorRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();

        public List<FormulaOperatorViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<FormulaOperatorViewModel>(FormulaOperatorQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public FormulaOperatorViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<FormulaOperatorViewModel>(FormulaOperatorQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Insert(FormulaOperatorViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(FormulaOperatorQueries.StoreProcInsert,
                     new
                     {
                         Name = viewModel.Name,
                         TestTypeId = viewModel.Operator,
                         ModifiedBy = viewModel.CreatedBy
                     }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Update(FormulaOperatorViewModel viewModel)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(FormulaOperatorQueries.StoreProcUpdate,
                    new
                    {
                        Id = viewModel.Id,
                        Name = viewModel.Name,
                        TestTypeId = viewModel.Operator,
                        ModifiedBy = viewModel.CreatedBy
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage Delete(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(FormulaOperatorQueries.StoreProcDelete,
                    new
                    {
                        Id = Id
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }
    }
}
