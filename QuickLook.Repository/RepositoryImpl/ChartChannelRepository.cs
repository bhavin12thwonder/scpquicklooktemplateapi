﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using QuickLook.Common;
using QuickLook.Model.Entities;
using QuickLook.Model.ViewModel;
using QuickLook.Repository.Repository;
using QuickLook.Repository.SqlQueries;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace QuickLook.Repository.RepositoryImpl
{
    public class ChartChannelRepository : IChartChannelRepository
    {
        private readonly QuickLookContext db = new QuickLookContext();

        public List<ChartChannelViewModel> GetAll()
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartChannelViewModel>(ChartChannelQueries.StoreProcGetAll, null, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
            }
        }

        public ChartChannelViewModel GetById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<ChartChannelViewModel>(ChartChannelQueries.StoreProcGetById, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public List<ChartChannelViewModel> GetByChartConfigId(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var result = sqlcon.QueryAsync<ChartChannelViewModel>(ChartChannelQueries.StoreProcGetByChartConfigId, new { @Id = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.ToList();
                var chartChannelViewModelData = result.Select(x => new ChartChannelViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    TemplateTabChartConfigurationId = x.TemplateTabChartConfigurationId,
                    //ATDPosition = x.ATDPosition,
                    //ATDPosition = GetATDPositionsNameById(x.Id),
                    ATDPositionIds = GetATDPositionsById(x.Id),
                    ATDPositionNameIds= GetATDPositionsNameById(x.Id),
                    ChartLineColour =x.ChartLineColour,
                    ChartLineStyleId=x.ChartLineStyleId,
                    ChartLineThicknessId=x.ChartLineThicknessId,
                    FillArea=x.FillArea,
                    Polarity = x.Polarity,
                    IsLegendVisible =x.IsLegendVisible,
                    LegendName=x.LegendName,
                    GoodValue = x.GoodValue,
                    AcceptableValue = x.AcceptableValue,
                    MarginalValue =x.MarginalValue,
                    PoorValue = x.PoorValue,
                    IsDeleted = x.IsDeleted,
                    IsAreaChart=x.IsAreaChart,
                    CreatedBy = x.CreatedBy,
                    CreatedOn = x.CreatedOn,
                    ModifiedBy = x.ModifiedBy,
                    ModifiedOn = x.ModifiedOn,
                    XAxisChannelId = x.XAxisChannelId,
                    SiUnit = x.SiUnit
                }).ToList();
                return chartChannelViewModelData;
            }
        }

        public SpTransactionMessage Insert(ChartChannelViewModel viewModel, int userId)
        {
            try
            {
                if (viewModel != null)
                {
                    using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
                    {
                        return sqlcon.QueryAsync<SpTransactionMessage>(ChartChannelQueries.StoreProcInsert,
                             new
                             {
                             //ChartChannelTable = viewModel.ToDataTable<ChartChannelViewModel>().AsTableValuedParameter("dbo.ChartChannelTable"),
                             TemplateTabChartConfigurationId = viewModel.TemplateTabChartConfigurationId,
                                 Name = viewModel.Name,
                                 ChartLineColour = viewModel.ChartLineColour,
                                 ChartLineStyleId = viewModel.ChartLineStyleId,
                                 ChartLineThicknessId = viewModel.ChartLineThicknessId,
                                 LegendName = viewModel.LegendName,
                                 GoodValue = viewModel.GoodValue,
                                 AcceptableValue = viewModel.AcceptableValue,
                                 MarginalValue = viewModel.MarginalValue,
                                 PoorValue = viewModel.PoorValue,
                                 //ATDPosition = viewModel.ATDPositionName,
                                 ATDPosition = viewModel.ATDPosition,
                                 //ATDPositionIds=viewModel.ATDPositionIds,
                                 ATDPositionIds = viewModel.ATDPositionIds.ToDataTable<UniqueIdsViewModel>().AsTableValuedParameter("dbo.UniqueIdsTable"),
                                 FillArea = viewModel.FillArea,
                                 Polarity = viewModel.Polarity,
                                 IsLegendVisible = viewModel.IsLegendVisible,
                                 SiUnit = viewModel.SiUnit,
                                 IsDeleted = viewModel.IsDeleted,
                                 UserId = userId
                             }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
                    }
                }
                else
                {
                    //Console.WriteLine("check----------------");
                    return null;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("\nMessage ---\n{0}", e.Message);
                return null;
            }
        }

        public SpTransactionMessage Update(ChartChannelViewModel viewModel, int userId)
        {
            try
            {
                using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
                {
                    return sqlcon.QueryAsync<SpTransactionMessage>(ChartChannelQueries.StoreProcUpdate,
                        new
                        {
                        //ChartChannelTable = viewModel.ToDataTable<ChartChannelViewModel>().AsTableValuedParameter("dbo.ChartChannelTable"),
                        Id = viewModel.Id,
                            TemplateTabChartConfigurationId = viewModel.TemplateTabChartConfigurationId,
                            Name = viewModel.Name,
                            ChartLineColour = viewModel.ChartLineColour,
                            ChartLineStyleId = viewModel.ChartLineStyleId,
                            ChartLineThicknessId = viewModel.ChartLineThicknessId,
                            LegendName = viewModel.LegendName,
                            GoodValue = viewModel.GoodValue,
                            AcceptableValue = viewModel.AcceptableValue,
                            MarginalValue = viewModel.MarginalValue,
                            PoorValue = viewModel.PoorValue,
                            ATDPosition = viewModel.ATDPosition,
                            //ATDPositionIds = viewModel.ATDPositionIds,
                            ATDPositionIds = viewModel.ATDPositionIds.ToList().ToDataTable<UniqueIdsViewModel>().AsTableValuedParameter("dbo.UniqueIdsTable"),
                            FillArea = viewModel.FillArea,
                            Polarity = viewModel.Polarity,
                            IsLegendVisible = viewModel.IsLegendVisible,
                            SiUnit = viewModel.SiUnit,
                            IsDeleted = viewModel.IsDeleted,
                            UserId = userId
                        }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nMessage ---\n{0}", e.Message);
                return null;
            }
        }

        //public SpTransactionMessage Insert(List<ChartChannelViewModel> viewModel, int userId)
        //{
        //    using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
        //    {
        //        return sqlcon.QueryAsync<SpTransactionMessage>(ChartChannelQueries.StoreProcInsert,
        //             new
        //             {
        //                 ChartChannelTable = viewModel.ToDataTable<ChartChannelViewModel>().AsTableValuedParameter("dbo.ChartChannelTable"),
        //                 UserId = userId
        //             }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
        //    }
        //}

        //public SpTransactionMessage Update(List<ChartChannelViewModel> viewModel, int userId)
        //{
        //    using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
        //    {
        //        return sqlcon.QueryAsync<SpTransactionMessage>(ChartChannelQueries.StoreProcUpdate,
        //            new
        //            {
        //                ChartChannelTable = viewModel.ToDataTable<ChartChannelViewModel>().AsTableValuedParameter("dbo.ChartChannelTable"),
        //                UserId = userId
        //            }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
        //    }
        //}

        public SpTransactionMessage Delete(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                return sqlcon.QueryAsync<SpTransactionMessage>(ChartChannelQueries.StoreProcDelete,
                    new
                    {
                        Id = Id
                    }, null, 0, System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
            }
        }

        public SpTransactionMessage UpdateXAxisChannel(ChartChannelViewModel viewModel, int userId)
        {
            try
            {
                if (viewModel != null)
                {
                    using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
                    {
                        return sqlcon.QueryAsync<SpTransactionMessage>(ChartChannelQueries.StoreProcXaxisChartChannel,
                             new
                             {
                                 //ChartChannelTable = viewModel.ToDataTable<ChartChannelViewModel>().AsTableValuedParameter("dbo.ChartChannelTable"),
                                 TemplateTabChartConfigurationId = viewModel.TemplateTabChartConfigurationId,
                                 Name = viewModel.Name,
                                 ChartLineColour = viewModel.ChartLineColour,
                                 ChartLineStyleId = viewModel.ChartLineStyleId,
                                 ChartLineThicknessId = viewModel.ChartLineThicknessId,
                                 LegendName = viewModel.LegendName,
                                 GoodValue = viewModel.GoodValue,
                                 AcceptableValue = viewModel.AcceptableValue,
                                 MarginalValue = viewModel.MarginalValue,
                                 PoorValue = viewModel.PoorValue,
                                 //ATDPosition = viewModel.ATDPositionName,
                                 ATDPosition = viewModel.ATDPosition,
                                 //ATDPositionIds=viewModel.ATDPositionIds,
                                 ATDPositionIds = viewModel.ATDPositionIds.ToDataTable<UniqueIdsViewModel>().AsTableValuedParameter("dbo.UniqueIdsTable"),
                                 FillArea = viewModel.FillArea,
                                 Polarity = viewModel.Polarity,
                                 IsLegendVisible = viewModel.IsLegendVisible,
                                 IsDeleted = viewModel.IsDeleted,
                                 UserId = userId
                             }, null, commandTimeout: 0, commandType: System.Data.CommandType.StoredProcedure).Result.FirstOrDefault();
                    }
                }
                else
                {
                    //Console.WriteLine("check----------------");
                    return null;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nMessage ---\n{0}", e.Message);
                return null;
            }
        }

        private List<UniqueIdsViewModel> GetATDPositionsById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                 var a = sqlcon.QueryAsync<ATDPositionChartChannelMapViewModel>(ChartChannelQueries.StoreProcGetATDPositionById, new { @ChartChannelId = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.Select(d => new UniqueIdsViewModel { Id = ((d.ATDPositionId.HasValue) ? d.ATDPositionId.Value : 0) }).ToList<UniqueIdsViewModel>();
                return a;
                //return sqlcon.QueryAsync<ATDPositionChartChannelMapViewModel>(ChartChannelQueries.StoreProcGetATDPositionById, new { @ChartChannelId = Id}, null, 0, System.Data.CommandType.StoredProcedure).Result.Select(d =>new UniqueIdsViewModel {Id= ((d.AtdpositionId.HasValue) ? d.AtdpositionId.Value : 0), ATDPositionName=d.ATDPositionName }).ToList<UniqueIdsViewModel>();
            }
        }
        private List<int?> GetATDPositionsNameById(int Id)
        {
            using (SqlConnection sqlcon = new SqlConnection(db.Database.GetDbConnection().ConnectionString))
            {
                var atdNames = sqlcon.QueryAsync<ATDPositionChartChannelMapViewModel>(ChartChannelQueries.StoreProcGetATDPositionById,
                    new { @ChartChannelId = Id }, null, 0, System.Data.CommandType.StoredProcedure).Result.Select(d => d.ATDPositionId).ToList();
                return atdNames;
            }
        }
    }
}
