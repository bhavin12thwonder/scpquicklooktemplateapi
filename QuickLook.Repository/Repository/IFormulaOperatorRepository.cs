﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface IFormulaOperatorRepository
    {
        SpTransactionMessage Insert(FormulaOperatorViewModel viewModel);
        SpTransactionMessage Update(FormulaOperatorViewModel viewModel);
        SpTransactionMessage Delete(int Id);
        List<FormulaOperatorViewModel> GetAll();
        FormulaOperatorViewModel GetById(int Id);
    }
}
