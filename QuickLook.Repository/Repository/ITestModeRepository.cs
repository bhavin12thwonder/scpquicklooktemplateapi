﻿using System;
using System.Collections.Generic;
using System.Text;
using QuickLook.Common;
using QuickLook.Model;
using QuickLook.Model.ViewModel;

namespace QuickLook.Repository.Repository
{
    public interface ITestModeRepository
    {
        SpTransactionMessage Insert(TestModeViewModel templateViewModel);
        SpTransactionMessage Update(TestModeViewModel templateViewModel);
        SpTransactionMessage Delete(int Id);
        List<TestModeViewModel> GetAll();
        TestModeViewModel GetById(int Id);
        List<TestModeViewModel> GetByTestType(int Id);
    }
}
