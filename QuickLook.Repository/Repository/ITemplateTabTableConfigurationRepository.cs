﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface ITemplateTabTableConfigurationRepository
    {
        SpTransactionMessage Insert(TemplateTabTableConfigurationViewModel templateViewModel,int userId);
        SpTransactionMessage Update(TemplateTabTableConfigurationViewModel templateViewModel, int userId);
        SpTransactionMessage Delete(int Id, int userId);
        List<TemplateTabTableConfigurationViewModel> GetAll();
        TemplateTabTableConfigurationViewModel GetById(int Id);
        List<TemplateTabTableConfigurationViewModel> GetByTemplateTabConfigId(int Id);
        bool CheckTabTableConfigName(int tabConfigId, string Name);
    }
}
