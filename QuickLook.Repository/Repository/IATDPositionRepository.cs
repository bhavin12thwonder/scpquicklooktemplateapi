﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface IATDPositionRepository
    {
        SpTransactionMessage Insert(ATDPositionViewModel viewModel);
        SpTransactionMessage Update(ATDPositionViewModel viewModel);
        SpTransactionMessage Delete(int Id);
        List<ATDPositionViewModel> GetAll();
        ATDPositionViewModel GetById(int Id);
    }
}
