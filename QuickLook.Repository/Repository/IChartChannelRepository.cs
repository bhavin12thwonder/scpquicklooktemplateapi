﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface IChartChannelRepository
    {
        //SpTransactionMessage Insert(List<ChartChannelViewModel> viewModel, int userId);
        //SpTransactionMessage Update(List<ChartChannelViewModel> viewModel, int userId);
        SpTransactionMessage Insert(ChartChannelViewModel viewModel, int userId);
        SpTransactionMessage Update(ChartChannelViewModel viewModel, int userId);
        SpTransactionMessage Delete(int Id);
        List<ChartChannelViewModel> GetAll();
        ChartChannelViewModel GetById(int Id);
        List<ChartChannelViewModel> GetByChartConfigId(int Id);
        SpTransactionMessage UpdateXAxisChannel(ChartChannelViewModel viewModel, int userId);
    }
}
