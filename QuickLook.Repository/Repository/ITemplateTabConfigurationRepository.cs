﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface ITemplateTabConfigurationRepository
    {
        SpTransactionMessage Insert(List<TemplateTabConfigurationUpsertViewModel> templateViewModel);
        SpTransactionMessage Update(List<TemplateTabConfigurationUpsertViewModel> viewModel);
        SpTransactionMessage UpdateById(TemplateTabConfigurationViewModel viewModel);
        SpTransactionMessage Delete(int Id);
        List<TemplateTabConfigurationViewModel> GetAll();
        TemplateTabConfigurationViewModel GetById(int Id);
        List<TemplateTabConfigurationRowsViewModel> GetByTemplateTabId(int Id);
        //List<TemplateTabConfigurationPreviewRowsViewModel> PreviewByTemplateTabId(int Id);
        SpTransactionMessage UpdateTemplateTabConfigTypeByTabId(int Id, int configTypeId);
        TemplateTabConfigurationTypeCheckViewModel GetTemplateTabConfigTypeByTabId(int Id,int configTypeId);
    }
}
