﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface ITemplateTabChartConfigurationDetailsRepository
    {
        SpTransactionMessage Insert(TemplateTabChartConfigurationDetailsUpsertViewModel viewModel,int userId);
        SpTransactionMessage Update(TemplateTabChartConfigurationDetailsUpsertViewModel viewModel, int userId);
        SpTransactionMessage Delete(int Id, int userId);
        List<TemplateTabChartConfigurationDetailsViewModel> GetAll();
        TemplateTabChartConfigurationDetailsViewModel GetById(int Id);
        TemplateTabChartConfigurationDetailsViewModel GetByTemplateTabChartConfigId(int Id);
        TemplateTabChartConfigurationPreviewViewModel PreviewByTemplateTabChartConfigId(int Id, int testId, string submatrixName);
    }
}
