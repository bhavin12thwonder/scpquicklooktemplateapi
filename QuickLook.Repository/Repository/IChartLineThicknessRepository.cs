﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface IChartLineThicknessRepository
    {
        SpTransactionMessage Insert(ChartLineThicknessViewModel viewModel);
        SpTransactionMessage Update(ChartLineThicknessViewModel viewModel);
        SpTransactionMessage Delete(int Id);
        List<ChartLineThicknessViewModel> GetAll();
        ChartLineThicknessViewModel GetById(int Id);
    }
}
