﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface IChartLineStyleRepository
    {
        SpTransactionMessage Insert(ChartLineStyleViewModel viewModel);
        SpTransactionMessage Update(ChartLineStyleViewModel viewModel);
        SpTransactionMessage Delete(int Id);
        List<ChartLineStyleViewModel> GetAll();
        ChartLineStyleViewModel GetById(int Id);
    }
}
