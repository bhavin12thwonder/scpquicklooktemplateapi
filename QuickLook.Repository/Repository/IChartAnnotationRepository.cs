﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface IChartAnnotationRepository
    {
        SpTransactionMessage Insert(List<ChartAnnotationViewModel> viewModel,int userId);
        SpTransactionMessage Update(ChartAnnotationViewModel viewModel, int userId);
        SpTransactionMessage Delete(int Id);
        List<ChartAnnotationViewModel> GetAll();
        ChartAnnotationViewModel GetById(int Id);
        List<ChartAnnotationViewModel> GetByChartConfigId(int Id);
    }
}
