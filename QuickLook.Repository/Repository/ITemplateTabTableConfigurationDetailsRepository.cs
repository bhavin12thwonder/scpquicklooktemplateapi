﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface ITemplateTabTableConfigurationDetailsRepository
    {
        SpTransactionMessage Insert(List<TemplateTabTableConfigurationDetailsUpsertViewModel> viewModel,int userId);
        SpTransactionMessage Update(List<TemplateTabTableConfigurationDetailsUpsertViewModel> viewModel, int userId);
        SpTransactionMessage Delete(int Id, int userId);
        List<TemplateTabTableConfigurationDetailsViewModel> GetAll();
        TemplateTabTableConfigurationDetailsViewModel GetById(int Id);
        List<TemplateTabTableConfigurationDetailsRowsViewModel> GetByTemplateTabTableConfigId(int Id);
        List<TemplateTabTableConfigurationDetailsPreviewRowsViewModel> PreviewByTemplateTabTableConfigId(int Id, int testId);
    }
}
