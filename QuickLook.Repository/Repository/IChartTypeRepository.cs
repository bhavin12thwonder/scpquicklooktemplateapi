﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface IChartTypeRepository
    {
        SpTransactionMessage Insert(ChartTypeViewModel viewModel);
        SpTransactionMessage Update(ChartTypeViewModel viewModel);
        SpTransactionMessage Delete(int Id);
        List<ChartTypeViewModel> GetAll();
        ChartTypeViewModel GetById(int Id);
    }
}
