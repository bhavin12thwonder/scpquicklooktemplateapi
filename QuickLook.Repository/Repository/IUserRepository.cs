﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface IUserRepository
    {
        SpTransactionMessage Insert(UsersViewModel viewModel);
        SpTransactionMessage Update(UsersViewModel viewModel);
        SpTransactionMessage Delete(int Id);
        List<UsersViewModel> GetAll();
        UsersViewModel GetById(int Id);
        UsersViewModel GetByUserName(string Username);
    }
}
