﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface ITemplateTabChartConfigurationRepository
    {
        SpTransactionMessage Insert(TemplateTabChartConfigurationViewModel templateViewModel,int userId);
        SpTransactionMessage Update(TemplateTabChartConfigurationViewModel templateViewModel, int userId);
        SpTransactionMessage Delete(int Id, int userId);
        List<TemplateTabChartConfigurationViewModel> GetAll();
        TemplateTabChartConfigurationViewModel GetById(int Id);
        List<TemplateTabChartConfigurationViewModel> GetByTemplateTabConfigId(int Id);
        bool CheckTabChartConfigName(int chartConfigId, int chartTypeId, string Name);
    }
}
