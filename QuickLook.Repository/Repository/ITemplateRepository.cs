﻿using System;
using System.Collections.Generic;
using System.Text;
using QuickLook.Common;
using QuickLook.Model;
using QuickLook.Model.ViewModel;

namespace QuickLook.Repository.Repository
{
    public interface ITemplateRepository
    {
        SpTransactionMessage Insert(TemplateViewModel templateViewModel);
        SpTransactionMessage Update(TemplateViewModel templateViewModel);
        SpTransactionMessage Delete(int Id);
        SpTransactionMessage Enable(int Id);
        List<TemplateViewModel> GetAll();
        TemplateViewModel GetById(int Id);
        List<TemplateTabPreviewViewModel> PreviewByTemplateId(int Id, int testId, string submatrixName);
        bool CheckTemplateName(int templateId, string name, int testType, List<UniqueIdsViewModel> testMode);

    }
}
