﻿using QuickLook.Common;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.Repository
{
    public interface ITemplateTabsRepository
    {
        SpTransactionMessage Insert(TemplateTabsCreateViewModel templateViewModel);
        SpTransactionMessage Update(TemplateTabsViewModel templateViewModel);
        SpTransactionMessage Delete(int Id,int tabIndex);
        List<TemplateTabsViewModel> GetAll();
        TemplateTabsViewModel GetById(int Id);
        List<TemplateTabsViewModel> GetByTemplateId(int Id);
    }
}
