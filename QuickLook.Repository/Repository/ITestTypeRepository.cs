﻿using System;
using System.Collections.Generic;
using System.Text;
using QuickLook.Common;
using QuickLook.Model;
using QuickLook.Model.ViewModel;

namespace QuickLook.Repository.Repository
{
    public interface ITestTypeRepository
    {
        SpTransactionMessage Insert(TestTypeViewModel templateViewModel);
        SpTransactionMessage Update(TestTypeViewModel templateViewModel);
        SpTransactionMessage Delete(int Id);
        List<TestTypeViewModel> GetAll();
        TestTypeViewModel GetById(int Id);

    }
}
