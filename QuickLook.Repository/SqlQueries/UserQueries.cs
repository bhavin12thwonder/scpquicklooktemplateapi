﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public class UserQueries
    {
        public static readonly string StoreProcInsert = "sappUserInsert";
        public static readonly string StoreProcUpdate = "sappUserUpdate";
        public static readonly string StoreProcGetById = "sappUserGetById";
        public static readonly string StoreProcGetAll = "sappUserGetAll";
        public static readonly string StoreProcDelete = "sappUserDelete";
        public static readonly string StoreProcGetByUsename = "sappUserGetByUsername";
    }
}
