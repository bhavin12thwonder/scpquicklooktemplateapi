﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public class ChartAnnotationQueries
    {
        public static readonly string StoreProcInsert = "sappChartAnnotationInsert";
        public static readonly string StoreProcUpdate = "sappChartAnnotationUpdate";
        public static readonly string StoreProcGetById = "sappChartAnnotationGetById";
        public static readonly string StoreProcGetByChartConfigId = "sappChartAnnotationGetByChartConfigId";
        public static readonly string StoreProcGetAll = "sappChartAnnotationGetAll";
        public static readonly string StoreProcDelete = "sappChartAnnotationDelete";
    }
}
