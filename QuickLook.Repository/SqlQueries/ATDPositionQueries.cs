﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public class ATDPositionQueries
    {
        public static readonly string StoreProcInsert = "sappATDPositionInsert";
        public static readonly string StoreProcUpdate = "sappATDPositionUpdate";
        public static readonly string StoreProcGetById = "sappATDPositionGetById";
        public static readonly string StoreProcGetAll = "sappATDPositionGetAll";
        public static readonly string StoreProcDelete = "sappATDPositionDelete";
    }
}
