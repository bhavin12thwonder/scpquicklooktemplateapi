﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public class ChartChannelQueries
    {
        public static readonly string StoreProcInsert = "sappChartChannelInsert";
        public static readonly string StoreProcUpdate = "sappChartChannelUpdate";
        public static readonly string StoreProcGetById = "sappChartChannelGetById";
        public static readonly string StoreProcGetByChartConfigId = "sappChartChannelGetByChartConfigId";
        public static readonly string StoreProcGetATDPositionById = "sappATDPositionGetByChartChannelId";
        public static readonly string StoreProcGetAll = "sappChartChannelGetAll";
        public static readonly string StoreProcDelete = "sappChartChannelDelete";
        public static readonly string StoreProcXaxisChartChannel = "sappXaxisChartChannelInsert";
    }
}
