﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TestTypeQueries
    {
        public static readonly string StoreProcInsert = "sappTestTypeInsert";
        public static readonly string StoreProcUpdate = "sappTestTypeUpdate";
        public static readonly string StoreProcGetById = "sappTestTypeGetById";
        public static readonly string StoreProcGetAll = "sappTestTypeGetAll";
        public static readonly string StoreProcDelete = "sappTestTypeDelete";
    }
}
