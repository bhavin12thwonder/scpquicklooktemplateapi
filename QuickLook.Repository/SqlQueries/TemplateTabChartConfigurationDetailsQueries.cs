﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TemplateTabChartConfigurationDetailsQueries
    {
        public static readonly string StoreProcInsert = "sappTemplateTabChartConfigurationDetailsInsert";
        public static readonly string StoreProcUpdate = "sappTemplateTabChartConfigurationDetailsUpdate";
        public static readonly string StoreProcGetById = "sappTemplateTabChartConfigurationDetailsGetById";
        public static readonly string StoreProcGetByTemplateTabChartConfigId = "sappTemplateTabChartConfigurationDetailsGetByChartConfigurationId";
        public static readonly string StoreProcPreviewByTemplateTabChartConfigId = "sappTemplateTabChartConfigurationDetailsPreviewByChartConfigurationId";
        public static readonly string StoreProcGetAll = "sappTemplateTabChartConfigurationDetailsGetAll";
        public static readonly string StoreProcDelete = "sappTemplateTabChartConfigurationDetailsDelete";
    }
}
