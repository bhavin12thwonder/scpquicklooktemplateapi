﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TemplateTabsQueries
    {
        public static readonly string StoreProcInsert = "sappTemplateTabsInsert";
        public static readonly string StoreProcUpdate = "sappTemplateTabsUpdate";
        public static readonly string StoreProcGetById = "sappTemplateTabsGetById";
        public static readonly string StoreProcGetByTemplateId = "sappTemplateTabsGetByTemplateId";
        public static readonly string StoreProcGetAll = "sappTemplateTabsGetAll";
        public static readonly string StoreProcDelete = "sappTemplateTabsDelete";
    }
}
