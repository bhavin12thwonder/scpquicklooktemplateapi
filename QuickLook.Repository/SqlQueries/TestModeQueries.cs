﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TestModeQueries
    {
        public static readonly string StoreProcInsert = "sappTestModeInsert";
        public static readonly string StoreProcUpdate = "sappTestModeUpdate";
        public static readonly string StoreProcGetById = "sappTestModeGetById";
        public static readonly string StoreProcGetByTestTypeId = "sappTestModeGetByTestTypeId";
        public static readonly string StoreProcGetAll = "sappTestModeGetAll";
        public static readonly string StoreProcDelete = "sappTestModeDelete";
    }
}
