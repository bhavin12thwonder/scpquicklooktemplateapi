﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public class FormulaOperatorQueries
    {
        public static readonly string StoreProcInsert = "sappFormulaOperatorInsert";
        public static readonly string StoreProcUpdate = "sappFormulaOperatorUpdate";
        public static readonly string StoreProcGetById = "sappFormulaOperatorGetById";
        public static readonly string StoreProcGetAll = "sappFormulaOperatorGetAll";
        public static readonly string StoreProcDelete = "sappFormulaOperatorDelete";
    }
}
