﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public class ChartTypeQueries
    {
        public static readonly string StoreProcInsert = "sappChartTypeInsert";
        public static readonly string StoreProcUpdate = "sappChartTypeUpdate";
        public static readonly string StoreProcGetById = "sappChartTypeGetById";
        public static readonly string StoreProcGetAll = "sappChartTypeGetAll";
        public static readonly string StoreProcDelete = "sappChartTypeDelete";
    }
}
