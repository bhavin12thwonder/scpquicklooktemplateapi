﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TemplateTabChartConfigurationQueries
    {
        public static readonly string StoreProcInsert = "sappTemplateTabChartConfigurationInsert";
        public static readonly string StoreProcUpdate = "sappTemplateTabChartConfigurationUpdate";
        public static readonly string StoreProcGetById = "sappTemplateTabChartConfigurationGetById";
        public static readonly string StoreProcGetByTemplateTabConfigId = "sappTemplateTabChartConfigurationGetByTabConfigurationId";
        public static readonly string StoreProcCheckTemplateTabChartConfigName = "sappTemplateTabChartConfigurationNameAvailability";
        public static readonly string StoreProcGetAll = "sappTemplateTabChartConfigurationGetAll";
        public static readonly string StoreProcDelete = "sappTemplateTabChartConfigurationDelete";
    }
}
