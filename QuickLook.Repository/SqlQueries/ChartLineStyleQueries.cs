﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public class ChartLineStyleQueries
    {
        public static readonly string StoreProcInsert = "sappChartLineStyleInsert";
        public static readonly string StoreProcUpdate = "sappChartLineStyleUpdate";
        public static readonly string StoreProcGetById = "sappChartLineStyleGetById";
        public static readonly string StoreProcGetAll = "sappChartLineStyleGetAll";
        public static readonly string StoreProcDelete = "sappChartLineStyleDelete";
    }
}
