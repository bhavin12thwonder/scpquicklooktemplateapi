﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public class ChartLineThicknessQueries
    {
        public static readonly string StoreProcInsert = "sappChartLineThicknessInsert";
        public static readonly string StoreProcUpdate = "sappChartLineThicknessUpdate";
        public static readonly string StoreProcGetById = "sappChartLineThicknessGetById";
        public static readonly string StoreProcGetAll = "sappChartLineThicknessGetAll";
        public static readonly string StoreProcDelete = "sappChartLineThicknessDelete";
    }
}
