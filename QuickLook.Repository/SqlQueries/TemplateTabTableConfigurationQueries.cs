﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TemplateTabTableConfigurationQueries
    {
        public static readonly string StoreProcInsert = "sappTemplateTabTableConfigurationInsert";
        public static readonly string StoreProcUpdate = "sappTemplateTabTableConfigurationUpdate";
        public static readonly string StoreProcGetById = "sappTemplateTabTableConfigurationGetById";
        public static readonly string StoreProcGetByTemplateTabConfigId = "sappTemplateTabTableConfigurationGetByTabConfigurationId";
        public static readonly string StoreProcCheckTemplateTabTableConfigName = "sappTemplateTabTableConfigurationNameAvailability";
        public static readonly string StoreProcGetAll = "sappTemplateTabTableConfigurationGetAll";
        public static readonly string StoreProcDelete = "sappTemplateTabTableConfigurationDelete";
    }
}
