﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TemplateTabConfigurationQueries
    {
        public static readonly string StoreProcInsert = "sappTemplateTabConfigurationInsert";
        public static readonly string StoreProcUpdate = "sappTemplateTabConfigurationUpdate";
        public static readonly string StoreProcUpdateById = "sappTemplateTabConfigurationUpdateById";
        public static readonly string StoreProcGetById = "sappTemplateTabConfigurationGetById";
        public static readonly string StoreProcGetByTemplateTabId = "sappTemplateTabConfigurationGetByTemplateTabId";
        public static readonly string StoreProcPreviewByTemplateTabId = "sappTemplateTabConfigurationPreviewByTabId";
        public static readonly string StoreProcTemplateTabConfigurationTypeGetByTemplateTabId = "sappTemplateTabConfigurationTypeGetByTemplateTabId";
        public static readonly string StoreProcTemplateTabConfigurationTypeUpdateByTemplateTabId = "sappTemplateTabConfigurationTypeUpdateByTemplateTabId";
        public static readonly string StoreProcGetAll = "sappTemplateTabConfigurationGetAll";
        public static readonly string StoreProcDelete = "sappTemplateTabConfigurationDelete";
    }
}
