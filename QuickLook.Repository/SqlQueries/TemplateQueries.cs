﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TemplateQueries
    {
        public static readonly string StoreProcInsert = "sappTemplateInsert";
        public static readonly string StoreProcUpdate = "sappTemplateUpdate";
        public static readonly string StoreProcGetById = "sappTemplateGetById";
        public static readonly string StoreProcPreviewByTemplateId = "sappTemplatePreviewById";
        public static readonly string StoreProcCheckTemplateName = "sappTemplateNameAvailability";
        public static readonly string StoreProcGetAll = "sappTemplateGetAll";
        public static readonly string StoreProcDelete = "sappTemplateDelete";
        public static readonly string StoreProcEnable = "sappTemplateEnable";
        public static readonly string StoreProcGetTestModeIdsById = "sappGetTestModeIds";
    }
}
