﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLook.Repository.SqlQueries
{
    public static class TemplateTabTableConfigurationDetailsQueries
    {
        public static readonly string StoreProcInsert = "sappTemplateTabTableConfigurationDetailsInsert";
        public static readonly string StoreProcUpdate = "sappTemplateTabTableConfigurationDetailsUpdate";
        public static readonly string StoreProcGetById = "sappTemplateTabTableConfigurationDetailsGetById";
        public static readonly string StoreProcGetByTemplateTabTableConfigId = "sappTemplateTabTableConfigurationDetailsGetByTableConfigurationId";
        public static readonly string StoreProcPreviewByTemplateTabTableConfigId = "sappTemplateTabTableConfigurationDetailsPreviewByTableConfigurationId";
        public static readonly string StoreProcGetAll = "sappTemplateTabTableConfigurationDetailsGetAll";
        public static readonly string StoreProcDelete = "sappTemplateTabTableConfigurationDetailsDelete";
    }
}
