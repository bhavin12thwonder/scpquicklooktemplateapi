﻿using System;

namespace QuickLook.Common
{
    public class SpTransactionMessage
    {
        public int? Id { get; set; }
        public string TransactionMessage { get; set; }
        public int Number { get; set; }
        public int Severity { get; set; }
        // public string State  { get; set; }
        public string StoredProcedureName { get; set; }
        public int LineNumber { get; set; }
        public bool IsSuccess { get; set; }
    }
    public interface IViewModel
    {
        bool IsSuccess { get; set; }
        string Message { get; set; }
    }
}
