﻿using Dapper;
using Newtonsoft.Json;
using QuickLook.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace QuickLook.Common
{
    public static class TableValueParameterExtension
    {
        public static SqlMapper.ICustomQueryParameter AsTableValuedParameter<T>
        (this IEnumerable<T> enumerable, string typeName, IEnumerable<string> orderedColumnNames = null)
        {
            var dataTable = new DataTable();
            if (enumerable == null)
            {
                dataTable.Columns.Add("NONAME");
                return dataTable.AsTableValuedParameter(typeName);
            }
            if (typeof(T).IsValueType || typeof(T).FullName.Equals("System.String"))
            {
                dataTable.Columns.Add(orderedColumnNames == null ?
                    "NONAME" : orderedColumnNames.First(), typeof(T));
                foreach (T obj in enumerable)
                {
                    dataTable.Rows.Add(obj);
                }
            }
            else
            {
                PropertyInfo[] properties = typeof(T).GetProperties
                    (BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo[] readableProperties = properties.Where
                    (w => w.CanRead).ToArray();
                if (readableProperties.Length > 1 && orderedColumnNames == null)
                    throw new ArgumentException("Ordered list of column names must be provided when TVP contains more than one column");
                var columnNames = (orderedColumnNames ??
                    readableProperties.Select(s => s.Name)).ToArray();
                foreach (string name in columnNames)
                {
                    dataTable.Columns.Add(name, readableProperties.Single
                        (s => s.Name.Equals(name)).PropertyType);
                }

                foreach (T obj in enumerable)
                {
                    dataTable.Rows.Add(
                        columnNames.Select(s => readableProperties.Single
                            (s2 => s2.Name.Equals(s)).GetValue(obj))
                            .ToArray());
                }
            }
            return dataTable.AsTableValuedParameter(typeName);
        }

        public static DataTable ToDataTable<T>(this List<T> iList)
        {
            DataTable dataTable = new DataTable();

            PropertyDescriptorCollection propertyDescriptorCollection = TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);


                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            if (iList != null && iList.Count() > 0)
            {
                foreach (T iListItem in iList)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                    }
                    dataTable.Rows.Add(values);
                }
            }
            return dataTable;
        }
    }

    public class ChartAxisConfiguration
    {
        public double[] channelXAxis, channelYAxis, channelDataConverted;
        public ChannelDataViewModel channelDataXAxis, channelDataYAxis;
        public double barChartXAxisValue, barChartYAxisValue, atdPositionValue;
        List<ChartChannelATDPositionPreviewViewModel> lstATDPositionsValues;

        public double[] GetChannelXAxis(int testId, string channelName)
        {
            channelXAxis = null;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetChannelData/?testId=" + testId + "&channels=" + channelName.ToUpper());
            request.Method = "GET";
            request.ContentType = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var apiResponse = reader.ReadToEnd();
                    var channelData = JsonConvert.DeserializeObject<List<ChannelDataViewModel>>(apiResponse);
                    channelXAxis = (channelData.Count > 0) ? channelData[0].Value : null;
                }
            }
            return channelXAxis;
        }

        public ChannelDataViewModel GetChannelDataXAxis(int testId, string channelName)
        {
            channelDataXAxis = null;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetChannelData/?testId=" + testId + "&channels=" + channelName.ToUpper());
            request.Method = "GET";
            request.ContentType = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var apiResponse = reader.ReadToEnd();
                    var channelData = JsonConvert.DeserializeObject<List<ChannelDataViewModel>>(apiResponse);
                    channelDataXAxis = (channelData.Count > 0) ? channelData[0] : null;
                }
            }
            return channelDataXAxis;
        }

        public double[] GetChannelYAxis(int testId, string channelName, bool polarity)
        {
            channelYAxis = null;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;
            if (string.IsNullOrEmpty(channelName)) {
                return channelYAxis;
            }
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetChannelData/?testId=" + testId + "&channels=" + channelName.ToUpper());
            request.Method = "GET";
            request.ContentType = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var apiResponse = reader.ReadToEnd();
                    var channelData = JsonConvert.DeserializeObject<List<ChannelDataViewModel>>(apiResponse);
                    channelYAxis = (channelData.Count > 0) ? channelData[0].Value : null;
                    if (polarity && channelYAxis != null)
                    {
                        channelYAxis = channelYAxis.Select(x => x * (-1)).ToArray();
                    }
                }
            }
            return channelYAxis;
        }

        public ChannelDataViewModel GetChannelDataYAxis(int testId, string channelName, bool polarity)
        {
            channelDataYAxis = null;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;
            if (string.IsNullOrEmpty(channelName))
            {
                return channelDataYAxis;
            }
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetChannelData/?testId=" + testId + "&channels=" + channelName.ToUpper());
            request.Method = "GET";
            request.ContentType = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var apiResponse = reader.ReadToEnd();
                    var channelData = JsonConvert.DeserializeObject<List<ChannelDataViewModel>>(apiResponse);
                    channelDataYAxis = (channelData.Count > 0) ? channelData[0] : null;
                    if (polarity && channelDataYAxis != null)
                    {
                        channelDataYAxis.Value = channelDataYAxis.Value.Select(x => x * (-1)).ToArray();
                    }
                }
            }
            return channelDataYAxis;
        }

        public double[] GetChannelDataConverted(int testId, string submatrixName, string unit, string channelName)
        {
            channelDataConverted = null;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/ConvertData_Unit");
            request.Method = "POST";
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(new
                {
                    TestId = testId,
                    SubMatrix = submatrixName,
                    Unit = unit,
                    ChannelName = channelName
                });
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var apiResponse = reader.ReadToEnd();
                    var channelData = JsonConvert.DeserializeObject<List<ChannelDataViewModel>>(apiResponse);
                    channelDataConverted = (channelData.Count > 0) ? channelData[0].Value : null;
                }
            }
            return channelDataConverted;

        }

        public double GetBarChartXAxisValue(int testId, string channelName)
        {
            barChartXAxisValue = 0;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetCalculationValue/?testRequestId=" + testId + "&channels=" + channelName.ToUpper());
            request.Method = "GET";
            request.ContentType = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var apiResponse = reader.ReadToEnd();
                    var channelData = JsonConvert.DeserializeObject<List<CalculationValueDataViewModel>>(apiResponse);
                    barChartXAxisValue = (channelData.Count > 0) ? channelData[0].Value : 0;
                }
            }
            return barChartXAxisValue;
        }

        public double GetBarChartYAxisValue(int testId, string channelName)
        {
            barChartYAxisValue = 0;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetCalculationValue/?testRequestId=" + testId + "&channels=" + channelName.ToUpper());
            request.Method = "GET";
            request.ContentType = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var apiResponse = reader.ReadToEnd();
                    var channelData = JsonConvert.DeserializeObject<List<CalculationValueDataViewModel>>(apiResponse);
                    barChartYAxisValue = (channelData.Count > 0) ? channelData[0].Value : 0;
                }
            }
            return barChartYAxisValue;
        }

        public double GetATDPositionAxisValue(int testId, ATDRequestModel atdPsitionModel)
        {
            barChartYAxisValue = 0;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetCalculationValue_ATD/testRequestId=" + testId);
            if (atdPsitionModel.ATDPositionId == 0)
            {
                return 0.0;
            }
            request.Method = "POST";
            byte[] bytesData = Encoding.UTF8.GetBytes(atdPsitionModel.ToString());
            request.ContentType = "application/json";
            request.ContentLength = bytesData.Length;
            using (Stream writer = request.GetRequestStream())
            {
                writer.Write(bytesData, 0, bytesData.Length);
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    var apiResponse = reader.ReadToEnd();
                    var channelData = JsonConvert.DeserializeObject<List<CalculationValueDataViewModel>>(apiResponse);
                    atdPositionValue = (channelData.Count > 0) ? channelData[0].Value : 0;
                }
            }
            return atdPositionValue;
        }

        public List<ChartChannelATDPositionPreviewViewModel> GetATDPositionsAxisValue(int testId, List<ATDRequestModel> atdPsitionModel)
        {
            barChartYAxisValue = 0;
            lstATDPositionsValues = new List<ChartChannelATDPositionPreviewViewModel>();
            if (atdPsitionModel.Count > 0)
            {
                string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;

                foreach (var item in atdPsitionModel)
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetCalculationValue_ATD/testRequestId=" + testId);
                    request.Method = "POST";
                    byte[] bytesData = Encoding.UTF8.GetBytes(item.ToString());
                    request.ContentType = "application/json";
                    request.ContentLength = bytesData.Length;
                    using (Stream writer = request.GetRequestStream())
                    {
                        writer.Write(bytesData, 0, bytesData.Length);
                    }

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            var apiResponse = reader.ReadToEnd();
                            var channelData = JsonConvert.DeserializeObject<List<CalculationValueDataViewModel>>(apiResponse);
                            atdPositionValue = (channelData.Count > 0) ? channelData[0].Value : 0;
                        }
                    }
                    lstATDPositionsValues.Add(new ChartChannelATDPositionPreviewViewModel { ATDPositionId = item.ATDPositionId, ATDPositionValue = item.ATDPosition, ATDPositionAxisValue = atdPositionValue });
                }
            }
            return lstATDPositionsValues;
        }
    }

    public class TableCalculationConfiguration
    {
        public CalculationValueDataViewModel tableValueObj;
        public double calculationValue;

        public CalculationValueDataViewModel GetCalculationValueObj(int testId, string calculation)
        {
            tableValueObj = null;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;
            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetChannelData/?testId=" + testId + "&calculations=" + calculation);
            request.Method = "GET";
            request.ContentType = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    var apiResponse = reader.ReadToEnd();
                    var calcValue = JsonConvert.DeserializeObject<List<CalculationValueDataViewModel>>(apiResponse);
                    tableValueObj = (calcValue.Count > 0) ? calcValue[0] : null;
                }
            }
            return tableValueObj;
        }

        public double GetCalculationValue(int testId, string calculation, string atd)
        {
            calculationValue = 0;
            string apiUrl = (Convert.ToBoolean(Resource.ConfigResource.isSSL)) ? "https:" + Resource.ConfigResource.ASAMApiUrl : "http:" + Resource.ConfigResource.ASAMApiUrl;
            
            
            if (string.IsNullOrEmpty(atd))
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetCalculationValue?testId=" + testId + "&calculations=" + calculation);
                request.Method = "GET";
                request.ContentType = "application/json";
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var apiResponse = reader.ReadToEnd();
                        var calcValue = JsonConvert.DeserializeObject<List<CalculationValueDataViewModel>>(apiResponse);
                        calculationValue = (calcValue.Count > 0) ? calcValue[0].Value : 0;
                    }
                }
            }
            else
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "ASAM/GetCalculationValue_ATD?testId=" + testId);
                request.Method = "POST";
                request.ContentType = "application/json";
                
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(new
                    {
                        CalcName = calculation,
                        ATD = atd
                    });
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var apiResponse = reader.ReadToEnd();
                        var calcValue = JsonConvert.DeserializeObject<List<CalculationValueDataViewModel>>(apiResponse);
                        calculationValue = (calcValue.Count > 0) ? calcValue[0].Value : 0;
                    }
                }
            }            
            return calculationValue;
        }
    }
}
